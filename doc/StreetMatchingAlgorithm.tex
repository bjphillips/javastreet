\documentclass[a4paper]{article}
\usepackage{fancyvrb}
\usepackage{verbatim}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage{adjustbox}

% Stop hyperref putting coloured boxes around links
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}

% Allow verbatim text in footnotes
\VerbatimFootnotes

\setlength{\parindent}{0pt}
\setlength{\parskip}{1.3ex}

\newcommand{\st}[1]{\(\texttt{#1}\)}

\title{The Street Matching Algorithm}
\author{Braden Phillips\\The University of Adelaide}
\date{\today}

\begin{document}
\maketitle

\section{Introduction}

The Street Matching Algorithm determines when a production rule will
\emph{fire} and apply the actions on its RHS. The purpose of this document is
to define the desired behaviour of the algorithm without over-specifying how
the algorithm is implemented. This is hard to do and hence we resort to
providing a reference algorithm.

Although this document reviews some concepts, it is assumed the reader is
familiar with ``A Brief Description of Street Language''.

\section{Nomenclature}

\subsection{Conditions and Alpha Memories}

A Street Language production rule is shown in Figure~\ref{fig:ExampleRule}.

\begin{figure}
\begin{verbatim}
st {ExampleRule     
    (A B <C>)
    (<C> <D> E)
    -(E F <D>)          
-->                     
    (<C> <D> <E>)    
}
\end{verbatim}
\caption{A Street Language production rule.}
\label{fig:ExampleRule}
\end{figure}

A production rule's \emph{LHS} is a list of \emph{condition elements}.
\st{(A B <C>)} is a condition element.

A condition element is a list of \emph{conditions}. \st{<C>} is a condition.

The LHS specifies a pattern that must be matched by working memory for the rule
to fire.

A \emph{positive condition element} specifies a part of the pattern that must
exist in working memory for the rule to fire. \st{(A B <C>)} is a positive
condition element.

A \emph{negated condition element} specifies a part of the pattern that must
not exist in working memory for the rule to fire. \st{-(E F <D>)} is a negated
condition element.

A more precise specification of the meaning of positive and negated condition
elements is presented in Section~\ref{sec:InstancesAndPartialInstances},
\nameref{sec:InstancesAndPartialInstances}.

An \emph{alpha block} is the set of WMEs in working memory that satisfy a
condition element.

An \emph{alpha memory} is the collection of alpha blocks for a rule. An example
of an alpha memory is shown in Figure~\ref{fig:AlphaMemory}.

\begin{figure}
\begin{verbatim}
(A B <C>) | (<C> <D> E) | -(E F <D>)
------------------------------------
(A B  1 ) | ( 2   3  E) |  (E F  3 )
(A B  2 ) | ( 4   3  E) | 
(A B  7 ) | ( 1   7  E) |
\end{verbatim}
\caption{An alpha memory for the rule in Figure~\ref{fig:ExampleRule}}
\label{fig:AlphaMemory}
\end{figure}

In Street Language the conditions can include:
\begin{itemize}
	\item \st{C}
	\item \st{!= C}
	\item \st{> C}
	\item \st{< C}
	\item \st{>= C}       
	\item \st{<= C}      
	\item \st{<x>}
	\item \st{<*>}      
	\item \st{<x> != C}   
	\item \st{<x> < C}  
	\item \st{<x> > C}
	\item \st{<x> <= C}  
	\item \st{<x> >= C} 
	\item \st{<x> != <y>}
\end{itemize}

The conditions that include a constant \st{C} and the wildcard condition
\st{<*>} are called \emph {constant conditions}.

The conditions that include a variable \st{<x>} are called \emph{variable
conditions}.

A condition such as \st{<x> > C} is both constant and variable.

Before a WME is added to an alpha block it must satisfy all constant
conditions in the corresponding condition element.

Alpha block columns for variable conditions contain values that are needed for
the Street Matching Algorithm. An implementation of the algorithm needs to store
these values.

Alpha block columns for conditions that are not variable contain values that
are not needed. An implementation of the algorithm does not need to store these
values.

\subsection{Instances and Partial Instances}
\label{sec:InstancesAndPartialInstances}

An \emph{instance} is a set of WMEs that satisfy a LHS. Hence 
\st{\{(A B 1), (1 7 E)\}} is an instance of the example in
Figure~\ref{fig:AlphaMemory}.

The purpose of the Street Matching Algorithm is to find the new instances
that arise after a sequence of changes are made to working memory.

An instance specifies a unique value for each variable appearing in the positive
conditions elements on the RHS. We say the instance \emph{binds} the values of
these variables.

To apply the actions of rule for a new instance, all that is needed is the
values bound to the variables that appear on both the LHS and the RHS. Hence it
is sufficient for an implementation of the Street Matching Algorithm to output
just the \emph{variable bindings} corresponding to a new instance.

The variable bindings corresponding to an instance can be substituted into the
condition elements of the rule to make assertions about working memory. For
the example in Figure~\ref{fig:ExampleRule}, if there is an instance with 
\st{<C> = 1} and \st{<D> = 7} then
\begin{itemize}
	\item working memory contains a WME \st{(A B 1)}
	\item working memory contains a WME \st{(1 7 E)}
	\item working memory does not contain a WME \st{(E F 7)}
\end{itemize}

An instance binds the values of all the variables appearing in positive
condition elements. Negated condition elements are only allowed to contain
variables that appear in positive condition elements on the same LHS. Hence an
instance also binds the values of all variables in the negated condition
elements.

Let us call a pair consisting of a condition element and a WME a \emph{match
pair}. For example \st{((A B <C>), (A B 1))} is a match pair. If the
condition element is positive we have a \emph{positive match pair}; if the
condition element is negated we have a \emph{negated match pair}.

We call an \emph{annotated instance} the set of positive match pairs
corresponding to the WMEs in an instance. Hence 
\st{\{((A B <C>), (A B 1)), ((<C> <D> E), (1 7 E))\}} is an annotated instance
from Figure~\ref{fig:AlphaMemory}.

Note that annotated instances never contain negated match pairs.

A \emph{partial instance} is a set of positive match pairs. A partial instance
\emph{may} lead to one or more annotated instances through a process or adding
positive match pairs.

\section{Finding Instances}
\label{sec:FindingInstances}

The goal of the Street Matching Algorithm is to find \emph{new} instances of a
rule. Before we worry about whether an instance is new, let us just consider
an algorithm for finding all instances of a rule for a given state of working
memory.

The algorithm will begin with a set of partial instances, each with only one
match pair. It will proceed by adding positive match pairs and testing against
negated match pairs until a set of instances is found.

An important constraint for the Street Matching Algorithm is that it should
produce consistent outputs irrespective of the order in which it considers the
condition elements in a rule. This constraint is imposed to permit
optimisations in which the order is selected to minimise the number of steps
required to find new instances.

The algorithm described here does require that negated condition elements are
considered after related positive condition elements. This does not impact on
the possible performance benefit of ordering.

\subsection{First Iteration}

If any of the positive condition elements has an empty alpha block then there
will be no instances of the rule. The algorithm should stop.

Any negated condition elements with empty alpha blocks cannot block the
creation of instances. The algorithm should proceed but should ignore these
condition elements.

The algorithm begins with one of the positive condition elements (every rule
has at least one positive condition element) and its corresponding alpha
block. We create a partial instance for each match pair from the alpha block
and continue with the next iteration of the algorithm for each of these
partial instances.

\subsection{Subsequent Iterations}

This step of the algorithm involves \emph{joining} match pairs. Match pairs
join if they have a consistent binding for all shared variables that satisfies
all variable conditions. The WMEs in an alpha block will satisfy all the
constant conditions in the condition element (including wildcard conditions)
and hence it is only the variable bindings and the special condition \st{<x>
!= <y>} that need to be considered when joining match pairs from alpha blocks.

For this step we take each partial instance from the previous iteration and
combine it with the next condition element from the LHS and its corresponding
alpha block. 

We will only consider a negated condition element once all its variables
appear in the partial instance.

If the condition element is positive then we create a new partial instance for
each match pair from the alpha block that joins with all the match pairs in
the partial instance. The new partial instances are added to the set of
partial instances to consider at the next iteration.

If the condition element is negated then if any of the match pairs from its
alpha block join with all the match pairs in the partial instance then the
partial instance will not lead to any instances and should be discarded. If
none of the match pairs from the alpha block join with the partial instance
then the partial instance as it is can be added to the set of partial
instances to be considered next iteration.

\section{Finding New Instances}

The Street Matching Algorithm is only concerned with finding new instantiations.
It does not detect when an instantiation ceases to exist.

A \emph{critical change} to working memory is one that may lead to new
instantiations of a rule.

New instantiations of a rule can occur when a WME is added to working memory
that matches a positive condition element in the rule.

New instantiations of a rule can also occur when a WME is deleted from working
memory that matched a negated condition element in the rule.

Hence any addition of a new WME to the alpha block of a positive condition
element, or any deletion of a WME from the alpha block of a negated condition
element is a critical change.

In Street Language, WMEs are not always added and removed from memory one by
one. A number of changes to working memory can be grouped together as an
atomic action.

\subsection{Newly Added and Newly Deleted WMEs}

Actions from a given RHS and corresponding to a given instantiation are
grouped together as an atomic action. They act together to make a single
change to working memory rather than a series of changes. The actions in an
atomic action arrive at a production rule together. They will be performed in
the order in which they are written on the RHS and will not be interleaved
with any other actions. 

Consider a RHS that deletes and then adds the WME \st{(A B C)}. If 
\st{(A B C)} did not exist initially, then the action will add it as a new
WME. This addition may trigger new rule instantiations. If 
\st{(A B C)} did exist initially, then the action will have no effect and
should not trigger any new rule instantiations.

Similarly, consider a a RHS that adds and then deletes the WME \st{(A B C)}.
If \st{(A B C)} did exist initially, then the action will delete it. This
deletion may trigger new rule instantiations. If \st{(A B C)} did not exist
initially, then the action will have no effect and should not trigger any new
rule instantiations.

The following process can be used to find whether an atomic action adds new
WMEs or deletes old ones in an alpha block. To begin, all WMEs in the alpha
block are marked as not added ($added = 0$) and not deleted ($deleted = 0$).
Then, as the steps in the atomic action add and delete a WME, its status is
updated according to the finite state machine in Figure~\ref{fig:NewWMEFSM}.

Once the atomic action is complete, add WMEs in positive alpha blocks marked
as deleted, can be removed from the alpha blocks.

\begin{figure}
\begin{center}
\includegraphics[scale=0.5]{NewWMEFSM.pdf}
\end{center}
\caption{Finite state machine to update the status of a WME over the
course of an atomic action}
\label{fig:NewWMEFSM}
\end{figure}

\subsection{Searching for New Instances}

Once an atomic action is complete, the Street Matching Algorithm conducts a
search for new instances. It begins a new search for every critical change
i.e. for every WME added to the alpha block of a positive condition element
and for every WME deleted from the alpha block of a negated condition element.

To begin a search with a newly added WME we begin with a partial instance
containing only the match pair for the new WME. We then use the algorithm from
Section~\ref{sec:FindingInstances} to find all instances by combining this
partial instance with the other alpha blocks in the rule.

All instances found must be valid for the new state of working memory; hence
all newly deleted WMEs are treated as absent form working memory and all newly
added WMEs are treated as present in working memory.

A search starting with the match pair for a newly added WME will find all new
instances that include that match pair. To avoid finding duplicates, once we
have begun a search with a WME it is marked as searched. If a search starting
with another match pair tries to add a searched match pair from a positive
condition element then it will only result in duplicates and hence we do not add
partial instances including searched matched pairs to the set of new partial
instances.

For each newly deleted WME from a negated CE we need to find instances that
are valid for the new state of working memory but that were stopped by the
presence of the WME in the negated alpha block. Note that there may still be
WMEs in the negated alpha block that stop any new instances.

How might a WME have been stopping otherwise valid instances? The match pair for
this WME joined with the partial instantiation consisting of match pairs from
all the positive CEs.

So to find these stopped instances, consider beginning a search for instances
with a match pair consisting of the newly deleted WME and its negated condition
element, but treat this is if it were the match pair from a positive condition
element. Lets imagine we create a \emph{virtual match pair} consisting of the
newly deleted WME and its un-negated condition element.

Then for new instances, there must be match pairs from the remaining positive
condition elements that join with the virtual match pair. As usual, there should
not be any match pairs from any of the negated condition elements that join with
the partial instances. This includes the negated condition element from which
the newly deleted WME in the virtual match pair was drawn as this may still
contain WMEs that stop a new instance. 

Again this search is conducted for the new state of working memory. Newly
added WMEs are considered present in memory and newly deleted WMEs are
considered as absent from working memory.

\end{document}

\documentclass[a4paper]{article}
\usepackage{fancyvrb}
\usepackage{verbatim}
\usepackage[pdftex]{graphicx}
\usepackage{hyperref}
\usepackage{adjustbox}
\usepackage{cite}
\usepackage{textcomp} % for \texttildelow

% Stop hyperref putting coloured boxes around links
\hypersetup{
    colorlinks,%
    citecolor=black,%
    filecolor=black,%
    linkcolor=black,%
    urlcolor=black
}

% Allow verbatim text in footnotes
\VerbatimFootnotes

\setlength{\parindent}{0pt}
\setlength{\parskip}{1.3ex}

\newcommand{\st}[1]{\(\texttt{#1}\)}

\title{A Brief Description of Street Language}
\author{Jesse Frost \& Braden Phillips\\The University of Adelaide}
\date{\today}

\begin{document}
\maketitle

\section{Introduction}

Street Language is the equivalent of assembly language for a Street Engine.
It is a parallel production language, inspired by OPS5
\cite{forgy82} and Soar's production language \cite{laird12} but different to
these predecessors.

Because the Street Engine is intended for cognitive computing, instead of
talking about `programs', we say that Street Language production rules dictate
the behaviour of a \emph{Street agent}.

\section{Street Language}

\subsection{Grammar}

Let us begin with an example of a Street Language production rule. (The
complete grammar is included in Appendix~\ref{sec:grammar}.)

\begin{small}
\begin{verbatim}
/* Street Language consists of production rules */
st {                    // Rules begin with 'st' and are enclosed in parentheses.
    (<p> name Fred)     // A rule may have one or more condition elements.
    (<p> age (<a><40))  // Condition elements are enclosed in brackets.
-->                     // A '-->' comes after the conditions.
    (<p> isOld)         // Then there can be one or more actions
}
\end{verbatim}
\end{small}

Note:
\begin{itemize}
	\item single line comments begin with \st{//}
	\item multi-line comments are enclosed between \st{/*} and
	\st{*/}
	\item the language is case sensitive
	\item condition elements and actions refer to \emph{variables}
	and	\emph{constants}
	\item variables are enclosed between \st{<} and \st{>}
	\item variable names must be \emph{identifiers} beginning with a letter or
	underscore and optionally followed by any number of letters, digits or
	underscores
	\item constants can be identifiers or integers
	\item condition elements can include the binary relations \st{<},
	\st{>}, \st{<=}, \st{>=}, or \st{!=} (but not \st{=})
	\item actions can include the binary operators \st{+}, \st{-}, \st{*} and
	\st{/}
\end{itemize}

\subsection{Working Memory}

A Street agent has a \emph{working memory} consisting of a set of tuples
called Working Memory Elements (WMEs) and pronounced `wimmies'. Working memory
is a set. There is no order defined and a WME is either present or not; it
does not logically have multiple instances.

The elements in a WME are sometimes referred to as the WME's
\emph{attributes}.  In \st{(a b c)} we say that \st{a} is the first
attribute or attribute 0. \st{b} is the second attribute or attribute 1. A WME
can have one or more attributes.

\subsection{Productions}

Productions, also known as production rules or just rules, consist of a left
hand side (LHS) of one or more condition elements (CEs), and a right hand side
(RHS) of one or more actions. CEs and actions may be either positive or
negated. The LHS must start with one positive CE. The RHS may contain any
combination of actions that create or delete WMEs.

The LHS specifies a pattern in working memory. When a new instance of this
pattern is found, the RHS actions are executed. The actions will make changes
to working memory.

\subsection{Condition Elements}

Condition elements represent individual working memory elements to test for. A
positive CE tests for the presence of a WME and a negated CE tests for the
absence of a WME. The LHS must begin with at least one positive CE.

CEs are a tuple of tests. A test may either test for a constant, a variable,
a wildcard, or a relation of variables or constants. For example, the
following CE tests for the presence of the WME \st{(object type chair)}:

\begin{verbatim}
(object type chair)
\end{verbatim}

The following tests for the absence of the WME \st{(object type table)}:
 
\begin{verbatim}
-(object type table)
\end{verbatim}

Variables are indicated by angle brackets. When a CE contains a variable, it
is testing for a WME with any value in that position. For example, the
following CE will test for the presence of any WME that contains \st{object}
in the first position and \st{type} in the second position. So both of the
WMEs above will \emph{match} this CE:

\begin{verbatim}
(object type <x>)
\end{verbatim}

Variable names are arbitrary, so long as they are a valid identifier as
defined above. 

To match, a condition element and a WME must have the same number of attributes.
Hence the WME \st{(a b)} matches the CE \st{(a <x>)} but the WME \st{(a b c)}
does not.

A binary relation can test a variable against a constant or
another variable. For example the next CE will match any WME with
\st{object} in the first position, \st{number} in the second position, and
a numerical value less than 9 in the third position:

\begin{verbatim}
(object number (<x> < 9))
\end{verbatim}
 
A production is \emph{instantiated} by a subset of WM that satisfies its CEs
in such a way that the variables have consistent values throughout the LHS.
For example consider this LHS:

\begin{verbatim}
(problem object <x>)
(<x> type chair)
\end{verbatim}

The first CE will test for the presence of a WME with \st{problem} in the
first position, \st{object} in the second position, and any value in the
third position.

The second CE will test for the presence of any WME with any value in the
first position, \st{type} in the second position, and \st{chair} in the
third position.

If there is a subset of working memory with WMEs $W_1$ and $W_2$ where $W_1$
matches the first CE and $W_2$ matches the second CE and the value in the 
third position of $W_1$ is the same as the value in the first position of $W_2$,
then $\{W_1, W_2\} $ is an instantiation of this production. 

Hence \st{\{(problem object o1), (o1 type chair)\}}  is a valid instantiation
of the LHS above, but \st{\{(problem object o1), (o3 type chair)\}} is not.

CEs can contain the wildcard \st{<*>}. Like a variable, this can match any
value. Unlike a variable, the wildcard can match a different value for each
appearance. So \st{\{(problem object o1), (o3 type chair)\}} is a valid
instantiation of the LHS:

\begin{verbatim}
(problem object <*>)
(<*> type chair)
\end{verbatim}

In this case, the same behaviour could be achieved using two different
variables (e.g. \st{<x>} and \st{<y>}) instead of two wildcards, but wildcards
can be more efficiently implemented in a Street Engine.

The wildcard cannot appear in binary relations (e.g. \st{(A B (<*> > 7))}) but the
same behaviour can be achieved using a relation with no variable (e.g. 
\st{(A B >7)}). 

\subsubsection{Negated CEs}

In rules containing negated CEs, an instantiation is a subset of working
memory that satisfies all the positive CEs, while none of the negated CEs are
satisfied with the same variable bindings. For example consider this LHS:

\begin{verbatim}
(problem object <x>)
-(<x> type chair)
\end{verbatim}

If working memory is empty and then we add \emph{only} 
\(W_1=\texttt{(problem object o1)}\), then $\{W_1\}$ is a new
instantiation for this production and the rule will fire. If we then add
\(W_2=\texttt{(o1 type chair)}\) then $\{W_1\}$ \emph{is no longer} an
instantiation of the rule, because $W_2$, which matches the negated CE, has
the same \st{<x>} value as $W_1$. If we then delete
\st{(o1 type chair)}, $\{W_1\}$ is a new instantiation and hence the rule
will fire again. Finally, if we then add \(W_2=\texttt{(object753 type chair)}\),
$W_1$ \emph{remains} an instantiation, because the variable \st{<x>}
takes on different values in the WME that satisfies the positive CE and the
WME that satisfies the negated CE.

A variable in a negated CE must also appear in at least one positive CE;
however the wildcard can be used in negated CEs as in the following example
which will fire only when there are no WMEs of the form \st{(<x> hasA <*>)}
for a particular \st{(problem object <x>)}.

\begin{verbatim}
st {detectHasNothing
    (problem object <x>)
    -(<x> hasA <*>)
-->
    (<x> has nothing)
}
\end{verbatim}

\subsubsection{Relations in Condition Elements}
A condition element can contain the following tests at any attribute position
where \st{C} is any constant and \st{<x>} and \st{<y>} are any
variables.  
\begin{enumerate}
	\item \st{C}
	\item \st{!= C}
	\item \st{> C}
	\item \st{< C}
	\item \st{>= C}       
	\item \st{<= C}      
	\item \st{<x>}      
	\item \st{<*>}
	\item \st{(<x> != C)}
	\item \st{(<x> < C)}
	\item \st{(<x> > C)}
	\item \st{(<x> <= C)}
	\item \st{(<x> >= C)}
	\item \st{(<x> != <y>)}
\end{enumerate}

Note that binary relations with a variable on the left hand side must be
enclosed in parenthesis. This is required to avoid ambiguities. For example
\st{(myAgent <t> > 7)} is a CE with 3 attributes: \st{myAgent}, \st{<t>} and
\st{>7}. It would match the WME \st{(myAgent age 10)} but not 
\st{(myAgent 10)}. \st {(myAgent (<t> > 7))} is a CE with 2 attributes. It would
match \st{(myAgent 10)} but not \st{(myAgent age 10)}.

\subsubsection{Other Examples}

\begin{itemize}

\item \(WM = \{\texttt{(A B X), (X C D), (X E Y1), (X E Y2), (Y1 E Y2)}\}\)

The LHS:
\begin{verbatim}
(A B <x>)
(<x> C D)
(<x> E <y>)
\end{verbatim}
has two instantiations:
\(\{\texttt{(A B X), (X C D), (X E Y1)}\}\) and\\
\(\{\texttt{(A B X), (X C D), (X E Y2)}\}\).

\item \(WM = \{\texttt{(A B X), (X C D), (X E Y1), (X E Y2), (Y1 E Y2)}\}\)

The LHS:
\begin{verbatim}
(A B <x>)
(<x> C D)
-(<x> E <*>)
\end{verbatim}
has no instantiations.

\item \(WM = \{\texttt{(A B A), (A C D), (X E Y1), (X E Y2), (Y1 E Y2)}\}\)

The LHS:
\begin{verbatim}
(A B <x>)
(<x> C D)
-(<x> E <*>)
\end{verbatim}
has one instantiation: \(\{\texttt{(A B A), (A C D)}\}\)

\item Note that a single WME can satisfy more than one CE in a production.
For example the LHS:
\begin{verbatim}
(<x> B C)
(A <y> C)
(<x> D E)
\end{verbatim}
could be instantiated by two WMEs, such as \st{(A B C)} and \st{(A D E)}.

\end{itemize}

\subsection{Actions}
The RHS of a production contains actions, which specify changes to be made to
working memory. A normal action adds a WME to working memory and an action
preceded by a minus sign deletes a WME from working memory. For each
instantiation of a production's LHS, the RHS actions are executed once. If an
instantiation no longer exists (if some of its WMEs are deleted, or a WME is
added that matches a negated CE with the same variable bindings as an
instantiation), the changes made by the corresponding action \emph{are not}
automatically undone.

Each action specifies one WME to add or delete from working memory. The action
\st{(object type chair)} means, ``add the WME \st{(object type chair)} to
working memory'', and \st{-(object type chair)} means, ``delete this WME.''
Because working memory is a set, adding a WME that already exists, or deleting
a WME that does not exist has no effect. If an action contains variables with
the same names as appear in the LHS, they take on the value they are bound to
by the corresponding instantiation. If a variable does not appear in the LHS,
a new unique value is assigned for each execution of the RHS.

An action can contain binary operations between variables and variables, or
variables and constants. For example, if an instantiation of a rule has
\st{<x>} bound to the value 4, then the RHS:

\begin{verbatim}
(thing count <x>+1)
(<x>-1 C D)
\end{verbatim}

will result in the WMEs \st{(thing count 5)} and \st{(3 C D)} being added
to working memory. Operations with variables not assigned in the LHS, or with
variables bound to string values, are not defined (but may not crash the
simulator or even give an error message or warning).

\subsection{I/O}
Just as a microprocessor is connected to I/O peripherals, so is a Street Engine.
We say that the peripherals connected to a Street Engine form the
\emph{environment} of the agents running on the Street Engine.

Inputs to a Street Engine arrive as new WMEs of the form 
\st{(\_INPUT <a> <b> ...)}. The values of the attributes \st{<a>}, 
\st{<b>} etc..., and their meaning depend entirely on the device that sent the
input to the Street Engine. The \st{\_INPUT} constant can only appear as the
first attribute in a CE. It cannot be used at all in actions\footnote{This is
enforced so that peripherals can maintain a view of \st{\_INPUT} WMEs in
working memory that is consistent with the Street Engine's view. If, for
example, a Street agent could delete \st{\_INPUT} WMEs, then it could do so
without notifying the input device. The input device and the Street Engine
would no longer have a consistent view of the \st{\_INPUT} WMEs in working
memory.}. Input devices can both add and delete \st{\_INPUT} WMEs.

Outputs from a Street Engine to its environment are WMEs of the form 
\st{(\_OUTPUT <a> <b> ...)}. Peripheral devices are notified of the addition and
deletion of these WMEs. Of course how a particular device responds depends
entirely on that device. Street agents can add and delete
\st{\_OUTPUT} WMEs but the \st{\_OUTPUT} attribute can only appear as the
first attribute in actions; it cannot appear at all in CEs.

\section{Street Execution}

Street Language production rules are executed concurrently and asynchronously.
The actions from a production rule are communicated to the other production
rules but the order in which actions from different rules are communicated is
not defined. Similarly there are no guarantees about the time it takes for an
action to be communicated from one production rule to another\footnote{The
JavaStreet simulator follows a pattern of execution in which, at each step,
each rule executes buffered actions until there are no actions waiting or the
execution of an action produces one or more new instantiations.
\emph{BEWARE}: you should not expect any other Street Engine to follow this
pattern.}.

Because production rules execute asynchronously it is possible (easy, in fact)
for race conditions to occur. This means that identical production rules
with identical input conditions can produce different outputs on different
Street Engines, or even on different runs with the same Street Engine.

Street Language has three features that can be used for avoiding or resolving
races in situations where that is considered important: \emph{atomic actions},
\emph{local actions first}, and \emph{sequencing rules}.

\subsection{Atomic Actions}

Actions from a given RHS and corresponding to a given instantiation are
grouped together as an \emph{atomic action}. They act together to make a
single change to working memory rather than a series of changes. 

The actions in an atomic action arrive at a production rule together. They
will be performed in the order in which they are written on the RHS and will
not be interleaved with any other actions. The rule will apply all the actions
in the atomic action to working memory before checking for new instantiations.

Consider the following example:
\begin{verbatim}
st {rule1
    (A B C)
-->
    (D E F)
    -(D E F)
}

st {rule2
    (D E F)
-->
    (G H I)
}
\end{verbatim}

Imagine that we begin with working memory empty and then add the WME
\st{(A B C)}. \st{rule1} will fire. When its actions arrive at
\st{rule2}, \st{(D E F)} will be applied first; however
\st{rule2} will not search for new instantiations until \st{-(D E F)} is
applied. The atomic action makes no change to working memory and hence
\st{rule2} will not fire.

If two \emph{different} rules have conflicting actions (e.g. one adds a WME
that is deleted by another) and they have an instantiation at the same time,
the behaviour is undefined. The language makes no guarantee about the order in
which the actions are performed and a \emph{race} occurs. This could result in
a change to working memory, or no change to working memory, and different
rules could end up with different views of the state of working
memory\footnote{Again the JavaStreet simulator is deceptive in this regard.
Using the debugger you can print the current state of working memory. In any
other Street Engine there is not guaranteed to be a single, consistent view
of working memory.}. The same implementation of the Street Language may do
different things on different occasions.
 
\subsection{Local Actions First}

When a rule fires, it will apply it own actions to its own view of working
memory before any other actions from other rules. In other words, a rule
applies its local actions first.

For example, the rule below counts the number of WMEs in memory of the form
\st{(A item <i>)}. Consider the case when working memory initially contains
\st{(A count 0)} and then \st{(A item galoshes)} is added, quickly
followed by \st{(A item braces)}.

\begin{verbatim}
/* BEWARE: this rule is still not race-free if `(A count <c>)` arrives 
 * after the items. */
st {countItems
    (A item <i>)
    (A count <c>)
-->
    -(A item <i>)   // remove the item so we don't count it again
    -(A count <c>)  // remove the old count
    (A count <c>+1) // increment the count
}
\end{verbatim}

The rule will fire with \st{<i> = galoshes} and \st{<c> = 0}. Because
local actions are applied first, \st{(A count 0)} will be deleted and
\st{(A count 1)} will be added before the rule considers the addition of 
\st{(A item braces)} to working memory. When it does, it will fire with
\st{<i> = braces} and \st{<c> = 1}. The result will be that both the
galoshes and braces will be removed from working memory and the count will be
2 as we intend.

Without the local actions first policy, the rule could fire a second time
with \st{<i> = braces} and \st{<c> = 0} before its first set of actions
are applied. In this case both the galoshes and braces would be removed from
working memory, but the count would only be 1 instead of 2.

A consequence of always applying local actions first is that it is possible
for a rule to fully occupy itself so that it never acts upon changes from
other rules. For example, consider the following rule.
\begin{verbatim}
st {busyRule
    (system tick <t>)
    (system ticker go)
-->
    -(system tick <t>)
    (system tick <t>+1)
}
\end{verbatim}

Once this rule fires for the first time it will keep firing, incrementing the
tick value each time. Deleting \st{(system ticker go)} will not stop it
firing because the local actions incrementing the tick value will keep the rule
fully occupied and it will never see the action that deletes the 
\st{(system ticker go}) WME.

A single change to working memory can cause multiple new instantiations of a
rule. The rule will fire for all of these instantiations and produce a
separate atomic action for each. As always these atomic actions will be
applied locally first, but note that the order in which they will be applied
is not defined.

\emph{Beware} that it is easy to accidentally write rules where a single
change to working memory creates multiple new instantiations that trigger
conflicting actions despite the local actions first policy.
The \st{countItems} rule above suffers from a race of this kind. If
\st{(A count 0)} arrives at the rule after \st{(A item galoshes)} and
\st{(A item braces)}, then when \st{(A count 0)} does arrive, there will be
2 new instances of the rule, both with \st{<c> = 0}. The actions for both
instances will fire and, effectively, only one of the items will be counted.
Sequencing rules solve this problem.

\subsection{Sequencing Rules}

A sequencing rule is a special form of production rule, indicated by the
modified arrow, \st{\textasciitilde\textasciitilde >}. If a change to working
memory. When the LHS of a sequencing rule matches working memory, it will fire
its RHS actions as usual; however if a single (atomic) change to working
memory causes multiple matches, it will only fire its actions for one of those
matches. Which match it chooses to fire is arbitrary.

As noted above, the \st{countItems} rule would not work correctly when the
initial WME \st{(A count 0)} arrived after the items to count. We can
resolve this race by making it a sequencing rule as below:

\begin{verbatim}
/* This rule is race-free */
st {countItems
    (A item <i>)
    (A count <c>)
~~>                 // NOTE: a sequencing rule
    -(A item <i>)   // remove the item so we don't count it again
    -(A count <c>)  // remove the old count
    (A count <c>+1) // increment the count
}
\end{verbatim}

This time, if \st{(A count 0)} arrives at the rule after 
\st{(A item galoshes)} and \st{(A item braces)}, then there will still be
two matches of the rule with \st{<c> = 0}, but the actions for only one will
fire. Lets assume that the actions are fired for
\st{<i> = galoshes} and \st{<c> = 0}. This will cause another new
instantiation of the rule, this time with \st{<c> = 1} and
\st{<i> = braces}. After its actions are taken both the galoshes and the
braces will have been removed from memory, and the count will be 2 as we would
like.

It is expected that sequencing rules will only be used occasionally, when it
is necessary to resolve a race.

\section{Knowledge Representation and Programming Paradigms}

\emph{Knowledge representation} is how an agent's data is arranged in working
memory. A structured approach to writing agent production rules might be called
a \emph{programming paradigm}.

It is early days for the Street Language and we have not yet experimented with
many knowledge representations or programming paradigms.

In Soar, WMEs always have 3 attributes and form a graph in working memory. A
similar approach can be used in Street although using 4 attributes can improve
the implementation as explained below. Each WME specifies an edge on a graph.
The first attribute is a name for the graph, the second is the head node of
the edge, the third is a label for the edge and the fourth is the tail node.
Hence the subset of working memory consisting of the WMEs below could be drawn
as the graph shown. 

\adjustbox{valign=t}{\begin{minipage}{0.45\linewidth}
\vspace{0.5cm}
\begin{verbatim}
(people person _ID1)
(people person _ID2)
(people _ID1 name Fred)
(people _ID1 age 40)
(people _ID2 name Valerie)
(people _ID2 size small)
(people _ID2 pets _ID3)
(people _ID3 cat Fluffy)
\end{verbatim}
\end{minipage}}%
\hspace{0.5cm}
\adjustbox{valign=t}{\begin{minipage}{0.45\linewidth}
\begin{center}
\includegraphics[width=\linewidth]{WorkingMemoryExample.pdf}
\end{center}
\end{minipage}}

The graph could be built with WMEs with just 3 attributes, but traversing
such a graph can require rules with condition elements of the form 
\st{(<head> <attribute> <value>)}. Because these match every 3-attribute
WME in working memory, the memory associated with the condition element
(its `alpha memory') becomes very large but contains few WMEs that will ever
participate in an instantiation. It fills up with irrelevant information. Using
a constant for the first attribute solves this problem.

While graphs data structures may prove to be useful, they are not a fixed part
of Street Language. Other data structures are possible and may be more
appropriate for a given problem.

\newpage
\bibliographystyle{IEEEtran}
\bibliography{IEEEabrv,StreetLanguage}

\appendix

\section{ANTLR4 Grammar Definition}
\label{sec:grammar}

\verbatiminput{../src/main/antlr/StreetLanguage.g4}

\end{document}

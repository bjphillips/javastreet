/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;
import java.util.concurrent.Semaphore;
import java.util.concurrent.locks.ReentrantLock;

/**
 * A Threaded Agent is an instance of a Street interpreter running in its own
 * thread.
 *
 * The public methods in the ThreadedAgent class are synchronized and can be
 * called at any time from any other thread.
 *
 * External environments should instantiate a ThreadedAgent and only access the
 * underlying agent indirectly via the public methods in this class. These
 * protect the shared variables with the interfaceLock.
 *
 * The various debuggers, on the other hand, need to access the underlying
 * agent. To do this they should first take the agentMutex and give it when they
 * are done.
 *
 */
public class ThreadedAgent extends Agent implements Runnable {

	private boolean terminate = false;
	private boolean paused = false;
	private int step = 0;
	private final Object interfaceLock = new Object();
	private final Semaphore agentMutex = new Semaphore(1);
	private final List<TokenPacket> inputTokenPackets = new ArrayList<>();
	private final Thread myThread = new Thread(this);
	private final List<Peripheral> peripherals = new ArrayList<>();

	/**
	 * The 'execution loop' for the agent. Note that this runs on a thread of
	 * its own and hence needs to be synchronised with the other methods below.
	 */
	@Override
	public void run() {
		while (!terminate) {

			// yield execution to give other threads a chance
			try {
				Thread.sleep(0,1);
			} catch (InterruptedException ex) {
			}

			synchronized (interfaceLock) {
				boolean takeStep = (step > 0) || (!paused && !this.isIdle)
						|| (!paused && !inputTokenPackets.isEmpty());

				// wait for a signal (could occasionally be spurious) and then try again
				// it is important to make sure termonate has not been set otherwise we
				// we may wait and never be notified
				if (!takeStep && !terminate) {
					try {
						interfaceLock.wait();
					} catch (InterruptedException ex) {
					}
				}

				// step the agent
				if (takeStep && !terminate) {
					// add any inputTokenPackets to the agent
					for (TokenPacket tokenPacket : inputTokenPackets) {
						this.addInput(tokenPacket);
					}
					inputTokenPackets.clear();
					// step cycle
					takeMutex();
					this.stepCycle();
					giveMutex();
					// decrement the step counter
					step = (step > 0) ? step - 1 : 0;
					// for each output token, fire any registered output handlers
					for (TokenPacket tokenPacket : this.getOutputs()) {
						for (Peripheral peripheral : peripherals) {
							peripheral.handleOutput(tokenPacket);
						}
					}
				}
			}
		}
	}

	public void start() {
		myThread.setName("Threaded Agent");
		myThread.start();
	}

	public void terminate() {
		synchronized (interfaceLock) {
			terminate = true;
			interfaceLock.notify();
		}
		try {
			myThread.join();
		} catch (InterruptedException ex) {
			// do nothing - we are terminating anyway
		}
	}

	public void pause() {
		synchronized (interfaceLock) {
			paused = true;
		}
	}

	public void resume() {
		synchronized (interfaceLock) {
			paused = false;
			interfaceLock.notify();
		}
	}

	public void step(int s) {
		synchronized (interfaceLock) {
			if (s > 0) {
				this.step += s;
			}
			interfaceLock.notify();
		}
	}

	public boolean isBusy() {
		boolean busy;
		synchronized (interfaceLock) {
			busy = (step > 0);
		}
		return busy;
	}

	public void pushInput(TokenPacket tokenPacket) {
		synchronized (interfaceLock) {
			inputTokenPackets.add(tokenPacket);
			interfaceLock.notify();
		}
	}

	public void takeMutex() {
		try {
			agentMutex.acquire();
		} catch (InterruptedException ex) {
			// This should not happen
			System.err.println("Attempt to take the agent mutex unexpectedly interrupted.");
		}
	}

	public void giveMutex() {
		agentMutex.release();
	}

	public void addPeripheral(Peripheral peripheral) {
		synchronized (interfaceLock) {
			peripherals.add(peripheral);
		}
	}

	// Notify the main loop to wake it from suspension and deal with any
	// changes
	public void signalEvent() {
		synchronized (interfaceLock) {
			interfaceLock.notify();
		}
	}
}

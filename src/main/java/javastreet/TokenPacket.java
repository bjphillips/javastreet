/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.ArrayList;

/**
 * A TokenPacket is the list of tokens produced by one instantiation of a RHS.
 * A productor treats the tokens in a TokenPacket as a single atomic change
 * to working memory.
 * 
 */
public class TokenPacket extends ArrayList<Token> {
	public TokenPacket() {
		super();
	}
	
	public TokenPacket(Token token) {
		super();
		this.add(token);
	}
	
	public TokenPacket(String[] a, boolean add) {
		super();
		this.add(new Token(a, add));
	}

	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (Token t: this) {
			s.append(t.toString()).append("\n");
		}
		if (!this.isEmpty()) {
			s.append("--\n");
		}
		return s.toString();
	}
}

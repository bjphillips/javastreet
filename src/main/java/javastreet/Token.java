/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

public class Token extends WME {

	protected TokenFlag flag;
	
	Token(WME m, TokenFlag f) {
		super(m.attributes);
		attributes = m.attributes.clone();
		flag = f;
	}
	
	public Token(String[] a, boolean add) {
		super(a);
		if (add) {
			flag = TokenFlag.ADD;
		} 
		else {
			flag = TokenFlag.DELETE;
		}
	}

	public TokenFlag getFlag() {
		return flag;
	}
	
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("(");
		for (String attribute : this.attributes) {
			s.append(attribute).append(" ");
		}
		s.append(flag.toString()).append(")");
		return s.toString();
	}
}
/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.Arrays;

public class WME {
	protected String[] attributes;
	
	public WME(String[] a) {
		attributes = a.clone();
	}

	public String getAttribute(int i) {
		return attributes[i];
	}
	
	public void setAttribute(int i, String a) {
		attributes[i] = a;
	}
	
	public int numberOfAttributes() {
		return attributes.length;
	}

	// A WME equals another if they have the same number of attribtes and the
	// attributes are the same
	@Override
	public boolean equals(Object o) {
		if (! (o instanceof WME)) {
			return false;
		}		
		WME w = (WME) o;		
		if (this.attributes.length != w.attributes.length) {
			return false;
		}
		for (int i = 0; i < this.attributes.length; i++) {
			if (!this.attributes[i].equals(w.attributes[i])) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 53 * hash + Arrays.deepHashCode(this.attributes);
		return hash;
	}

	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("(");
		for (int i=0; i<this.attributes.length; i++) {
			s.append(this.attributes[i]);
			if (i<this.attributes.length-1) {
				s.append(" ");
			}
		}
		s.append(")");
		return s.toString();
	}
}
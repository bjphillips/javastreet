/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import javastreet.peripherals.*;

/**
 * A simple environment that loads production rule file and steps it for a number of
 * cycles. The rule file and number of cycles are specified on the command line.
 */
class CommandLineRunner implements AgentController, MessageHandler {
    
    private final ThreadedAgent agent;
    private final Object stdoutLock = new Object();
	private int exitCode;
	private Runnable exitHandler;
    
    public CommandLineRunner(ThreadedAgent agent) {
        this.agent = agent;
        agent.setMessageHandler(this);
		exitCode = 0;
    }

    @Override
    public void runController(String[] args)  {
		
        // set up the agent
        agent.pause();
        agent.start();
        agent.logNewTraffic = true;
        agent.logIOTraffic = true;
        agent.logFirings = true;
        agent.logCycles = true;
		
		// report current version
		System.out.println("JavaStreet Version "+Version.NUMBER);

		// plug in some peripherals
		new RandomNumberPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new PrintPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new TimerPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new ExitPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent

        // load the productions into the agent
        if(args.length == 0) {
            System.err.println("Specify the Street source file on the command line.");
			exit(1);
        }
        if (!agent.loadFile(args[0])) {
            System.err.println("Giving up.");
			exit(1);
		}

        // Take the second command line argument to be a number of cycles to run
        int steps = 25;
        if(args.length == 2) {
            try {
                steps = Integer.parseInt(args[1]);
            } catch (NumberFormatException ex) {
                System.err.println("Unable to interpret the second argument as a number of cycles to run: "+args[1]);
				exit(1);
            }
        }

        // start the agent
        agent.init();
		String[] t = {"_INPUT", "system", "reset"};
        agent.addInput(new TokenPacket(t, true));

        // run the agent
        System.out.println("Stepping the agent for "+steps+" cycles");
        agent.step(steps);
        while (agent.isBusy()) {
			// Busy loop
		}

        // dump the results
        System.out.println("\nWorking memory is now:");
        System.out.println(agent.getWorkingMemory().toString());

        // Bye
		exit(0);
    }
    
	@Override
	public void setExitHandler(Runnable exitHandler) {
		this.exitHandler = exitHandler;
	}
	
	@Override
	public int getExitCode() {
		return exitCode;
	}

    @Override
    public void handleMessage(String message) {
        synchronized (stdoutLock) {
            System.out.print(message);
        }
    }
	
	void exit(int exitCode) {
		this.exitCode = exitCode;
		if (exitHandler != null) {
			exitHandler.run();
		}
	}

}

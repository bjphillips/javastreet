/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

class AlphaBlock {

	private final ConditionElement conditionElement;
	private final List<CAMWord> cam;
	private boolean criticalChange;
	private int matchPairIndex;

	AlphaBlock(ConditionElement conditionElement) {
		this.conditionElement = conditionElement;
		cam = new ArrayList<>();
		criticalChange = false;
	}

	void clear() {
		cam.clear();
	}

	boolean isNegated() {
		return conditionElement.isNegated();
	}
	
	boolean getCriticalChange() {
		return criticalChange;
	}

	// Returns the number of words in a cam (including any marked as deleted)
	int size() {
		return cam.size();
	}

	ConditionElement getConditionElement() {
		return conditionElement;
	}
	
	/**
	 * Returns a match pair corresponding to the first added WME in a positive
	 * alpha block or the first deleted WME in a negated alpha block and marks
	 * it as searched; returns null if there is none.
	 */
	MatchPair searchFirstNewMatchPair() {
		matchPairIndex = -1;
		return searchNextNewMatchPair();
	}
	
	/**
	 * Returns the match pair corresponding to the next added WME in a positive
	 * alpha block or a the next deleted WME in a negated alpha block and marks
	 * it as searched; returns null if there is none.
	 */
	MatchPair searchNextNewMatchPair() {
		for (matchPairIndex++; matchPairIndex<cam.size(); matchPairIndex++) {
			if (!isNegated() && cam.get(matchPairIndex).added) {
				cam.get(matchPairIndex).searched = true;
				return new MatchPair(conditionElement, cam.get(matchPairIndex).wme);
			}
			if (isNegated() && cam.get(matchPairIndex).deleted) {
				cam.get(matchPairIndex).searched = true;
				return new MatchPair(conditionElement, cam.get(matchPairIndex).wme);				
			}
		}
		return null;
	}
	
    /** Insert a token to this alpha block.
	 * Only inserts the token if it passes all constant tests.
     * Marks matching words in memory as added or deleted.
     * - if adding and the word was not in memory, add it to memory and mark it
     *   as added and not deleted
     * - if adding and the word is added and not deleted, leave it unchanged
     * - if adding and the word is deleted and not added, mark it as not added 
         and not deleted
     * - if adding and the word is not added and not deleted, leave it unchanged
     * - if deleting and the word was not in memory, leave it unchanged
     * - if deleting and the word is added and not deleted, remove it form memory
     * - if deleting and the word is deleted and not added, leave it unchanged
     * - if deleting and the word is not added and not deleted, mark it as 
	 *   deleted and not added 
	 */
	void insertToken(Token token) {
		if (!conditionElement.constantTest(token)) {
			return;
		}
				
		// ADD Token
		if (token.getFlag() == TokenFlag.ADD) {
			// see if the token matches a word in memory
			CAMWord word = null;
			for (CAMWord testWord: cam) {
				if (token.equals(testWord.wme)) {
					word = testWord;
					break;
				}
			}

			//adding and the word was not in memory
			if (word == null) {
				word = new CAMWord(token);
				word.added = true;
				cam.add(word);
				return;
			}
			// adding and the word is deleted and not added
			if (word.deleted && !word.added) {
				word.deleted = false;
				return;
			}
			// adding and the word is added and not deleted OR
			// adding and the word is not added and not deleted
			return;
		}
		
		// DELETE Token
		for (CAMWord word: cam) {
			if (word.wme.equals(token)) {
				// deleting and the word is not added and not deleted
				if (!word.deleted && !word.added) {
					word.deleted = true;
					return;
				}
				// deleting and the word is added and not deleted
				else if (word.added && !word.deleted) {
					cam.remove(word);
					return;
				}
			}
		}
	}
	
	/** 
	 * Used after applying an atomic action but before searching for new
	 * instances.
	 * - removes all words marked as deleted from positive alpha block
	 * - sets critical change if there is an added word in a positive alpha block
	 * - sets critical change if there is a deleted word in a negated alpha block
	 */
	void preTwiddleFlags() {
		criticalChange = false;
		Iterator camIterator = cam.iterator();	// need an iterator as elements are deleted
		while (camIterator.hasNext()) {
			CAMWord word = (CAMWord) camIterator.next();
			if (word.deleted) {
				if (this.isNegated()) {
					criticalChange = true;
					return;
				}
				else {
					camIterator.remove();
				}
			}
			if (word.added && !this.isNegated()) {
				criticalChange = true;
			}
		}
	}

	/**
	 * Used after searching for new instances.
	 * Clears the searched and added flags on all words.
	 * Deletes and words marked as deleted.
	 * Clears the cricialChange flag.
	 */
	void postTwiddleFlags() {
		criticalChange = false;
		Iterator camIterator = cam.iterator();	// need an iterator as elements are deleted
		while (camIterator.hasNext()) {
			CAMWord word = (CAMWord) camIterator.next();
			if (word.deleted) {
				camIterator.remove();
			}
			else {
				word.added = false;
				word.searched = false;
			}
		}
	}

	/**
	 * Adds match pairs from this WMEs in this alpha block to a partial instance
	 * to create a list of new partial instances. All of the new partial
	 * instances returned may eventually lead to annotated instances of a rule
	 * once all the rules alpha blocks have been added. The returned list may be
	 * empty.
	 */
	PartialInstanceList addTo(PartialInstance partialInstance) {
		PartialInstanceList returnList = new PartialInstanceList();

		/* If the condition element is positive then we create a new partial 
		 * instance for each match pair from the alpha block that joins with 
		 * all the match pairs in the partial instance. The new partial 
		 * instances are added to the set of partial instances to consider 
		 * at the next iteration. 
		 */
		if (!conditionElement.isNegated()) {
			for (CAMWord word : cam) {
				/* If we add a searched match pair from a positive 
				 * condition element then it will only result in duplicate 
				 * instances.
				 *
				 * We assume all deleted WMEs have been removed from positive
				 * alpha blocks before this step.
				 */
				if (!word.searched) {
					MatchPair matchPair = new MatchPair(this.conditionElement, word.wme);
					if (partialInstance.joinsAllMatchPairs(matchPair)) {
						PartialInstance newPartialInstance = partialInstance.getClone();
						newPartialInstance.addMatchPair(matchPair);
						returnList.add(newPartialInstance);
					}
				}
			}
			return returnList;
		}
		
		/* If the condition element is negated then if any of the match pairs 
		 * from its alpha block join with all the match pairs in the partial 
		 * instance then the partial instance will not lead to any instances 
		 * and should be discarded. 
		 * If none of the match pairs from the alpha block join with the partial 
		 * instance then the partial instance as it is can be added to the set 
		 * of partial instances to be considered next iteration.
		 */
		for (CAMWord word : cam) {
			/* We consider the current state of working memory and hence
			 * WMEs marked as deleted are ignored. (They are still in the 
			 * alpha blocks because they may be used to start new searches.)
			 */
			if (!word.deleted) {
				MatchPair matchPair = new MatchPair(this.conditionElement, word.wme);
				if (partialInstance.joinsAllMatchPairs(matchPair)) {
					return returnList;	// returns an empty list
				}
			}
		}
		PartialInstance newPartialInstance = partialInstance.getClone();
		returnList.add(newPartialInstance);
		return returnList;
    }

    // Returns a String representation of the contents of this cam
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("Alpha memory for CE: ").append(conditionElement.toString()).append("\n");
		for (int i = 0; i < cam.size(); i++) {
			s.append(i).append(": ").append(cam.get(i).toString()).append("\n");
		}
		return s.toString();
	}

	/**
	 * Inner class representing a word in the cam, consists of a WME and 
	 * some flags.
	 * All the WMEs in a cam have the same number of attributes.
	 */
	class CAMWord {

		WME wme;
		boolean deleted;
		boolean added;
		boolean searched;

		CAMWord(WME wme) {
			String[] attributes = new String[wme.numberOfAttributes()];
			for (int i=0; i<attributes.length; i++) {
				attributes[i] = wme.getAttribute(i);
			}
			this.wme = new WME(attributes);
			deleted = false;
			added = false;
			searched = false;
		}

		// Returns a string representation of this word
		@Override
		public String toString() {
			return wme.toString() + ", added: " + added + ", deleted: " + deleted +", searched: " + searched;
		}
	}
}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

/**
 * An agent sends message strings to a messageHandler function.
 * What the message handler does with those string is entirely up to its own
 * discretion. Note that handleMessage may be called simultaneously from
 * multiple threads and hence it must manage any required synchronization internally.
 */
interface MessageHandler {
    default public void handleMessage(String message) {
        // do nothing by default
    };
}

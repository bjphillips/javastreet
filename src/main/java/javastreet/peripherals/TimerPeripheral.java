/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet.peripherals;

import java.util.*;
import javastreet.*;

/**
 * The timer peripheral can be used to schedule an input event for a number of
 * milliseconds in the future. It responds to a token:
 * (_OUTPUT, timer, <handle>, <ms>, ADD) 
 *
 * The timer will respond, after a delay of <ms> milliseconds with:
 * (_INPUT, timer, <handle>, ADD)
 *
 * The time will not produce another 
 * (_INPUT, timer, <handle>, ADD) token for the same <handle>
 * again until the agent sends
 * (_OUTPUT, timer, <handle>, DELETE)
 * to which the timer will respond with 
 * (_INPUT, timer, <handle>, DELETE)
 *
 */
public class TimerPeripheral extends Peripheral {

    private final List<String> timerHandles;
	private final static String TIMER = "timer";

    public TimerPeripheral(ThreadedAgent a) {
        super(a);
        timerHandles = new ArrayList<>();
    }

    @Override
    public void handleOutput(TokenPacket tokenPacket) {
		for (Token token : tokenPacket) {

			// if this is (_OUTPUT, timer, <handle>, DELETE) remove 
			// <handle> from the timerHandles list and send (_INPUT, timer, <handle>, DELETE).
			if (token.getFlag().equals(TokenFlag.DELETE)
					&& token.numberOfAttributes() == 3
					&& TIMER.equals(token.getAttribute(1))) {
				String oldhandle = token.getAttribute(2);
				if (timerHandles.contains(oldhandle)) {
					timerHandles.remove(oldhandle);
					String[] t = {"_INPUT", TIMER, oldhandle};
					this.getAgent().pushInput(new TokenPacket(t, false));
				}
				return;
			}

			// if this is (_OUTPUT, timer, <handle>, <ms>, ADD) 
			// remember <handle> 
			if (token.getFlag().equals(TokenFlag.ADD)
					&& token.numberOfAttributes() == 4
					&& TIMER.equals(token.getAttribute(1))) {
				String handle = token.getAttribute(2);
				int ms = 1;
				try {
					ms = Integer.parseInt(token.getAttribute(3));
				} catch (NumberFormatException ex) {
					// do nothing. Just leave ms at the default value.
				}
				// only schedule a new timer if <handle> is not in the timerHandles list
				if (!timerHandles.contains(handle)) {
					// schedule a thread to send a message in a while
					(new DelayedInput(handle, ms)).start();
					timerHandles.add(handle);
				}
			}
		}
    }

    /**
     * Private inner class to provide a thread that will will sleep until a
     * specified time in the future before sending an input to the Street
     * Engine.
     */
    private class DelayedInput extends Thread {

        private final String handle;
        private final int ms;

        DelayedInput(String handle, int ms) {
            this.handle = handle;
            this.ms = ms;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(ms);
            } catch (InterruptedException ex) {
                // this should not happen - we were not expecting any interruptions!
            }
			String[] t = {"_INPUT", TIMER, handle};
			getAgent().pushInput(new TokenPacket(t, true));
        }
    }

}

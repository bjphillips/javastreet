/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet.peripherals;

import javastreet.*;

/**
 * The print peripheral responds to output tokens of the form
 * (_OUTPUT, stdout, <v>, ADD)
 * (_OUTPUT, stderr, <v>, ADD)
 * (_OUTPUT, log, <v>, ADD)
 * by printing "The agent says: <v>\n" to stdout, stderr or the log respectively.
 */
public class PrintPeripheral extends Peripheral {

    public PrintPeripheral(ThreadedAgent a) {
        super(a);
    }

    @Override
    public void handleOutput(TokenPacket tokenPacket) {
		
		for (Token token : tokenPacket) {
			if (!token.getFlag().equals(TokenFlag.ADD)) {
				continue;
			}
			String destination = token.getAttribute(1);
			if (!"stdout".equals(destination) &&
					!"stderr".equals(destination) &&
					!"log".equals(destination)) {
				continue;
			}
			StringBuilder message = new StringBuilder("The agent says: ");
			for (int i=2; i<token.numberOfAttributes(); i++) {
				message.append(token.getAttribute(i)).append(" ");
			}
			switch (destination) {
				case "stdout":
					System.out.println(message.toString());
					break;
				case "stderr":
					System.err.println(message.toString());
					break;
				case "log":
					getAgent().sendMessage(message.toString()+"\n");
					break;
				default:
					// do nothing
					break;
			}
		}
    }
}
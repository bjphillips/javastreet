/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet.peripherals;

import javastreet.*;

/**
 * The exit peripheral allows the agent to request the simulator terminate or
 * pause. The pause option us useful for setting breakpoints in the agent.
 * (_OUTPUT, exit, <status>, ADD) or (_OUTPUT, pause, <stauts>, ADD)
 */
public class ExitPeripheral extends Peripheral {

    public ExitPeripheral(ThreadedAgent a) {
        super(a);
    }

    @Override
    public void handleOutput(TokenPacket tokenPacket) {
		for (Token token : tokenPacket) {
			// if this is (_OUTPUT, exit, <status>, ADD)
			// print a message and request termination
			if (token.getFlag().equals(TokenFlag.ADD)
					&& "exit".equals(token.getAttribute(1))) {
				System.out.println("The agent requests an exit with status: " + token.getAttribute(2));
				// This is an ugly way to end things...
				System.exit(0);
			}
			// if this is (_OUTPUT, pause, <status>, ADD)
			// print a message and request termination
			if (token.getFlag().equals(TokenFlag.ADD)
					&& "pause".equals(token.getAttribute(1))) {
				getAgent().sendMessage("The agent requests a pause in execution with status: " + token.getAttribute(2)+"\n");
				getAgent().pause();
			}
		}
	}
}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet.peripherals;

import java.util.*;
import javastreet.*;

/**
 * The random number generator responds to a token
 * (_OUTPUT, random, <handle>, <min>, <max>, ADD)
 *
 * The random number generator will respond with a 
 * (_INPUT, random, <handle>, <value>, ADD) token where <value> is the
 * random number between <min> and <max> inclusive.
 *
 * The random number generator will not produce another 
 * (_INPUT, random, <handle>, <value>, ADD) token again until the agent sends
 * (_OUTPUT, random, <handle>, DELETE)
 * to which the random number generator will respond with 
 * (_INPUT, random, <handle>, <value>, DELETE)
 *
 * To set the seed of the random number generator
 * (_OUTPUT, randomSeed, <seed>)
 */
public class RandomNumberPeripheral extends Peripheral {

    private final Map<String, Integer> handleResults;
    private final Random rand;
	private final static String RANDOM = "random";
	private final static String RANDOM_SEED = "randomSeed";

    public RandomNumberPeripheral(ThreadedAgent a) {
        super(a);
        rand = new Random();
        handleResults = new HashMap<>();
    }

    @Override
    public void handleOutput(TokenPacket tokenPacket) {
		for (Token token : tokenPacket) {

			// if this is (_OUTPUT, randomSeed, <seed>) then reseed the random
			// number generator
			if (token.getFlag().equals(TokenFlag.ADD)
					&& token.numberOfAttributes()==3
					&& RANDOM_SEED.equals(token.getAttribute(1))) {
				long seed;
				try {
					seed = Long.parseLong(token.getAttribute(2));
				} catch (NumberFormatException ex) {
					seed = 0;
				}
				rand.setSeed(seed);
			}
			
			// if this is (_OUTPUT, random, <handle>, DELETE) remove 
			// <handle> from the HashMap and send (_INPUT, random, <handle>, <value>, DELETE).
			if (token.getFlag().equals(TokenFlag.DELETE)
					&& token.numberOfAttributes()==3
					&& RANDOM.equals(token.getAttribute(1))) {
				String handle = token.getAttribute(2);
				if (handleResults.containsKey(handle)) {
					int value = handleResults.get(handle);
					handleResults.remove(handle);
					String[] t = {"_INPUT", RANDOM, handle, Integer.toString(value)};
					this.getAgent().pushInput(new TokenPacket(t, false));
				}
			}

			// if this is (_OUTPUT, random, <handle>, <min>, <max>, ADD)
			// remember this is the last <handle> received
			if (token.getFlag().equals(TokenFlag.ADD)
					&& token.numberOfAttributes()==5
					&& RANDOM.equals(token.getAttribute(1))) {
				String handle = token.getAttribute(2);
				int min, max;
				try {
					min = Integer.parseInt(token.getAttribute(3));
					max = Integer.parseInt(token.getAttribute(4));
				} catch (NumberFormatException ex) {
					min = 0;
					max = 10;
				}
				if (!handleResults.containsKey(handle)) {
					int result = rand.nextInt((max - min) + 1) + min;
					handleResults.put(handle, result);
					String[] t = {"_INPUT", RANDOM, handle, Integer.toString(result)};
					this.getAgent().pushInput(new TokenPacket(t , true));
				}
			}
		}
	}
}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.io.FileNotFoundException;

/**
 * The main entry point for the JavaStreet simulator creates an AgentController
 * and starts it executing with the runController method.
 * 
 * The ConsoleDebuger, the CommandLineRunner and the GUIDebugger are AgentControllers.
 * 
 * In the GUIDebugger, runController launches a separate thread for the 
 * debugger and returns. 
 * 
 * The CommandLineRunner and ConsoleDebugger only return from runController
 * once the debugger exits.
 * 
 * The exitHandler mechanism allows for consistent handling of these different
 * cases. The handler will be called when the agent controller exits.
 */
public interface AgentController {
	public void runController(String[] args) throws FileNotFoundException;	
	public void setExitHandler(Runnable exitHandler);
	public int getExitCode();
}

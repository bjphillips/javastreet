/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

enum TestType {
	EQ, NE, GT, LT, GTE, LTE, EQV, NEV;

	static TestType stringToTest(String s) {
		switch(s) {
			case "<":
				return LT;
			case ">":
				return GT;
			case ">=":
				return GTE;
			case "<=":
				return LTE;
			case "!=":
				return NE;
			default:
				return EQ;
		}
	}

	@Override
	public String toString() {
		switch (this) {
			case LT:
				return "<";
			case GT:
				return ">";
			case LTE:
				return "<=";
			case GTE:
				return ">=";
			case EQ:
			case EQV:
				return "=";
			case NE:
			case NEV:
				return "!=";
			default:
				return "";
		}
	}
}
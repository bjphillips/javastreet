/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.io.InputStream;
import java.util.*;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.*;
import streetlanguage.*;

/**
 * An Agent represents a single instance of a Street interpreter. An instance of
 * Agent has its own independent memory and other structures.
 *
 * External environments should instantiate a ThreadedAgent instead of an Agent.
 * The ThreadedAgent interface is cleaner and safer.
 */
class Agent {

    private final List<Productor> productorList;
    private final WorkingMemory workingMemory;
	private final TokenPacket debugBuffer;
	private final List<TokenPacket> outputBuffer;
    private final List<TokenPacket> inputBuffer;
    private int idCount = 0;
    private int cycleCount = 0;
    private MessageHandler messageHandler;
    protected boolean logDebugTraffic = false;
    protected boolean logNewTraffic = false;
    protected boolean logIOTraffic = false;
    protected boolean logFirings = false;
    protected boolean logCycles = false;
	protected boolean isIdle = false;

    Agent() {
        productorList = new ArrayList<>();
        workingMemory = new WorkingMemory();
        outputBuffer = new ArrayList<>();
        inputBuffer = new ArrayList<>();
		debugBuffer = new TokenPacket();
		messageHandler = null;
    }

    /**
     * Register a MessageHandler object that provides a handleMessage(String)
     * method.
     */
    void setMessageHandler(MessageHandler messageHandler) {
        this.messageHandler = messageHandler;
    }
    
    /**
     * Send a message to the messageHandler.
     */
    public void sendMessage(String message) {
        if (messageHandler != null) {
            messageHandler.handleMessage(message);
        }
    }

    /**
     * Adds a production to this agent. Writes all WorkingMemory to the production input.
	 * Note: this does not update the 'state' of the production until it is
	 * cycled, see {@ Production}.
     *
     * @param production The production to add
     */
    void addProduction(Production production) {
        // delete any existing productor of the same name
		if (this.hasProduction(production.getName())) {
			productorList.remove(this.getProductor(production.getName()));
		}
		// create new productor
        Productor productor = new Productor(production, this);
        productorList.add(productor);
        // if WorkingMemory is not empty, initialise productor with
        // new WorkingMemory elements
		TokenPacket a = new TokenPacket();
        for (WME w : workingMemory) {
			a.add(new Token(w, TokenFlag.ADD));
        }
		productor.writeTokenPacket(a);
		// flag the agent as not idle
		isIdle = false;
        sendMessage("Added Production: " + production.getName() + "\n");
    }

    /**
     * Clears all productors (empties their alpha memories), working memory
	 * and agent buffers. Also clears cycle counter and id counter.
     */
    void init() {
        for (Productor productor : productorList) {
            productor.clear();
        }
		workingMemory.clear();
		debugBuffer.clear();
		outputBuffer.clear();
		inputBuffer.clear();
		idCount = 0;
		cycleCount = 0;
		isIdle = true;
    }
	
    /**
     * Deletes all productors, clears working memory and agent buffers.
     */
	void reset() {
		productorList.clear();
		init();
	}

    /**
     * Steps the agent for one cycle 
	 * - read tokens in the debug buffer into each productor's incoming buffer
	 * - read tokens in the input buffer into each productor's incoming buffer
	 * - cycle each productor
	 * - read tokens from each productor's outgoing buffer into the
	 *   incoming buffers of all other productors
	 * - read _OUTPUT tokens into the output buffer
	 * 
	 * Sets isIdle at the end of the cycle if all buffers productors in all 
	 * productors are empty.
     */
    void stepCycle() {
        StringBuilder log = new StringBuilder();
		boolean isIdleSoFar = true;
        
        if (logCycles) {
            log.append("\nSTEPPING, CYCLE: " + cycleCount + "\n================================" + "\n");
        }

        // clear any outputs from the last cycle
        outputBuffer.clear();
		
		// read tokens in the debug buffer into each productor's incoming buffer
		if (logDebugTraffic) {
            log.append("Debug tokens:\n");
			log.append(debugBuffer.toString());
		}
		for (Productor p : productorList) {
			p.writeTokenPacket(debugBuffer);
		}
		for (Token t: debugBuffer) {
			workingMemory.addToken(t);
		}
        debugBuffer.clear();
		
        // read tokens in the input buffer into each productor's incoming buffer
        if (logIOTraffic) {
            log.append("Input tokens:\n");
        }
        for (TokenPacket a : inputBuffer) {
            if (logIOTraffic) {
                log.append(a.toString());
            }
            for (Productor pr : productorList) {
                pr.writeTokenPacket(a);
            }
			for (Token t: a) {
	            workingMemory.addToken(t);
			}
        }
        inputBuffer.clear();

        // cycle each productor
        for (Productor p : productorList) {
            p.stepPhase();
			if (!p.buffersAreEmpty()) {
				isIdleSoFar = false;
			}
        }

		// read tokens from each productor's outgoing buffers into the
		// incoming buffers of all other productors
		// - also read _OUTPUT tokens into the output buffer
        if (logNewTraffic) {
            log.append("New tokens:\n");
        }
        for (Productor p : productorList) {
            TokenPacket a = p.readTokenPacket();
			while (a != null) {
				if (logNewTraffic) {
					log.append(a.toString());
				}
				for (Productor otherP : productorList) {
					if (!otherP.equals(p)) {
						otherP.writeTokenPacket(a);
					}
				}
				TokenPacket outputAtomicAction = new TokenPacket();
				for (Token t: a) {
					if ("_OUTPUT".equals(t.getAttribute(0))) {
						outputAtomicAction.add(t);
		            }
					else {
						workingMemory.addToken(t);
					}
				}
				if (!outputAtomicAction.isEmpty()) {
					outputBuffer.add(outputAtomicAction);
				}
				a = p.readTokenPacket();
			}
        }

        // log any output tokens 
        if (logIOTraffic) {
            log.append("Output tokens:\n");
            for (TokenPacket tp : outputBuffer) {
                log.append(tp.toString());
            }
            log.append("\n");
        }

        cycleCount++;
        sendMessage(log.toString());
		this.isIdle = isIdleSoFar;
    }

    /**
     * Put a packet of tokens into the input buffer to be processed next cycle.
     */
    void addInput(TokenPacket tp) {
		for (Token t: tp) {
			t.setAttribute(0, "_INPUT");
		}
        inputBuffer.add(tp);
		isIdle = false;
    }
	
    /**
     * Get a copy of the output buffer.
     */
    List<TokenPacket> getOutputs() {
        return new ArrayList<>(outputBuffer);
    }

    /**
     * Load Street Language source from a string and add the rules to the agent.
     * The rules are only added if there are no parse errors. Messages are
     * appended to the log.
     *
     * @return true on success.
     */
    public boolean loadString(String inputProductions) {
        ANTLRInputStream input = new ANTLRInputStream(inputProductions);
        return loadInputStream(input);
    }

    /**
     * Load a Street Language source file and add the rules to the agent. The
     * rules are only added if there are no parse errors. Messages are appended
     * to the log.
     *
     * @return true on success.
     */
	@SuppressWarnings("squid:S1166") // In this case there is no need to propagate or log the exception
    public boolean loadFile(String inputFileName) {
        ANTLRInputStream input;
        try {
            // open the source file as a character stream
            input = new ANTLRFileStream(inputFileName);
        } catch (Exception e) {
            sendMessage("Unable to open the Street source file: " + inputFileName + "\n");
            return false;
        }
        return loadInputStream(input);
    }

    /**
     * Load Street Language source from an InputStream and add the rules to the agent.
     * The rules are only added if there are no parse errors. Messages are
     * appended to the log.
     *
     * @return true on success.
     */
	@SuppressWarnings("squid:S1166") // In this case there is no need to propagate or log the exception
    public boolean loadInputStream(InputStream inputStream) {
        ANTLRInputStream input;
        try {
            // open the inputStream as a character stream
            input = new ANTLRInputStream(inputStream);
        } catch (Exception e) {
            sendMessage("Unable to open the Street source file." + inputStream.toString() + "\n");
            return false;
        }
        return loadInputStream(input);
    }

    /**
     * Put a token into the debug buffer
     */
    void addToken(Token t) {
        debugBuffer.add(t);
		isIdle = false;
    }

    /**
     * Make a new unique ID.
     */
    String newId() {
        idCount++;
        return "_ID" + Integer.toString(idCount);
    }

    /**
     * Get (the first) productor from the agent with a specified name.
     */
    Productor getProductor(String name) {
        for (Productor productor : productorList) {
            if (productor.production.getName().equals(name)) {
                return productor;
            }
        }
        return null;
    }
	
    /**
     * Check if the agent already contains a production with the specified name
     */
    boolean hasProduction(String name) {
        for (Productor productor : productorList) {
            if (productor.production.getName().equals(name)) {
                return true;
            }
        }
        return false;
    }

	/**
	 * Get the names of all the productions.
	 */
	List<String> getProductionNames() {
		List<String> productionNames = new ArrayList<>();
		for (Productor productor : productorList) {
			productionNames.add(productor.production.getName());
		}
		Collections.sort(productionNames);
		return productionNames;
	}

    /**
     * @return An ArrayList holding the current contents of working memory.
     */
    WorkingMemory getWorkingMemory() {
        return workingMemory;
    }

    /**
     * Load Street Language source from an ANTLRINPUTSteam and add the rules to
     * the agent. The rules are only added if there are no parse errors.
     * Messages are appended to the log.
     *
     * @return true on success.
     */
    private boolean loadInputStream(ANTLRInputStream input) {
        // create a lexer that feeds off of input CharStream
        StreetLanguageLexer lexer = new StreetLanguageLexer(input);

        // create a buffer of tokens pulled from the lexer
        CommonTokenStream tokens = new CommonTokenStream(lexer);

        // create a parser that feeds off the tokens buffer
        StreetLanguageParser parser = new StreetLanguageParser(tokens);

        // add an error listener to the parser
        ErrorFlaggingListener listener = new ErrorFlaggingListener();
        parser.addErrorListener(listener);

        // parse the source
        ParseTree tree = parser.source(); // begin parsing at source rule
        if (listener.parseErrorsFound) {
            sendMessage("Parse errors found.\n");
            return false;
        }

        // create a generic parse tree walker that can trigger callbacks
        ParseTreeWalker walker = new ParseTreeWalker();

        // create a parser output converter to create a list of productions as it listens during the walk
        ParserOutputConverter converter = new ParserOutputConverter();

        // walk the tree created during the parse, trigger callbacks to the converter
        walker.walk(converter, tree);
        if (converter.conversionErrorsFound) {
            sendMessage("Code conversion errors found.\n");
            return false;
        }

        // grab the results of the conversion
        List<Production> productions = converter.getProductions();
        sendMessage(productions.size() + " production(s) read.\n");

		// add the productions to the agent
		for (Production p : productions) {
			addProduction(p);
		}

        return true;
    }

    /**
     * Used for loadinputStream.
     */
    private class ErrorFlaggingListener extends BaseErrorListener {

        boolean parseErrorsFound;

        ErrorFlaggingListener() {
            parseErrorsFound = false;
        }

        @Override
        public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol,
                int line, int charPositionInLine, String msg, RecognitionException e) {
            parseErrorsFound = true;
        }
    }

}

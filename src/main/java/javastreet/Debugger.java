/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import javastreet.peripherals.*;

/**
 * The Debugger class accepts debug command line strings and takes the
 * appropriate action on the agent. It is sub-classed by the ConsoleDebugger and
 * GUIDebugger.
 */
class Debugger {

    protected ThreadedAgent agent;
    private Scanner commandScanner;
	protected volatile boolean exitRequest = false;
	protected volatile int exitCode = 0;
	private final static String IGNORING_EXTRA_PARAMETERS = "Ignoring extra paramaters: ";

    Debugger(ThreadedAgent agent) {
        this.agent = agent;
        agent.pause();
        agent.start();
        agent.init();
		String[] t = {"_INPUT", "system", "reset"};
        agent.pushInput(new TokenPacket(t, true));
		new RandomNumberPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new PrintPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new TimerPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
		new ExitPeripheral(agent); // NOSONAR the instantiation attaches an event listener to the agent
    }

    /**
     * Interpret a command line and take the relevant action.
	 * Sets exitRequest and exitCode if the debugger should exit.
     */
    void debugCommand(String commandLine) {
        commandScanner = new Scanner(commandLine);
		if (!commandScanner.hasNext()) {
			exitRequest = true;
			return;
		}
        String command = commandScanner.next();
        switch (command) {
            case "add":
                addOrDelete(true);
                break;
            case "assert":
                assertWME();
                break;
            case "del":
                addOrDelete(false);
                break;
            case "echo":
                echo();
                break;
            case "exit":
				exitRequest = true;
                return;
            case "help":
                help();
                break;
            case "init":
                init();
                break;
			case "list":
				list();
				break;
            case "load":
                load();
                break;
            case "pause":
                pause();
                break;
            case "prod":
                prod();
                break;
            case "run":
                run();
                break;
            case "reset":
                reset();
                break;
            case "source":
                source();
                break;
            case "step":
                step();
                break;
            case "trace":
                trace();
                break;
			case "version":
				agent.sendMessage("JavaStreet Version "+Version.NUMBER+"\n");
                break;
            case "wmem":
                wmem();
                break;
			case "//":
				return;
            default:
                agent.sendMessage("Unrecognised command: "+command+"\n");
                break;
        }
		return;
    }

	/**
     * Print some help.
     */
    private void help() {
        StringBuilder message = new StringBuilder();
        if (commandScanner.hasNext()) {
            message.append(IGNORING_EXTRA_PARAMETERS).append(commandScanner.nextLine()).append("\n");
        }
        message.append("Enter one of the following debugger commands:\n");
		message.append("* `add <a0> <a1> ...`: add (<a0> <a1> ...) to working memory\n");
		message.append("* `assert [-] <a0> <a1> ...`: exit if (<a0> <a1> ...) is not ([-]=is) in working memory\n");
        message.append("* `del <a0> <a1> ...`: delete (<a0> <a1> ...) from working memory\n");
        message.append("* `echo [message]`: prints [message]\n");
        message.append("* `exit`: exit the simulator\n");
        message.append("* `help`: print this help\n");
        message.append("* `init`: clear the alpha memories and token buffers and send a new (_INPUT system reset) token\n");
        message.append("* `list`: list the names of all loaded productions\n");
        message.append("* `load <file>`: load productions from <file>\n");
        message.append("* `pause`: pause the agent if it is running\n");
        message.append("* `prod <productionName>`: print a production including its alpha memories\n");
        message.append("* `reset`: delete all productions, clear all memories, and reset the agent\n");
        message.append("* `run`: run the agent continuously\n");
        message.append("* `source`<file>: execute the debugger commands from <file>\n");		
        message.append("* `step`: step the agent one cycle\n");
        message.append("* `step <n>`: step the agent <n> cycles\n");
        message.append("* `trace all`: include all optional events in the log\n");
        message.append("* `trace new|io|firings|cycles|debug`: to toggle logging these events\n");
        message.append("* `trace none`: include no optional events in the log\n");
        message.append("* `version`: print the version number of the javaStreet simulator\n");
        message.append("* `wmem`: print the contents of working memory\n");
        message.append("* `// [comment]`: ignores the comment\n");
        agent.sendMessage(message.toString());
    }

    /**
     * Add or delete a WME in working memory.
     */
    private void addOrDelete(boolean add) {
		List<String> a = new ArrayList<>();
		while (commandScanner.hasNext()) {
			a.add(commandScanner.next());
		}
		if (a.isEmpty()) {
            agent.sendMessage("Specify the WME e.g. `add <a0> <a1> <a2>`\n");
            return;
        }
        agent.takeMutex();
		String[] t = new String[a.size()];
		t = a.toArray(t);		
        agent.addToken(new Token(t, add));
        agent.giveMutex();
		agent.signalEvent();
    }

    /**
     * Exit with an error if a WME doesn't/does exist.
     */
    private void assertWME() {
		boolean negated = false;
		List<String> a = new ArrayList<>();
		while (commandScanner.hasNext()) {
			a.add(commandScanner.next());
		}
		if (a.isEmpty()) {
            agent.sendMessage("Specify the WME e.g. `assert agent hasA hat`\n");
            return;
        }
		if ("-".equals(a.get(0))) {
			negated = true;
			a.remove(0);
			if (a.isEmpty()) {
		        agent.sendMessage("Specify the WME e.g. `assert - agent hasA hat`\n");
			    return;
			}
		}
		String[] a2 = new String[a.size()];
		a2 = a.toArray(a2);
		WME wme = new WME(a2);
        agent.takeMutex();
		boolean present = agent.getWorkingMemory().contains(wme);
        agent.giveMutex();

		if (!present && !negated) {
			System.err.println("Assertion failed. Working memory does not contain "+wme.toString());
			exitRequest = true;
			exitCode = 1;
		}
		else if (present && negated) {
			System.err.println("Assertion failed. Working memory contains "+wme.toString());
			exitRequest = true;
			exitCode = 1;
		}
    }

	/**
     * Echo a message.
     */
    private void echo() {
        if (commandScanner.hasNext()) {
			agent.sendMessage(commandScanner.nextLine()+"\n");
		}
	}
	
    /**
     * Initialise the agent (clear all the alpha memories) and input a (_INPUT
     * system reset) token.
     */
    private void init() {
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.takeMutex();
        agent.init();
		String[] t = {"_INPUT", "system", "reset"};
        agent.addInput(new TokenPacket(t, true));
        agent.giveMutex();
        agent.sendMessage("Agent Initialised.\n");
		agent.signalEvent();
    }

	/**
	 * List the names of all the rules in production memory.
	 */
	private void list() {
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
		List<String> productionNames = agent.getProductionNames();
		for (String name: productionNames) {
	        agent.sendMessage(name+"\n");				
		}
	}

	/**
     * Load a Street Language source file and add the rules to the agent.
     */
    private void load() {
        if (!commandScanner.hasNext()) {
            agent.sendMessage("Specify the Street source file e.g. `load test.street`.\n");
            return;
        }
        String inputFileName = commandScanner.next();
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.takeMutex();
        agent.loadFile(inputFileName);
        agent.giveMutex();
		agent.signalEvent();
    }

    /**
     * Pause the agent
     */
    private void pause() {
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.pause();
        agent.sendMessage("The agent is paused.\n");
    }

    /**
     * Print information about a production
     */
    private void prod() {
        if (!commandScanner.hasNext()) {
            agent.sendMessage("Specify the name of a production e.g. `prod productionName`\n");
			return;
        }
        String productionName = commandScanner.next();
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }

        agent.takeMutex();
        Productor p = agent.getProductor(productionName);
        agent.giveMutex();
        if (p == null) {
            agent.sendMessage("Could not find production: "+productionName+"\n");
            return;
        }
        agent.sendMessage(p.toString());
    }

    /**
     * Run the agent continuously
     */
    private void run() {
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.sendMessage("Running the agent...\n");
        agent.resume();
    }

    /**
     * Clear all productions and re-initialise the agent.
     */
    private void reset() {
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.takeMutex();
        agent.reset();
		String[] t = {"_INPUT", "system", "reset"};
        agent.addInput(new TokenPacket(t, true));
        agent.giveMutex();
        agent.sendMessage("Agent Reset.\n");
		agent.signalEvent();
    }
	
    /**
     * Execute debugger commands from a file
     */
    private void source() {
        if (!commandScanner.hasNext()) {
            agent.sendMessage("Specify the name of the command file to execute e.g. `source tests.cmd`\n");
			return;
        }
        String fileName = commandScanner.next();
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
		Scanner inputScanner;
		try {
			inputScanner = new Scanner(new File(fileName));
		}
		catch (FileNotFoundException ex) {
			agent.sendMessage("Unable to read command file: "+fileName+"\n");
			return;
		}
        while (inputScanner.hasNextLine()) {
			String commandLine = inputScanner.nextLine();
			if (commandLine.trim().length()>0) {
				debugCommand(commandLine);
			}
			if (exitRequest) {
				return;
			}
        }
    }

	/**
     * Step the agent for one or more cycles.
     */
    private void step() {
        int stepCycles = 1;
        if (commandScanner.hasNext()) {
            if (commandScanner.hasNextInt()) {
                stepCycles = commandScanner.nextInt();
                if (stepCycles <= 0) {
                    agent.sendMessage("The number of cycles to step must be greater than zero.\n");
                    return;
                }
            } else {
                agent.sendMessage("Step one cycle (e.g. `step`) or specify the number of cycles to step (e.g. `step 10`).\n");
                return;
            }
        }
        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
        agent.sendMessage("Stepping the agent for "+stepCycles+" cycles...\n");
        agent.step(stepCycles);
        while (agent.isBusy()) {
			// Busy wait
		}
    }

	/**
     * Select events to add to the log.
     */
    private void trace() {
        if (commandScanner.hasNext()) {
            String option = commandScanner.next();
            switch (option) {
                case "new":
                    agent.logNewTraffic = !agent.logNewTraffic;
                    agent.sendMessage("Logging new token traffic: "+agent.logNewTraffic+"\n");
                    break;
                case "io":
                    agent.logIOTraffic = !agent.logIOTraffic;
                    agent.sendMessage("Logging IO traffic: "+agent.logIOTraffic+"\n");
                    break;
                case "debug":
                    agent.logDebugTraffic = !agent.logDebugTraffic;
                    agent.sendMessage("Logging debug traffic: "+agent.logDebugTraffic+"\n");
                    break;
                case "firings":
                    agent.logFirings = !agent.logFirings;
                    agent.sendMessage("Logging rule firings: "+agent.logFirings+"\n");
                    break;
                case "cycles":
                    agent.logCycles = !agent.logCycles;
                    agent.sendMessage("Logging cycles: "+agent.logCycles+"\n");
                    break;
                case "all":
                    agent.logFirings = true;
                    agent.logCycles = true;
                    agent.logIOTraffic = true;
                    agent.logNewTraffic = true;
                    agent.logDebugTraffic = true;
                    agent.sendMessage("Logging all optional events.\n");
                    break;
                case "none":
                    agent.logFirings = false;
                    agent.logCycles = false;
                    agent.logIOTraffic = false;
                    agent.logNewTraffic = false;
                    agent.logDebugTraffic = false;
                    agent.sendMessage("Logging no optional events.\n");
                    break;
                default:
                    agent.sendMessage("Unrecognised option: "+option+"\n");
                    return;
            }
        } else {
            agent.sendMessage("Specify 'trace x' where x is new, io, firings, all or none.\n");
            return;
        }

        if (commandScanner.hasNext()) {
            agent.sendMessage(IGNORING_EXTRA_PARAMETERS+commandScanner.nextLine()+"\n");
        }
    }

    /**
     * Print working memory.
     */
    private void wmem() {
        StringBuilder message = new StringBuilder("WORKING MEMORY:\n");
        if (commandScanner.hasNext()) {
            message.append(IGNORING_EXTRA_PARAMETERS).append(commandScanner.nextLine()).append("\n");
        }
        agent.takeMutex();
        message.append(agent.getWorkingMemory().toString());
        agent.giveMutex();
        agent.sendMessage(message.toString());
    }

}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

public class Main {
	
	/*
	 * This is a utility class that is not meant to be instantiated.
	 * In such cases it is good practice to create a private constructor to
	 * hide the public one that Java would otherwise create implicitly.
	 * Refer squid:S1118.
	 */
	private Main() {
	}
	
	/**
     * Main application entry point.
     * Launches a ConsoleDebugger if no command line arguments are passed of
     * the first command line argument is -b (for batch mode).
     * The ConsoleDebugger can handle commands redirected from a command file.
     * 
     * If the command line arguments are `file n` then a CommandLineRunner
     * runs the rules specified in file for n cycles.
     * 
     * If the first command line argument is -w (for window mode) then 
     * a GUIDebugger is launched.
     */
    public static void main(String[] args) throws Exception {

        AgentController agentController;
        ThreadedAgent agent = new ThreadedAgent();
        
        if ((args.length == 0) || ("-b".equals(args[0]))) {
            agentController = new ConsoleDebugger(agent);
        }
        else if ("-w".equals(args[0])) {
            agentController = new GUIDebugger(agent);
        }
        else {
            agentController = new CommandLineRunner(agent);            
        }

		// Create an exitHandler to tidy up when the agentController exits
		Runnable exitHandler = new Runnable() {
			@Override
			@SuppressWarnings("squid:S1147") // Use of System.exit is appropriate in this context
			public void run() {
				System.out.println("Simulator exiting.");
				agent.terminate();
				System.exit(agentController.getExitCode());
			}
		};
		agentController.setExitHandler(exitHandler);
		
        try {
            agentController.runController(args);
        }
        catch (Exception ex) {
            System.err.println("Terminating due to exception: "+ex.getMessage());
			agent.terminate();
			throw ex;
        }		
    }
	

}
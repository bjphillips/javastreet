/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

class WorkingMemory extends ArrayList<WME> {
	boolean addToken(Token t) {
		WME w = new WME(t.attributes);
		if (this.contains(w)) {
			if(t.getFlag() == TokenFlag.ADD) {
				return false;
			}
			else if (t.getFlag() == TokenFlag.DELETE) {
				this.remove(this.indexOf(w));
				return true;
			}
		} else {
			if (t.getFlag() == TokenFlag.ADD) {
				this.add(w);
				return true;
			}
			else if (t.getFlag() == TokenFlag.DELETE) {
				return false;
			}
		}
		return false;
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
        for(WME wme : this) {
            s.append(wme.toString()+"\n");
        }
        return s.toString();
	}
}
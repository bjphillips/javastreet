/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

/* Convert the syntax tree output from the ANTLR generated parser
 * into an array list of productions to add to the agent.
 *
 * This is done using the *listener* pattern. ANTLR provides a tree
 * walker that calls our listener (or event handler) methods for each
 * phrase in the source code.
 *
 * Note that this step is a bit redundant and it might make more sense to
 * pass parts of the syntax tree directly to the agent, or to call 
 * agent.addProduction directly from our listeners.
 */

import streetlanguage.*;
import java.util.*;

class ParserOutputConverter extends StreetLanguageBaseListener {

    private final List<Production> productions;
	private final List<String> productionNames;
    private Production currentProduction;
    private ConditionElement currentCE;
    private Action currentAction;
	private Set<String> currentVariables;
	private Set<String> currentBoundVariables;
    boolean conversionErrorsFound;
	private final static String LINE = "line ";
	private final static String INVALID_LHS_CONSTANT = " invalid left-hand side constant ";

	public ParserOutputConverter() {
		currentProduction = null;
		currentCE = null;
		currentAction = null;
		conversionErrorsFound = false;
		productions = new ArrayList<>();
		productionNames = new ArrayList<>();
	}

	public List<Production> getProductions() {
		return productions;
	}

	@Override
	public void enterProduction(StreetLanguageParser.ProductionContext ctx) {
		currentProduction = new Production();
		currentProduction.setName(ctx.Identifier().getText());
		currentVariables = new HashSet<>();
		currentBoundVariables = new HashSet<>();
	}

	@Override
	public void enterPositiveConditionElement(StreetLanguageParser.PositiveConditionElementContext ctx) {
		currentCE = new ConditionElement();
		currentCE.setNegated(false);
	}

	@Override
	public void enterNegatedConditionElement(StreetLanguageParser.NegatedConditionElementContext ctx) {
		currentCE = new ConditionElement();
		currentCE.setNegated(true);
	}

	/* (Variable relationalOperator (Variable | constant)) 
	 * Constant case encoded as e.g. x EQ C
	 * The only permitted case of (Variable relationalOperator Variable) is
	 * for the != relationalOperator and this is encoded as e.g. x NEV y
	 */
	@Override
	public void exitLhsRelation(StreetLanguageParser.LhsRelationContext ctx) {
		String arg1, arg2;
		TestType test;

		arg1 = trimVariableName(ctx.Variable(0).getText()); 
		test = TestType.stringToTest(ctx.relationalOperator().getText());
		if (ctx.Variable(1)!=null) {
			arg2 = trimVariableName(ctx.Variable(1).getText());
			// Permit inequality tests against variables only
			if (test != TestType.NE) {
				System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					" variable relations can only be for inequality but found "+test.toString());
				conversionErrorsFound = true;
			}
			test = TestType.NEV;
		}
		else {
			arg2 = ctx.constant().getText();
			if (arg2.startsWith("_")) {
				System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					INVALID_LHS_CONSTANT+arg2);
				conversionErrorsFound = true;
			}
		}

		currentCE.add(arg1, test, arg2);
	}

	/* (relationalOperator constant)
	 * encoded as e.g. null EQ C
	 */
	@Override
	public void exitLhsConstantRelation(StreetLanguageParser.LhsConstantRelationContext ctx) {
		String arg2;
		TestType test;

		test = TestType.stringToTest(ctx.relationalOperator().getText());
		arg2 = ctx.constant().getText(); 
		if (arg2.startsWith("_")) {
			System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
				INVALID_LHS_CONSTANT+arg2);
			conversionErrorsFound = true;
		}

		currentCE.add(null,test,arg2);
	}

	@Override
	/* encoded as x EQV x
	*/
	public void exitLhsVariable(StreetLanguageParser.LhsVariableContext ctx) {
		String arg1 = trimVariableName(ctx.Variable().getText()); 
		currentCE.add(arg1, TestType.EQV, arg1);
		currentVariables.add(arg1);
		if (!currentCE.isNegated()) {
			currentBoundVariables.add(arg1);
		}
	}

	@Override
	/* encoded as null EQ null
	*/
	public void exitLhsWildcard(StreetLanguageParser.LhsWildcardContext ctx) {
		currentCE.add(null, TestType.EQ, null);
	}

	@Override
	/* encoded as null EQ C
	*/
	public void exitLhsConstant(StreetLanguageParser.LhsConstantContext ctx) {
		String arg2 = ctx.constant().getText(); 
		currentCE.add(null, TestType.EQ, arg2);
		if (arg2.startsWith("_")) {
			if ((currentCE.numberOfAttributes() > 1) || !"_INPUT".equals(arg2)) {
				System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					INVALID_LHS_CONSTANT+arg2);
				conversionErrorsFound = true;
			}
		}
	}

	@Override
	public void exitSequencingArrow(StreetLanguageParser.SequencingArrowContext ctx) {
		currentProduction.setSequencing(true);
	}

	@Override
	public void exitPositiveConditionElement(StreetLanguageParser.PositiveConditionElementContext ctx) {
		currentProduction.addCE(currentCE); 
	}

	@Override
	public void exitNegatedConditionElement(StreetLanguageParser.NegatedConditionElementContext ctx) {
		currentProduction.addCE(currentCE); 
	}

	@Override
	public void enterPositiveAction(StreetLanguageParser.PositiveActionContext ctx) {
		currentAction = new Action();
		currentAction.setDelete(false);
	}

	@Override
	public void enterDeleteAction(StreetLanguageParser.DeleteActionContext ctx) {
		currentAction = new Action();
		currentAction.setDelete(true);
	}

	@Override
	public void exitRhsBinaryExpression(StreetLanguageParser.RhsBinaryExpressionContext ctx) {
		String arg1, arg2;
		OperationType operation;

		arg1 = trimVariableName(ctx.Variable(0).getText()); 
		operation = OperationType.stringToOperation(ctx.binaryOperator().getText());
		if (ctx.Variable(1)!=null) {
			arg2 = trimVariableName(ctx.Variable(1).getText());
			operation = operation.makeOperationVariable();
		}
		else {
			arg2 = ctx.constant().getText();
			if (arg2.startsWith("_")) {
				System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					" invalid right-hand side constant "+arg2);
				conversionErrorsFound = true;
			}
		}
		currentAction.add(arg1, operation, arg2);
	}

	@Override
	public void exitRhsVariable(StreetLanguageParser.RhsVariableContext ctx) {
		String arg1 = trimVariableName(ctx.Variable().getText()); 
		currentAction.add(arg1, OperationType.VAR, null);
	}

	@Override
	public void exitRhsConstant(StreetLanguageParser.RhsConstantContext ctx) {
		String arg1 = ctx.constant().getText(); 
		currentAction.add(arg1, OperationType.CONST, null);
		if (arg1.startsWith("_")) {
			if ((currentAction.numberOfAttributes() > 1) || !"_OUTPUT".equals(arg1)) {
				System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					" invalid right-hand side constant "+arg1);
				conversionErrorsFound = true;
			}
		}
	}

	@Override
	public void exitPositiveAction(StreetLanguageParser.PositiveActionContext ctx) {
		currentProduction.addAction(currentAction); 
	}

	@Override
	public void exitDeleteAction(StreetLanguageParser.DeleteActionContext ctx) {
		currentProduction.addAction(currentAction); 
	}

	@Override
	public void exitProduction(StreetLanguageParser.ProductionContext ctx) {
		// Ensure the variables in negated conditions also appear in positive ones
		currentVariables.removeAll(currentBoundVariables);
		if (!currentVariables.isEmpty()) {
			System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					" production "+currentProduction.getName()+
					" contains the variables "+currentVariables.toString()+
					" in negated condition elements but not in positive condition elements." +
					" Use the wildcard <*> instead.");
				conversionErrorsFound = true;
		}
		// Make sure production names are unique
		if (productionNames.contains(currentProduction.getName())) {
			System.err.println(LINE+ctx.getStart().getLine()+":"+ctx.getStart().getCharPositionInLine()+
					" the production name "+currentProduction.getName()+" is not unique.");
				conversionErrorsFound = true;
		}
		else {
			productionNames.add(currentProduction.getName());
		}
		productions.add(currentProduction);
	}

	// Trim the '<' and '>' from around a variable name
	private String trimVariableName(String v) {
		return v.substring(1,v.length()-1);
	}
}


/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.io.Console;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ConsoleDebugger extends Debugger implements AgentController, MessageHandler {

    private Console console;
    private Scanner inputScanner;
    private final Object stdoutLock = new Object();
	private Runnable exitHandler;

    public ConsoleDebugger(ThreadedAgent agent) {
        super(agent);
        agent.setMessageHandler(this);
		exitHandler = null;
    }

	/**
     * Read and execute commands line by line from either the console, or 
     * the standard input if started in batch mode.
	 * Returns an exit code.
     */
    @Override
    public void runController(String[] args) throws FileNotFoundException {

		// report current version
		System.out.println("JavaStreet Version "+Version.NUMBER);

		if ((args.length > 0) && ("-b".equals(args[0]))) {
            if (args.length == 1) {
                System.err.println("Specify a command file.");
				exit(1);
            }
            try {
                inputScanner = new Scanner(new File(args[1]));
            }
            catch (FileNotFoundException ex) {
                System.err.println("Unable to read command file: "+args[1]);
				throw ex;
            }
        }
        else {
            console = System.console();
            if (console == null) {
                inputScanner = new Scanner(System.in);
            }
        }

        while (true) {
            String commandLine = null;
            if (console == null) {
                if (inputScanner.hasNextLine()) {
                    commandLine = inputScanner.nextLine();
                }
                else {
                    System.out.println("All done.");
					exit(0);
                }
            }
            else {
                do {
                    commandLine = console.readLine("Street> ");
                } while (commandLine==null);
            }
   			// ignore lines that are all whitespace
			if ((commandLine != null) && (commandLine.trim().length()>0)) {
				debugCommand(commandLine);
			}
			if (exitRequest) {
				exit(exitCode);
			}
        }
    }
    
	@Override
	public void setExitHandler(Runnable exitHandler) {
		this.exitHandler = exitHandler;
	}
	
	@Override
	public int getExitCode() {
		return exitCode;
	}
	
	void exit(int exitCode) {
		this.exitCode = exitCode;
		if (exitHandler != null) {
			exitHandler.run();
		}
	}

	/**
     * 
     * This method is called from the agent's thread and can happen at any time.
     * Of course, if the agent is running continuously, and producing a lot of
     * text output, then this is going to get ugly with the command prompt.
     * There is no a lot we can do. Its just not a great use-case for a command 
     * line debugger.
     * We can just make sure the printout of the log doesn't interleave with
     * messages from the debugger. For that we will use a lock variable.
     */
    @Override
    public void handleMessage(String message) {
        synchronized (stdoutLock) {
            System.out.print(message);
        }
    }

}


/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

/**
 * A condition element is a list of conditions and a rule's LHS is a set of
 * condition elements.
 *
 * The valid tests and their encodings are:
 *                 leftAttribute TestType rightAttribute isConstant isVariable
 * 1) C            null          EQ       C              T          F
 * 2) != C         null          NE       C              T          F
 * 3) > C          null          GT       C              T          F
 * 4) < C          null          LT       C              T          F
 * 5) >= C         null          GTE      C              T          F
 * 6) <= C         null          LTE      C              T          F
 * 7) <x>          x             EQV      x              F          T
 * 8) <*>          null          EQ       null           T          F
 * 9) <x> != C     x             NE       C              T          T
 * 10) <x> < C     x             LT       C              T          T
 * 11) <x> > C     x             GT       C              T          T
 * 12) <x> <= C    x             LTE      C              T          T
 * 13) <x> >= C    x             GTE      C              T          T
 * 14) <x> != <y>  x             NEV      y              F          T
 *
 * A WME is added to the alpha block corresponding to a condition element if it passes
 * all the constant tests in the condition element.
 *
 * An instantiation occurs when the WMEs in the alpha blocks join on their
 * variable tests.
 */
class ConditionElement {

    private boolean negated;
    private final List<String> leftAttributes;
    private final List<String> rightAttributes;
    private final List<TestType> tests;

    ConditionElement() {
		leftAttributes = new ArrayList<>();
		rightAttributes = new ArrayList<>();
		tests = new ArrayList<>();
	}
	
    boolean isNegated() {
        return negated;
    }
	
	int numberOfAttributes() {
		return tests.size();
	}
	
	void setNegated(boolean negated) {
		this.negated = negated;
	}

	String getLeftAttribute(int i) {
		return leftAttributes.get(i);
	}
	
	String getRightAttribute(int i) {
		return rightAttributes.get(i);
	}
	
	TestType getTest(int i) {
		return tests.get(i);
	}
	
	void add(String leftAttribute, TestType test, String rightAttribute) {
		leftAttributes.add(leftAttribute);
		tests.add(test);
		rightAttributes.add(rightAttribute);
	}
	
	@Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        if (negated) {
            sb.append("-");
        }
        sb.append("(");

        for (int i = 0; i < tests.size(); i++) {

            if (leftAttributes.get(i) != null) {
                sb.append("<" + leftAttributes.get(i) + ">");
            }

            if (tests.get(i) != TestType.EQV) {
                sb.append(tests.get(i).toString());
				if (rightAttributes.get(i) == null) {
                    sb.append(" <*> ");					
				}
				else if (tests.get(i) != TestType.NEV) {
                    sb.append(rightAttributes.get(i) + " ");
                } 
				else {
                    sb.append("<" + rightAttributes.get(i) + "> ");
                }
            } else {
                sb.append(" ");
            }
        }

        sb.append(")");
        return sb.toString();
    }

    /* Test if a WME passes this condition element's constant tests.
	 */
    boolean constantTest(WME w) {
		if (w.numberOfAttributes()!=tests.size()) {
			return false;
		}
        for (int i = 0; i < tests.size(); i++) {
            if (this.isConstant(i) 
				&& !test(w.getAttribute(i), tests.get(i), rightAttributes.get(i))) {
				return false;
            }
        }
        return true;
    }

    private boolean test(String x, TestType test, String y) {
        int xInt = 0;
        int yInt = 0;
        boolean areInts = false;
        try {
            xInt = Integer.parseInt(x);
            yInt = Integer.parseInt(y);
            areInts = true;
        } catch (NumberFormatException e) {
            // This is not a disaster. x and y are not integers.
        }
        switch (test) {
            case EQ:
            case EQV:
                if (areInts) {
                    if (xInt == yInt) {
                        return true;
                    }
                } 
				else if (y == null) {
					return true;		// everything matches a wildcard
				}
				else {
                    if (x.equals(y)) {
                        return true;
                    }
                }
                break;
            case NE:
            case NEV:
                if (areInts) {
                    if (xInt != yInt) {
                        return true;
                    }
                } else if (!x.equals(y)) {
                    return true;
                }
                break;
            case LT:
                if (areInts) {
                    if (xInt < yInt) {
                        return true;
                    }
                } else if (x.compareTo(y) < 0) {
                    return true;
                }
                break;
            case GT:
                if (areInts) {
                    if (xInt > yInt) {
                        return true;
                    }
                } else if (x.compareTo(y) > 0) {
                    return true;
                }
                break;
            case LTE:
                if (areInts) {
                    if (xInt <= yInt) {
                        return true;
                    }
                } else if (x.compareTo(y) <= 0) {
                    return true;
                }
                break;
            case GTE:
                if (areInts) {
                    if (xInt >= yInt) {
                        return true;
                    }
                } else if (x.compareTo(y) >= 0) {
                    return true;
                }
                break;
			default:
				System.err.println("Unrecognised test " + test.toString() + " in " + this.toString());
        }
        return false;
    }
 
    boolean isConstant(int i) {
        return !((tests.get(i) == TestType.NEV) || (tests.get(i) == TestType.EQV));
    }

    boolean isVariable(int i) {
        return !(leftAttributes.get(i) == null);
    }
	
}

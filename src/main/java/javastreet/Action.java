/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.ArrayList;
import java.util.List;

class Action {
	private boolean delete;
	private final List<String> leftAttributes;
	private final List<String> rightAttributes;
	private final List<OperationType> operations;
	
	Action() {
		leftAttributes = new ArrayList<>();
		rightAttributes = new ArrayList<>();
		operations = new ArrayList<>();
	}
	
	int numberOfAttributes() {
		return operations.size();
	}
	
	boolean isDelete() {
		return delete;
	}
	
	void setDelete(boolean delete) {
		this.delete = delete;
	}
	
	OperationType getOperation(int i) {
		return operations.get(i);
	}
	
	String getLeftAttribute(int i) {
		return leftAttributes.get(i);
	}

	String getRightAttribute(int i) {
		return rightAttributes.get(i);
	}
	
	void add(String leftAttribute, OperationType operation, String rightAttribute) {
		leftAttributes.add(leftAttribute);
		operations.add(operation);
		rightAttributes.add(rightAttribute);
	}
		
	void makeOperationVariable(int i) {
		switch (operations.get(i)) {
			case ADD:
				operations.set(i,OperationType.ADDV);
				break;
			case SUB:
				operations.set(i, OperationType.SUBV);
				break;
			case MULT:
				operations.set(i, OperationType.MULTV);
				break;
			case DIV:
				operations.set(i, OperationType.DIVV);
				break;
			default:
				break;
		}
	}	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		if (delete) {
			sb.append("-");
		}
		
		sb.append("(");
		for (int i=0; i<operations.size(); i++) {
			switch (operations.get(i)) {
				case CONST:
					sb.append(leftAttributes.get(i)).append(" ");
					break;
				case VAR:
					sb.append("<").append(leftAttributes.get(i)).append("> ");
					break;
				default: //NOSONAR - reducing the number of lines to 5 would reduce readability
					sb.append("<").append(leftAttributes.get(i)).append("> ").append(operations.get(i).toString()).append(" ");
					if(operations.get(i).isVariable()) {
						sb.append("<").append(rightAttributes.get(i)).append("> ");
					}
					else {
						sb.append(rightAttributes.get(i)).append(" ");
					}
					break;
			}
		}
		sb.append(")");
		return sb.toString();
	}

}
/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javafx.application.Platform;
import javafx.embed.swing.JFXPanel;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;

/**
 * The GUIDebugger uses JavaFX (because it is new) but running in a Swing
 * application (so we don't have to change our build and distribution system).
 * 
 * 26/05/2015 After too many unproductive hours I've decided that JavaFX is not
 * (yet) a good match with Gradle and is really only supported by NetBeans and
 * Ant. If we are going to use JavaFx then doing it under Swing is not such a
 * bad idea.
 * 
 * Much of what you see here is thanks to
 * http://docs.oracle.com/javafx/2/swing/swing-fx-interoperability.htm#CHDIEEJE
 */
public class GUIDebugger extends Debugger implements AgentController {

	private Runnable exitHandler;

	public GUIDebugger(ThreadedAgent agent) {
        super(agent);
		exitHandler = null;
    }

    @Override
    public void runController(String[] args) {
        // Create the EDT (Event Dispatch Thread)
		Runnable edtRunnable = new Runnable() {
            @Override
            public void run() {
                initAndShowGUI();
            }			
		};
        SwingUtilities.invokeLater(edtRunnable);
    }
	
	@Override
	public void setExitHandler(Runnable exitHandler) {
		this.exitHandler = exitHandler;
	}
	
	@Override
	public int getExitCode() {
		return exitCode;
	}
	
    private void initAndShowGUI() {
        // This method is invoked on the EDT (Event Dispatch Thread)
        JFrame frame = new JFrame("Street Debugger");
        final JFXPanel fxPanel = new JFXPanel();
        frame.add(fxPanel);
        frame.setSize(420, 320);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
        frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.addWindowListener(new WindowAdapter() {
			@Override
            public void windowClosing(WindowEvent e) {
				exit();
			}
		});

        // Create the JavaFX thread
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                initFX(fxPanel);
            }
        });
    }

	public void exit() {
		if (exitHandler != null) {
			exitHandler.run();
		}
	}
	
    private void initFX(JFXPanel fxPanel) {
        // This method is invoked on the JavaFX thread
        Scene scene = createScene();
        fxPanel.setScene(scene);
    }

    private Scene createScene() {
        Parent root = null;
        FXMLLoader loader;
        GUIDebuggerController controller = null;
        try {
            loader = new FXMLLoader(getClass().getResource("GUIDebugger.fxml"));  
            root = loader.load();
            controller = loader.<GUIDebuggerController>getController();
        }
        catch (Exception ex) {
            System.err.println("Exception loading GUIDebugger.fxml: "+ex.getMessage());
			// Unchecked exception justified as there no reasonable hope of recovering from this
			throw new RuntimeException(ex);
        }
            
        controller.setDebugger(this);
        agent.setMessageHandler(controller);
        return new Scene(root);
    }
    
}
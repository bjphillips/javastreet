/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

public final class Version {
	public static final Integer MAJOR = 2;
	public static final Integer MINOR = 4;
	public static final Integer PATCH = 0;
	
	public static final String NUMBER = MAJOR.toString()+"."+MINOR.toString()+"."+PATCH.toString();
}

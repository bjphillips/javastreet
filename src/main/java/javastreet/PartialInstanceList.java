/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.ArrayList;

/**
 * An ArrayList of ParsialInstances.
 * @author phillips
 */
class PartialInstanceList extends ArrayList<PartialInstance> {
	
	PartialInstanceList() {
		super();
	}
	
	PartialInstanceList(PartialInstance partialInstance) {
		super();
		this.add(partialInstance);
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		for (PartialInstance partialInstance: this) {
			s.append(partialInstance.toString());
		}
		return s.toString();
	}
}


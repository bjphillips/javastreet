/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

enum OperationType {
	CONST, VAR, ADD, SUB, MULT, 
	DIV, ADDV, SUBV, MULTV, DIVV;
	
	static OperationType stringToOperation(String s) {
		if ("+".equals(s))
			return ADD;
		else if ("-".equals(s))
			return SUB;
		else if ("*".equals(s))
			return MULT;
		else 
			return DIV;
	}

	boolean isVariable() {
		if(this == ADDV || this == SUBV || this == MULTV || this == DIVV) {
			return true;
		}
		return false;
	}

	OperationType makeOperationVariable() {
		if (this.equals(ADD))
			return ADDV;
		else if (this.equals(SUB))
			return SUBV;
		else if (this.equals(MULT))
			return MULTV;
		else 
			return DIVV;
	}

	@Override
	public String toString() {
		switch (this) {
			case ADD:
			case ADDV:
				return "+";
			case SUB:
			case SUBV:
				return "-";
			case MULT:
			case MULTV:
				return "*";
			case DIV:
			case DIVV:
				return "/";
			default:
				return "";
		}
	}
}
/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

/**
 * A match pair is a pair consisting of a condition element and a WME. If the
 * condition element is positive it is a positive match pair; if the condition element is
 * negated it is a negated match pair. Joins occur between match pairs. Partial
 * instances are sets of positive match pairs.
 * 
 * The WME and the condition element in a match pair should always have the same 
 * number of attributes.
 */
class MatchPair {

	private final ConditionElement conditionElement;
	private final WME wme;

	MatchPair(ConditionElement conditionElement, WME wme) {
		this.conditionElement = conditionElement;
		this.wme = wme;
	}

	boolean isNegated() {
		return conditionElement.isNegated();
	}

	boolean isConstant(int i) {
        return conditionElement.isConstant(i);
    }

    boolean isVariable(int i) {
		return conditionElement.isVariable(i);
    }
	
	ConditionElement getConditionElement() {
		return this.conditionElement;
	}
	
	WME getWme() {
		return this.wme;
	}
	
	/**
	 * Returns true if another match pair joins to this match pair on the common
	 * variables.
	 *
	 * Match pairs join if they have a consistent binding for all shared
	 * variables that satisfies all variable conditions. 
	 * 
	 * The WMEs in an alpha block are guaranteed to satisfy all the constant 
	 * conditions in the alpha block's condition element. Hence when joining
	 * match pairs drawn from alpha blocks you can make the assumption that
	 * it is only the variable bindings and the special condition <x>
	 * != <y> that need to be considered.
	 * 
	 * Some examples to consider:
	 * 
	 * Ex 1) this.wme = (ID1, A, 5), this.conditionElement = (<s>, A, <v>),
	 * other.wme = (ID1, 5, A), other.conditionElement = (<s>, <v>, A)
	 *
	 * These join because in this match pair, <v>=5 and <s>=ID1 and in the other 
	 * <v>=5 and <s>=ID1.
	 * 
	 * Ex 2) this.wme = (ID1, A, 5), this.conditionElement = (<s>, A, <v>!=<t>),
	 * other.wme = (ID1, 6, A), other.conditionElement = (<s>, <t>, A)
	 *
	 * These join because in this match pair, <v>=5 and <s>=ID1 and in 
	 * the other match pair <t>=6 and <s>=ID1
	 * 
	 * Ex 3) this.wme = (ID1, A, 5), this.conditionElement = (<s>, A, <v>),
	 * other.wme = (ID1, 6, A), other.conditionElement = (<s>, <t>!=<v>, A)
	 *
	 * These join because in this match pair, <v>=5 and <s>=ID1 and in 
	 * the other match pair <t>=6 and <s>=ID1
	 */
	boolean joinTest(MatchPair other) {
		// for each of the attributes in this match pair...
		for (int i=0; i<this.conditionElement.numberOfAttributes(); i++) {
			if (this.isVariable(i)) {
				// compare against each of the attributes in the other match pair
				for (int j=0; j<other.conditionElement.numberOfAttributes(); j++) {
					// both this and the other condition element are variable tests
					if (other.isVariable(j)) { 
						// the NEV test is a special case for which we need to check both condition elements
						if (other.conditionElement.getTest(j).equals(TestType.NEV)) {
							// if the first variable in this condition element is the same as the second in the other
							if (this.conditionElement.getLeftAttribute(i).equals(other.conditionElement.getRightAttribute(j))) {
								// then their values should not match
								if (this.wme.getAttribute(i).equals(other.wme.getAttribute(j))) {
									return false;
								}
							}
						}
						// now check NEV in this condition element
						if (this.conditionElement.getTest(i).equals(TestType.NEV)) {
							// if the second variable in this ConditionElement is the same as the first in the other
							if (this.conditionElement.getRightAttribute(i).equals(other.conditionElement.getLeftAttribute(j))) {
								// then their values should not match
								if (this.wme.getAttribute(i).equals(other.wme.getAttribute(j))) {
									return false;
								}
							}
						}
						// in all cases, including NEVs, if the first variables are the same, their values should match
						if (this.conditionElement.getLeftAttribute(i).equals(other.conditionElement.getLeftAttribute(j))) {
							if (!this.wme.getAttribute(i).equals(other.wme.getAttribute(j))) {
								return false;
							}
						}
					}
				}
			}
		}
		return true;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("(");
		sb.append(conditionElement.toString());
		sb.append(", ");
		sb.append(wme.toString());
		sb.append(")");
		return sb.toString();
	}
}

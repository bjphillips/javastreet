/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

class Production {
	private String name;
	private final List<ConditionElement> ceList;
	private final List<Action> actionList;
	private final List<String> variableList;
	private boolean sequencing;
	
	Production() {
		ceList = new ArrayList<>();
		actionList = new ArrayList<>();
		variableList = new ArrayList<>();
		sequencing = false;
	}
		
	String getName() {
		return name;
	}

	void setName(String n) {
		name = n;
	}
	
	void setSequencing(boolean sequencing) {
		this.sequencing = sequencing;
	}
	
	boolean isSequencing() {
		return sequencing;
	}
	
	List<ConditionElement> getCEList() {
		return ceList;
	}
	
	List<Action> getActionList() {
		return actionList;
	}

	List<String> getVariableList() {
		return variableList;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("sp {").append(name).append("\n");
		for(int i=0; i<ceList.size(); i++) {
			ConditionElement c = ceList.get(i);
			sb.append("\t").append(c.toString()).append("\n");
		}
		if (!sequencing) {
			sb.append("-->\n");
		} 
		else {
			sb.append("~~>\n");			
		}
		for (int i=0; i<actionList.size(); i++) {
			Action a = actionList.get(i);
			sb.append("\t").append(a.toString()).append("\n");
		}
		sb.append("}\n");
		return sb.toString();
	}
	
	void addCE(ConditionElement cond) {
		ceList.add(cond);
	}

	void addAction(Action act) {
		actionList.add(act);
	}
}


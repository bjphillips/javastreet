/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

/**
 * The internal representation of a productor. Most of this interface is
 * internally used by {@url Agent}.
 */
class Productor {

    Production production;
    List<AlphaBlock> alphaBlocks;
    List<TokenPacket> incomingTokenPackets;
	List<TokenPacket> localTokenPackets;
    List<TokenPacket> outgoingTokenPackets;
    Agent myagent;

    /**
	 * Add a productor for a given production to the agent.
	 * This version sorts the alpha blocks in the productor so that any
	 * negated alpha blocks come last.
	 */
	Productor(Production production, Agent agent) {
        myagent = agent;
        this.production = production;
        alphaBlocks = new ArrayList<>();
        incomingTokenPackets = new ArrayList<>();
        localTokenPackets = new ArrayList<>();
        outgoingTokenPackets = new ArrayList<>();
        // for each ConditionElement in production add an alpha block
        List<ConditionElement> celist = production.getCEList();
        for (ConditionElement c : celist) {
			if (c.isNegated()) {
				alphaBlocks.add(new AlphaBlock(c)); // add negated blocks to the end
			}
			else {
				alphaBlocks.add(0,new AlphaBlock(c)); // add negated blocks to the start
			}
        }
    }

    /**
     * Run the productor for one phase.
	 * At each phase a productor consumes TokenPackets from either
	 * the localTokenPackets buffer or the incomingTokenPackets buffer until
	 * these buffers are empty or the consumption of a TokenPacket triggers
	 * one or more new instantiations.
     */
    void stepPhase() {
		boolean foundNewInstances = false;
		
		while (!foundNewInstances) {
			// pop the next token packet
			TokenPacket nextTokenPacket;
			if (!localTokenPackets.isEmpty()) {
				nextTokenPacket = localTokenPackets.remove(0);
			}
			else if (!incomingTokenPackets.isEmpty()) {
				nextTokenPacket = incomingTokenPackets.remove(0);			
			}
			else {
				return;
			}
			
			// insert all the tokens in nextTokenPacket into all alpha blocks
			for (AlphaBlock a : alphaBlocks) {
				for (Token t : nextTokenPacket) {
					a.insertToken(t);
				}
			}
			
			// remove any words marked as deleted from positive alpha blocks
			// check for critical changes
			// and check if there are any empty positive alpha blocks
			boolean emptyPositiveAlphaBlock = false;
			boolean criticalChange = false;
			for (AlphaBlock a : alphaBlocks) {
				a.preTwiddleFlags();
				if (a.getCriticalChange()) {
					criticalChange = true;
				}
				if (!a.isNegated() && a.size()==0) {
					emptyPositiveAlphaBlock = true;
				}
			}			
			
			// if any of the alpha blocks are empty then there will be no 
			// instances of the rules
			foundNewInstances = false;
			if (!emptyPositiveAlphaBlock && criticalChange) {

				/* The reference algorithm waits until there are only bound and
				 * totally unblund variables in an alpha block before trying
				 * to add that alpha block to a partial instance.
				 * This version of the simulator achieves this by adding all
				 * postive alpha blocks before any negated alpha blocks.
				 * The list of alpha blocks in the productor are sorted with
				 * any negated ones last to achieve this.
				 */
			
				PartialInstanceList newInstances = new PartialInstanceList();

				// for all the alpha blocks that have had a critical change
				for (AlphaBlock a : alphaBlocks) {
					if (a.getCriticalChange()) {
						
						// a positive alpha block that has had a critical change
						if (!a.isNegated()) {
							MatchPair newMatchPair = a.searchFirstNewMatchPair();
							while (newMatchPair != null) {
								PartialInstanceList partialInstances = new PartialInstanceList(new PartialInstance(newMatchPair));
								for (AlphaBlock b : alphaBlocks) {
									if (b != a) {
										PartialInstanceList nextPartialInstances = new PartialInstanceList();
										for (PartialInstance partialInstance: partialInstances) {
											PartialInstanceList partialInstancesToAdd = b.addTo(partialInstance);
											nextPartialInstances.addAll(partialInstancesToAdd);
										}
										partialInstances = nextPartialInstances;
										if (partialInstances.isEmpty()) {
											break;	// we can terminate early if there are no partial instances
										}
									}
								}
								newInstances.addAll(partialInstances);
								newMatchPair = a.searchNextNewMatchPair();
							}
						}
						
						// a negated alpha block that has had a critical change
						if (a.isNegated()) {
							MatchPair newMatchPair = a.searchFirstNewMatchPair();
							while (newMatchPair != null) {
								PartialInstanceList partialInstances = new PartialInstanceList(new PartialInstance(newMatchPair));
								for (AlphaBlock b : alphaBlocks) {
									// unlike the case where a is not negated, we add the alpha block b when b==a
									PartialInstanceList nextPartialInstances = new PartialInstanceList();
									for (PartialInstance partialInstance: partialInstances) {
										PartialInstanceList partialInstancesToAdd = b.addTo(partialInstance);
										nextPartialInstances.addAll(partialInstancesToAdd);
									}
									partialInstances = nextPartialInstances;
									if (partialInstances.isEmpty()) {
										break;	// we can terminate early if there are no partial instances
									}
								}
								newInstances.addAll(partialInstances);
								newMatchPair = a.searchNextNewMatchPair();
							}
						}
					}
				}

				// for every new instance, add the tokens to the outgoing buffer
				// and the local buffer
				boolean firedFirst = false;
				for (PartialInstance partialInstance : newInstances) {
					foundNewInstances = true;
					partialInstance.setVarNames();
					// if this is q sequencing rule, only fire the first instance in a phase
					if (production.isSequencing() && firedFirst) {
						if (myagent.logFirings) {
							myagent.sendMessage("Skipping: " + production.getName() + "\n" + partialInstance.toString() + "\n");
						}
					}
					else {
						firedFirst = true;
						TokenPacket tokenPacket = makeTokenPacket(partialInstance);
						outgoingTokenPackets.add(tokenPacket);
						localTokenPackets.add(tokenPacket);
					}
				}
			}

			//twiddle all flags	
			for (AlphaBlock a : alphaBlocks) {
				a.postTwiddleFlags();
			}
		}
    }

    private TokenPacket makeTokenPacket(PartialInstance inst) {
        if (myagent.logFirings) {
            myagent.sendMessage("Firing: " + production.getName() + "\n" + inst.toString() + "\n");            
        }
        //for each RHS action make a new token
        //in constants
        //
        //find variables in the arg1 of the CEs
        //fill in 
        //if cannot find, make new ID, fill in, store it too
        TokenPacket tokenPacket = new TokenPacket();
        List<Action> actionList = production.getActionList();
        List<String> newIdlist = new ArrayList<>();
        List<String> newIdvarlist = new ArrayList<>();
        for (Action action : actionList) {
            String[] attributes = new String[action.numberOfAttributes()];
            for (int i = 0; i < attributes.length; i++) {
                attributes[i] = "#";
                if (action.getOperation(i) == OperationType.CONST) {
                    attributes[i] = action.getLeftAttribute(i);
                }
            }
    		//if it is a var, then try to find it in the
            //instance var names. If it is not there, generate a
            //new ID. If it is use that
            //if it is there then switch operation type and find second argument
            //and do operation
            for (int i = 0; i < action.numberOfAttributes(); i++) {
                if (action.getOperation(i) != OperationType.CONST) {
                    //find first arg in var list
                    String bind1 = inst.findBinding(action.getLeftAttribute(i));
                    if (bind1 == null) {
						//if not in var list check newIDlist
                        //if not there make new one
                        if (newIdvarlist.indexOf(action.getLeftAttribute(i)) == -1) {
                            bind1 = myagent.newId();
                            newIdlist.add(bind1);
                            newIdvarlist.add(action.getLeftAttribute(i));
                            bind1 = newIdlist.get(newIdvarlist.indexOf(action.getLeftAttribute(i)));
                        } else {
                            bind1 = newIdlist.get(newIdvarlist.indexOf(action.getLeftAttribute(i)));
                        }
                    }
                    String bind2 = null;
                    //if variable type argument find second var in the var list
                    if (action.getOperation(i).isVariable()) {
                        bind2 = inst.findBinding(action.getRightAttribute(i));
                        if (bind2 == null) {
                            //if not in the var list check newIDlist
                            if (newIdvarlist.indexOf(action.getRightAttribute(i)) == -1) {
                                System.err.println("THERE IS A PROBLEM IN THE PRODUCTOR ACTION, SECOND VAR NOT IN VARLIST OR NEWID LIST, THIS SHOULD NOT HAPPEN!!!!!");
                                System.err.println(this.toString());
                                System.exit(-1);
                            } else {
                                bind2 = newIdlist.get(newIdvarlist.indexOf(action.getRightAttribute(i)));
                            }
                        }
                    } //if not variable type argument get second argument from the action
                    else {
                        bind2 = action.getRightAttribute(i);
                    }
                    //do the operation
                    //assign result to I
                    attributes[i] = doRHOp(bind1, bind2, action.getOperation(i));
                }
            }

            WME w = new WME(attributes);
            TokenFlag f = TokenFlag.ADD;
            if (action.isDelete()) {
                f = TokenFlag.DELETE;
            }
            tokenPacket.add(new Token(w, f));
        }
        return tokenPacket;
    }

    private String doRHOp(String arg1, String arg2, OperationType op) {

        if (op == OperationType.VAR) {
            return arg1;
        }
        int a1;
        int a2;
        try {
            a1 = Integer.parseInt(arg1);
            a2 = Integer.parseInt(arg2);
        } catch (NumberFormatException ex) {
            System.err.println(arg1 + " " + arg2);
            return "#";
        }
        switch (op) {
            case CONST:
                return arg1;
            case VAR:
                return arg2;
            case SUB:
            case SUBV:
                return Integer.toString(a1 - a2);
            case ADD:
            case ADDV:
                return Integer.toString(a1 + a2);
            case DIV:
            case DIVV:
                return Integer.toString(a1 / a2);
            case MULT:
            case MULTV:
                return Integer.toString(a1 * a2);
        }
        return "#";
    }

    //copy a token packet into the input buffer
    void writeTokenPacket(TokenPacket p) {
		if (!p.isEmpty()) {
			TokenPacket c = new TokenPacket();
			for (Token t : p) {
				// ignore _OUTPUT tokens
				if (!"_OUTPUT".equals(t.getAttribute(0))) {
					c.add(t);
				}
			}
			incomingTokenPackets.add(c);
		}
    }

    //read a token packet from the outgoing buffer and remove it
    TokenPacket readTokenPacket() {
        if (!outgoingTokenPackets.isEmpty()) {
            return outgoingTokenPackets.remove(0);
        }
        return null;
    }

	//empty all alpha memories and buffers, initialise the productor
    void clear() {
        for (AlphaBlock alphaBlock: alphaBlocks) {
            alphaBlock.clear();
        }
        incomingTokenPackets.clear();
        outgoingTokenPackets.clear();
		localTokenPackets.clear();
    }
	
	// check if all the productor's buffers are empty
	boolean buffersAreEmpty() {
		return (incomingTokenPackets.isEmpty() &&
	        outgoingTokenPackets.isEmpty() &&
			localTokenPackets.isEmpty());
	}

    // print a verbose representation of this productor including
    // its production and the contents of its alpha memories
    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append(production.toString());
		s.append("Incoming token packets:\n");
		for (TokenPacket a: incomingTokenPackets) {
			s.append(a.toString());
		}
		s.append("Local token packets:\n");
		for (TokenPacket a: localTokenPackets) {
			s.append(a.toString());
		}
        for (AlphaBlock alphaBlock: alphaBlocks) {
            s.append(alphaBlock.toString());
        }
        return s.toString();
    }
}

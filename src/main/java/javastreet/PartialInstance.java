/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.util.*;

/**
 * A partial instance is a set of positive match pairs. 
 * Recall that a match pair is a condition element and a WME that satisfies the
 * condition element.
 * An instance is a set of WMEs that satisfy a LHS.
 * An annotated instance the set of positive match pairs corresponding to
 * the WMEs in an instance. 
 * 
 * A partial instance may lead to one or more annotated instances through a 
 * process or adding positive match pairs.
 */
class PartialInstance {
	private final List<MatchPair> matchPairs;
	private List<List<String>> varnames;
	
	PartialInstance() {
		matchPairs = new ArrayList<>();
	}
	
	PartialInstance(MatchPair matchPair) {
		matchPairs = new ArrayList<>();
		matchPairs.add(matchPair);
	}
	
	/**
	 * Construct a new partial instance with clone of the match pairs.
	 */
	PartialInstance(List<MatchPair> matchPairs) {
		this.matchPairs = new ArrayList<>(matchPairs);
	}
	
	@Override
	public String toString() {
		StringBuilder s = new StringBuilder();
		s.append("{");
		for (MatchPair matchPair: matchPairs) {
			s.append(matchPair.toString()).append(" ");
		}
		s.append("}\n");
		if (varnames != null) {
			for (List<String> a : varnames) {
				for (String a1 : a) {
					s.append(a1);
				}
			}
		}
		return s.toString();
	}
	
	/* The variable condition elements in a positive match pair bind the
	 * value of the variable. In a partial instance all of the bound variables
	 * should be bound to the same value.
	 * Recall that the variable conditions are:
     *  <x>         
     *  <x> != C    
     *  <x> < C     
     *  <x> > C     
     *  <x> <= C    
     *  <x> >= C    
     *  <x> != <y>  
	 * And that in every case, the name of the variable (x) is stored in the leftAttribute 
	 * of the condition element.
	 *
	 * This method is used along with findBinding to find the binding of a variable in the
	 * partial instance. 
	 * TO DO: document the notation used (e.g. what does "#" mean) and, probably, refactor
	 * to a more natural data structure.
 	 */
	void setVarNames() {
		varnames = new ArrayList<>();
		for (MatchPair matchPair : matchPairs) {
			List<String> a = new ArrayList<>();
			if (matchPair.isNegated()) {
				for (int j=0; j<matchPair.getConditionElement().numberOfAttributes(); j++) {
					a.add("");				
				}
			} else {
				for (int j=0; j<matchPair.getConditionElement().numberOfAttributes(); j++) {
					if (matchPair.getConditionElement().isVariable(j)) {
						a.add(matchPair.getConditionElement().getLeftAttribute(j));
					} else {
						a.add("#");
					}
				}
			}
			varnames.add(a);
		}
	}
	
	/* Returns true if the other match pair joins all the match pairs in this
	 * partial instance.
	 */
	boolean joinsAllMatchPairs(MatchPair otherMatchPair) {
		for (MatchPair thisMatchPair: matchPairs) {
			if (!thisMatchPair.joinTest(otherMatchPair)) {
				return false;
			}
		}
		return true;
	}
	
	void addMatchPair(MatchPair matchPair) {
		matchPairs.add(matchPair);
	}
	
	PartialInstance getClone() {
		return new PartialInstance(matchPairs);
	}

	String findBinding(String var) {
		int k = 0;
		int i;

		for(i = 0 ; i<varnames.size(); i++) {
			k  = varnames.get(i).indexOf(var);
			if(k != -1) {
				break;
			}
		}
		if(i == varnames.size()) {
			return null;
		}

		WME w = matchPairs.get(i).getWme();
		return w.getAttribute(k);
	}
	
}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

public abstract class Peripheral implements java.util.EventListener {
	ThreadedAgent agent;

	public Peripheral(ThreadedAgent agent) {
		this.agent = agent;
		agent.addPeripheral(this);
	}

	public void handleOutput(TokenPacket tokenPacket) {
	}

	public ThreadedAgent getAgent() {
		return agent;
	}
}


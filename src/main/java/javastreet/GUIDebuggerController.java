/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package javastreet;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;

/**
 * FXML Controller class
 *
 * @author phillips
 */
@SuppressWarnings({"squid:S1172", "squid:S1068"}) // Unused fields and method parameters re normal in this context
public class GUIDebuggerController implements Initializable, MessageHandler {

    @FXML
    private TextArea logWindow;
    @FXML
    private TextField commandField;
    @FXML
    private Button runButton;
    private GUIDebugger debugger;
    @FXML
    private Button pauseButton;
    private final List<String> commandHistory = new ArrayList<>();
    private StringBuilder logBuffer = new StringBuilder();
    private int commandIndex = -1;
    private final static int MAX_COMMAND_HISTORY = 20;
    private final PeriodicLogUpdater periodicLogUpdater = new PeriodicLogUpdater();
    private final Object logUpdaterMonitor = new Object();
    private boolean logUpdaterMonitorSingalled = false;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        periodicLogUpdater.start();
        // start with the command field with the focus
        Platform.runLater(new Runnable() {
            @Override
            public void run() {
                commandField.requestFocus(); 
				// report current version
				logWindow.appendText("JavaStreet Version "+Version.NUMBER+"\n");
            }
        });
    }

    @FXML
    private void commandEntered(ActionEvent event) {
        String commandLine = commandField.getText();
        appendLogWindow(commandLine + "\n");
        commandField.setText("");
        commandHistory.add(0, commandLine);
        commandIndex = -1;
        while (commandHistory.size() > MAX_COMMAND_HISTORY) {
            commandHistory.remove(MAX_COMMAND_HISTORY);
        }
        debugger.debugCommand(commandLine);
		if (debugger.exitRequest) {
			debugger.exit();
		}
    }

    @FXML
    private void runPressed(ActionEvent event) {
        debugger.debugCommand("run");
    }

    void setDebugger(GUIDebugger debugger) {
        this.debugger = debugger;
    }

    @FXML
    private void pausePressed(ActionEvent event) {
        debugger.debugCommand("pause");
    }

    /* A local helper function to add text to the
     * logWindow and remove old text so that it does not 
     * grow to absurd lengths.
     */
    private void appendLogWindow(String s) {
        StringBuilder l = new StringBuilder(logWindow.getText()).append(s);
        if (l.length() > 5000) {
            l.delete(0, l.length() - 5001);
        }
        logWindow.setText(l.toString());
        logWindow.appendText(""); // force a scroll to the bottom
    }

    @FXML
    private void commandKeyPressed(KeyEvent event) {
        switch (event.getCode()) {
            case UP:
                if (commandIndex < commandHistory.size() - 1) {
                    commandIndex += 1;
                    commandField.setText(commandHistory.get(commandIndex));
                }
                event.consume();
                break;
            case DOWN:
                if (commandIndex > 0) {
                    commandIndex -= 1;
                    commandField.setText(commandHistory.get(commandIndex));
                } else if (commandIndex == 0) {
                    commandIndex = -1;
                    commandField.setText("");
                }
                event.consume();
                break;
            default:
                commandIndex = -1;
        }
    }

    /**
     * Buffer any incoming items for the log to be periodically updated by a
     * PeriodicLogUpdater. Without this, when the agent is running it can
     * overwhelm the GUI with updates to the lowWindow. Note that this is called
     * from the agent's thread
     */
    @Override
    public void handleMessage(String log) {
        if (log.length() > 0) {
            logBuffer.append(log);
            // wake the PeriodicLogUpdater if it is asleep
            synchronized (logUpdaterMonitor) {
                logUpdaterMonitorSingalled = true;
                logUpdaterMonitor.notify();
            }
        }
    }

    private class PeriodicLogUpdater extends Thread {

        @Override
        public void run() {
            while (true) {
                if (logBuffer.length() > 0) {
                    // Set appendLogWindow to run later on the JavaFX thread
                    Platform.runLater(new Runnable() {
                        @Override
                        public void run() {
                            appendLogWindow(logBuffer.toString());
                            logBuffer = new StringBuilder();
                        }
                    });
                    // Wait 100ms before updating the window again
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ex) {
                        // no problems, someone just poked us
                    }
                } else {
                    // wait until someone wakes us up
                    // For details refer to: http://tutorials.jenkov.com/java-concurrency/thread-signaling.html
                    if (!logUpdaterMonitorSingalled) {
                        try {
                            synchronized (logUpdaterMonitor) {
                                logUpdaterMonitor.wait();
                            }
                        } catch (InterruptedException ex) {
							// Ignore interruptions
                        }
                    }
                    logUpdaterMonitorSingalled = false;
                }
            }
        }
    }

}

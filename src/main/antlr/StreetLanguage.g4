// Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.

grammar StreetLanguage;

source:
    production*
;

production:
    'st' '{' Identifier     
        lhs 
    arrow
        rhs
    '}'
;

arrow:
    '-->'                                                   # RegularArrow
    | '~~>'                                                 # SequencingArrow
;

lhs:
    positiveConditionElement
    (positiveConditionElement | negatedConditionElement)*
;

negatedConditionElement:
    '-' '(' lhsTest+ ')' 
;

positiveConditionElement:
    '(' lhsTest+ ')'     
;

// The alternatives have been labelled to make it easier for the 
// application code when it walks the syntax tree
lhsTest:
    ('(' Variable relationalOperator (Variable | constant) ')')     # LhsRelation
    | Variable                                              # LhsVariable
    | Wildcard                                              # LhsWildcard
    | relationalOperator constant                           # LhsConstantRelation
    | constant                                              # LhsConstant
;

relationalOperator:
    '<' | '>' | '<=' | '>=' | '!='
;


rhs:
    action+
;

action:
    '-' '(' rhsOperand+ ')'                                 # DeleteAction
    | '(' rhsOperand+ ')'                                   # PositiveAction
;

rhsOperand:
    (Variable binaryOperator (Variable | constant))         # RhsBinaryExpression
    | Variable                                              # RhsVariable
    | constant                                              # RhsConstant
;

binaryOperator:
    '+' | '-' | '*' | '/'
;


integer:
    '-'?PositiveInteger
;

constant:
    Identifier | integer
;

/* L E X E R   R U L E S */

Wildcard:
    '<''*''>'
;

Variable:
    '<'IdentifierString'>'
;

Identifier:
    IdentifierString
;

fragment
IdentifierString:
    ('a'..'z'|'A'..'Z'|'_')('a'..'z'|'A'..'Z'|'_'|'0'..'9')*
;

PositiveInteger:
    ('0'..'9')+
;

Whitespace:
    (' '|'\t'|'\r'|'\n')+ -> skip
;

BlockComment:   
    '/*' .*? '*/' -> skip
;

LineComment:   
    '//' .*? '\r'? '\n' -> skip
;


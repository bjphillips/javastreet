package roach;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;

public class ASCIIRenderer implements Renderer, Runnable {

	public static final int DRAW_DELAY = 400;

	private final boolean showTrails;
	private final boolean debug;
	private final Map map;
	private final List<Robot> robots;
	private final Semaphore robotsMutex;

	public ASCIIRenderer(Map map, boolean showTrails, boolean debug) {
		this.showTrails = showTrails;
		this.debug = debug;
		this.map = map;
		this.robots = new ArrayList<>();
		this.robotsMutex = new Semaphore(1, true);
	}

	@Override
	public void run() {
		while (true) {
			System.out.println("\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n");
			System.out.println("--------------------------------");

			if (debug && !robots.isEmpty()) {
				Coordinate coord = ((RobotSim) robots.get(0)).getCoordinate();
				System.out.printf("(%.0f, %.0f), heading: %d, distance: %d\n", coord.getX(), coord.getY(),
						robots.get(0).getHeading(), robots.get(0).getDistance());
				if (robots.get(0).getLeft()) {
					System.out.printf("left ");
				} else {
					System.out.printf("     ");
				}
				if (robots.get(0).getFront()) {
					System.out.printf("front ");
				} else {
					System.out.printf("      ");
				}
				if (robots.get(0).getRight()) {
					System.out.printf("right");
				}
				System.out.printf("\n");
			}

			int i = 0;
			int j = 0;
			for (i = 0; i < map.getHorizontalLength(); i++) {
				for (j = 0; j < map.getVerticalLength(); j++) {

					if (robotExists(j, i)) {
						System.out.printf("@");
					} else if (map.isFood(j, i)) {
						System.out.printf("F");
					} else if (trailPointExists(j, i)) {
						System.out.printf(".");
					} else if (map.isDark(j, i)) {
						System.out.printf(";");
					} else if (map.isLight(j, i)) {
						System.out.printf("-");
					} else if (map.getWall(j, i) == 0) {
						System.out.printf(" ");
					} else if (map.getWall(j, i) == 1) {
						System.out.printf("#");
					} else {
						System.out.printf("%d", map.getWall(j, i));
					}
				}
				System.out.printf("\n");
			}
			System.out.printf("\n");
			try {
				Thread.sleep(DRAW_DELAY);
			} catch (Exception e) {
			}
		}
	}

	@Override
	public void begin() {
		Thread myThread = new Thread(this);
		myThread.setName("ASCII Renderer");
		myThread.start();
	}

	@Override
	public void addRobot(Robot robot) {
		try {
			robotsMutex.acquire();
		} catch (Exception e) {
		}
		robots.add(robot);
		robotsMutex.release();

	}

	private boolean robotExists(int x, int y) {
		boolean exists = false;
		try {
			robotsMutex.acquire();
		} catch (Exception e) {
		}
		for (Robot r : robots) {
			Coordinate coord = ((RobotSim) r).getCoordinate();
			if ((int) Math.round(coord.getX()) == x && (int) Math.round(coord.getY()) == y) {
				exists = true;
				break;
			}
		}
		robotsMutex.release();
		return exists;
	}

	private boolean trailPointExists(int x, int y) {
		boolean exists = false;
		for (Robot r : robots) {
			if (((RobotSim) r).trailPointExists(x, y)) {
				exists = true;
				break;
			}
		}
		return exists;
	}
}

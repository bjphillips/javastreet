package roach;

import java.util.Objects;
import javastreet.*;

/**
 * An InputPoller periodically checks the status of robot sensors and passes any
 * changes to the agent.
 *
 * @author phillips
 */
public class InputPoller implements Runnable {

	private Boolean front;
	private Boolean left;
	private Boolean right;
	private Integer heading;
	private Integer distance;
	private final ThreadedAgent agent;
	private final Robot robot;
	private Thread myThread;

	InputPoller(ThreadedAgent agent, Robot robot) {
		this.robot = robot;
		this.agent = agent;
		front = robot.getFront();
		left = robot.getLeft();
		right = robot.getRight();
		heading = robot.getHeading();
		distance = robot.getDistance();
		String[] frontAttributes = {"_INPUT", "front", front ? "true" : "false"};
		agent.pushInput(new TokenPacket(frontAttributes, true));
		String[] leftAttributes = {"_INPUT", "left", left ? "true" : "false"};
		agent.pushInput(new TokenPacket(leftAttributes, true));
		String[] rightAttributes = {"_INPUT", "right", right ? "true" : "false"};
		agent.pushInput(new TokenPacket(rightAttributes, true));
		String[] headingAttributes = {"_INPUT", "heading", heading.toString()};
		agent.pushInput(new TokenPacket(headingAttributes, true));
		String[] distanceAttributes = {"_INPUT", "distance", distance.toString()};
		agent.pushInput(new TokenPacket(distanceAttributes, true));
	}

	@Override
	public void run() {
		while (true) {
			Boolean newFront = robot.getFront();
			if (!Objects.equals(newFront, front)) {
				String[] oldAttributes = {"_INPUT", "front", front ? "true" : "false"};
				agent.pushInput(new TokenPacket(oldAttributes, false));
				String[] newAttributes = {"_INPUT", "front", newFront ? "true" : "false"};
				agent.pushInput(new TokenPacket(newAttributes, true));
				front = newFront;
			}
			Boolean newLeft = robot.getLeft();
			if (!Objects.equals(newLeft, left)) {
				String[] oldAttributes = {"_INPUT", "left", left ? "true" : "false"};
				agent.pushInput(new TokenPacket(oldAttributes, false));
				String[] newAttributes = {"_INPUT", "left", newLeft ? "true" : "false"};
				agent.pushInput(new TokenPacket(newAttributes, true));
				left = newLeft;
			}
			Boolean newRight = robot.getRight();
			if (!Objects.equals(newRight, right)) {
				String[] oldAttributes = {"_INPUT", "right", right ? "true" : "false"};
				agent.pushInput(new TokenPacket(oldAttributes, false));
				String[] newAttributes = {"_INPUT", "right", newRight ? "true" : "false"};
				agent.pushInput(new TokenPacket(newAttributes, true));
				right = newRight;
			}
			Integer newHeading = robot.getHeading();
			if (!Objects.equals(newHeading, heading)) {
				String[] oldAttributes = {"_INPUT", "heading", heading.toString()};
				agent.pushInput(new TokenPacket(oldAttributes, false));
				String[] newAttributes = {"_INPUT", "heading", newHeading.toString()};
				agent.pushInput(new TokenPacket(newAttributes, true));
				heading = newHeading;
			}
			Integer newDistance = robot.getDistance();
			if (!Objects.equals(newDistance, distance)) {
				String[] oldAttributes = {"_INPUT", "distance", distance.toString()};
				agent.pushInput(new TokenPacket(oldAttributes, false));
				String[] newAttributes = {"_INPUT", "distance", newDistance.toString()};
				agent.pushInput(new TokenPacket(newAttributes, true));
				distance = newDistance;
			}
			try {
				Thread.sleep(50); // ms
			} catch (InterruptedException e) {
			}
		}
	}

	void start() {
		myThread = new Thread(this);
		myThread.setName("Input Poller");
		myThread.start();
	}
}

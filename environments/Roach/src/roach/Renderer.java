package roach;

public interface Renderer {
  public void run();  
  public void begin();
  public void addRobot(Robot robot);
}
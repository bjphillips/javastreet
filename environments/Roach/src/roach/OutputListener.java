package roach;

import javastreet.*;

/**
 * Listens to outputs from the agent and passes relevant ones to the robot.
 *
 * @author phillips
 */
public class OutputListener extends Peripheral {

	private Robot robot;
	private ThreadedAgent agent;

	public OutputListener(ThreadedAgent agent, Robot robot) {
		super(agent);
		this.agent = agent;
		this.robot = robot;
	}

	@Override
	public void handleOutput(TokenPacket tokenPacket) {

		for (Token token : tokenPacket) {
			int angle;
			
			// is this (_OUTPUT forward)?
			if (token.numberOfAttributes() == 2
					&& token.getAttribute(1).equals("forward")
					&& token.getFlag().equals(TokenFlag.ADD)) {
				robot.forwardIncrement();
				String[] attributes = {"_INPUT", "forwardDone"};
				agent.pushInput(new TokenPacket(attributes, true));
			}
			// is this -(_OUTPUT forward)?
			if (token.numberOfAttributes() == 2
					&& token.getAttribute(1).equals("forward")
					&& token.getFlag().equals(TokenFlag.DELETE)) {
				String[] attributes = {"_INPUT", "forwardDone"};
				agent.pushInput(new TokenPacket(attributes, false));
			}
			// is this (_OUTPUT rotate <a>)?
			if (token.numberOfAttributes() == 3
					&& token.getAttribute(1).equals("rotate")
					&& token.getFlag().equals(TokenFlag.ADD)) {
				try {
					angle = Integer.parseInt(token.getAttribute(2));
				}
				catch (NumberFormatException ex) {
					System.err.println("Invalid output from the agent: "+token.toString());
					continue;
				}
				robot.rotate(angle);
				String[] attributes = {"_INPUT", "rotateDone"};
				agent.pushInput(new TokenPacket(attributes, true));
			}
			// is this -(_OUTPUT rotate <a>)?
			if (token.numberOfAttributes() == 3
					&& token.getAttribute(1).equals("rotate")
					&& token.getFlag().equals(TokenFlag.DELETE)) {
				String[] attributes = {"_INPUT", "rotateDone"};
				agent.pushInput(new TokenPacket(attributes, false));
			}
		}
	}
}

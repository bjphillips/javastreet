package roach;


public interface Robot {
  public boolean getFront();
  public boolean getLeft();
  public boolean getRight();
  // returns heading
  public int rotate(int angle);
  // returns total distance 
  public int forwardIncrement();
  public boolean isDark();  
  public boolean isLight();
  public boolean isFood();
  public int getHeading();
  public int getDistance();
}
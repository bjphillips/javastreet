\documentclass[a4paper,11pt]{article}
\usepackage{graphicx}
\usepackage{hyperref}
\title{Subsumption Architecture Primer}
\author{James Donovan}
\date{2016}

\begin{document}
\maketitle

\section{Overview}

Subsumption architecture was created with the goal of being able to stack
simple artificial intelligence behaviours on top of each other to create
complex behaviour [1 Section I]. The idea is that low level behaviours will
continue to work without re-implementing them into each advanced level of
behaviour.

Subsumption architecture is an alternative to traditional AI, which usually
consists of taking in readings from sensors, analysing the data to produce a
representation of the environment and then deciding what signals to send to
actuators to respond to the environment. Subsumption architecture instead
closely relates sensor readings to actions performed, with no complete
analysis of the environment to decide what to do next. Instead, many layers of
control have their own behaviour that incrementally affects the total
behaviour of the system.

Systems created with this architecture have the advantage of being able to
react to the environment within a human-like timeframe, are tested thoroughly
as each iteration test covers the new and old behaviours, and have parallel
perception, control, and action systems, similar to animals [8].

\section{Levels of Competence}

This AI software architecture focuses on a hierarchy of behaviours [1 Section
II-A]. The behaviour that has no reliances on other behaviours is a level 0
behaviour. A behaviour that relies only on the level 0 behaviour is level 1,
and so on, until every desirable behaviour has a level assigned. Each level of
desirable behaviour is termed a level of competence.

\section{Layers of Control}

Each level of competence has a corresponding layer of control, which specifies
modules and the data paths between them to satisfy the level of competence [1
Section II-B]. Modules are the functional parts of the architecture that
consist of inputs, outputs, data storage, and various possible states, which
will be explained in detail later. The data paths in a layer of control
specify which module outputs connect to which module inputs. A layer of
control cannot change the data paths created by lower layers of control, but
they can use inhibition and suppression mechanisms to alter the inputs and
outputs of a module to achieve desirable behaviour [1 Section III-B]. An
inhibitor can be connected from the output of one module to the output data
path of another module. If a signal is sent to the inhibitor from the output
of the first module, signals sent along the inhibited data path by the other
module will be lost until the specified time has passed. A suppressor is
similar to an inhibitor, but is placed from the output of one module to the
input data path of another. It is also used to ‘hi-jack’ the data line so that
a different module’s output can be used as the input instead. The suppressor
is active when data is output over the suppressing line, and the previous data
path is reconnected after the time specified by the suppressor has passed.
Inhibitors and suppressors are used when a pre-established data path from a
lower layer of control must be altered to achieve a higher level of
competence.
 
\begin{figure}
\begin{center}
\includegraphics[scale=1]{./GenericModule.png}
\caption{Unnamed module with Suppressor on one input data path and Inhibitor
on one output data path [1]}
\label{fig:GenericModule}
\end{center}
\end{figure}

Figure~\ref{fig:GenericModule} shows a module with a suppressor on one of the
input data paths, and an inhibitor on one of the output data paths. The
suppressor and inhibitor are connected to outputs of other modules not shown
in the diagram (and hence controlled by these modules). Note that the number
of units of time to inhibit the signal is shown in the circles.

\section{Modules}
Each subsumption architecture module has a number of inputs and outputs, a
finite state machine, and data storage (instance variables) separate to every
other module [1 Section III-A]. Each FSM runs asynchronously to every other
module’s FSM. Only the latest signal is stored at the inputs, so if it is not
analysed before the next signal arrives, it will be lost forever. Each FSM has
a NIL state that they initially start in, and each module has a reset signal
that sets the state back to NIL.

There are four kinds of states possible in a subsumption architecture FSM: 
\begin{itemize}
\item Output: an output message is generated using stored variables and
current inputs, and is sent along the appropriate output line from the module.
A specified state is entered afterwards.
\item Side effect: An instance variable has a new value set, determined by
instance variables and current inputs. A specified state is entered
afterwards.
\item Conditional dispatch: A result is generated based on instance variables
and current inputs. Based on this result, one of two new states will be
entered.
\item Event dispatch: Input lines are monitored, and once one of multiple
conditions is met, the next state will be chosen. Different states can be
moved to depending on the current input and conditions required to move to
certain states.
\end{itemize}

\section{Real-world Uses}

This AI architecture is particularly effective for robots that are designed to
navigate autonomously and interact in real time with the environment. Brooks
(the creator of subsumption architecture) made a robot that was able to avoid
obstacles and explore a room by either moving to the furthest sensed location
[1] or following the walls [2], with implementation details included in the
references. A video of a robot with similar characteristics to the former is
shown in [3], with particularly interesting behaviour at time 4:18 in the
video, showing that the robot is able to explore wide open areas fairly well.
The most notable example of a commercial product using the subsumption
architecture is the Roomba, developed by iRobot [4], a company cofounded by
Brooks. This product has similar behaviour to the previous example, where it
is able to explore (and vacuum) a room without creating any internal maps. An
insect-like robot also developed by Brooks is shown in [5], with the ability
to climb over obstructions. Several other robots created using subsumption
architecture are described in Elephants Don’t Play Chess [7 pp.9-12].

\section{Limitations and Criticisms}

Some criticisms exist towards the architecture. One machine intelligence
researcher criticises that the complexity of the system increases greatly when
adding more complicated behaviours to an already complicated system. 

``Once you start stacking all these modules together, it becomes more and more
difficult for the programmer to decide that, yes, an asynchronous local module
which raises the robotic leg higher when it detects a block, and meanwhile
sends asynchronous signal X to module Y, will indeed produce effective
behavior as the outcome of the whole intertwined system whereby intelligence
emerges from interaction with the environment...'' – E. Yudkowsky [6].

Other weaknesses include the restriction on being able to understand
complicated concepts and language due to lack of large amounts of memory and
the lack of symbolic representation (a form of logical reasoning) used in
traditional forms of AI [8].

\begin{thebibliography}{99}

\bibitem{brooks86a} R. Brooks, A Robust Layered Control System for a Mobile
Robot, IEEE Journal of Robotics and Automation, Vol. RA-2, NO. 1, March 1986,
pp 14 – 23, Available:
\url{http://ieeexplore.ieee.org/xpls/abs_all.jsp?arnumber=1087032&tag=1}

\bibitem{brooks86b} R. Brooks, J. Connell, Asynchronous distributed control
system for a mobile robot, MIT Artificial Intelligence Lab, Cambridge 1986,
Available: \url{http://proceedings.spiedigitallibrary.org/data/conferences/spiep/65690/0727_77.pdf}

\bibitem{francis13} Jay Francis, DinoBot - A Simple Robot Based on the
Subsumption Architecture, YouTube, 2013, Available:
\url{https://www.youtube.com/watch?v=7_7GDFnBBV0}

\bibitem{reiners07} P. Reiners, Robots, mazes, and subsumption architecture,
IBM developerWorks, 2007, Available:
\url{http://www.ibm.com/developerworks/library/j-robots/}

\bibitem{skitterbot08}  skitterbot, Ghenghis robot, YouTube, 2008, Available:
\url{https://www.youtube.com/watch?v=K2xUHYFcYKI}

\bibitem{yudkowsky08} E. Yudkowsky, Selling Nonapples, LessWrong, 2008,
Available: \url{http://lesswrong.com/lw/vs/selling_nonapples/}

\bibitem{brooks90} R. Brooks, Elephants Don’t Play Chess, MIT Artificial
Intelligence Laboratory, Cambridge, 1990, Available:
\url{https://books.google.com.au/books?id=cK-1pavJW98C&pg=PA3-IA4&dq=Elephants+Don%27t+Play+Chess&hl=en#v=onepage&q=Elephants%20Don%27t%20Play%20Chess&f=false}

\bibitem{wikipedia2015} et al, Subsumption Architecture, Wikipedia, 2015,
Available: \url{https://en.wikipedia.org/wiki/Subsumption_architecture}
\end{thebibliography}
\end{document}

\documentclass[a4paper,11pt]{article}
\usepackage{graphicx}
\usepackage{listings}
\usepackage[margin=2.5cm]{geometry}
\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}
\title{Subsumption Architecture Cockroach AI Modules}
\author{James Donovan}
\date{2016}

\begin{document}
  \maketitle
  \newpage
  \noindent This document consists of an in-depth description of the levels of competence and layers of control for the Subsumption Architecture Cockroach AI.
  \tableofcontents
  \newpage
  \section{Levels of Competence}
  \label{levels_of_competence}
    The following levels of competence specify the goal of each increment of functionality for the Cockroach AI. 
    Note that the diagrams showing the layers of control in section \ref{layers_of_control} follow the same colour coding convention.
    \begin{description}
      \item[Level 0:] Line up with wall
      \item[{\color{blue}Level 1}:] Move forward
      \item[{\color{orange}Level 2}:] Change direction at random when following wall
      \item[{\color{red}Level 3}:] Navigate to dark area when tired and frightened, sleep when tired and in dark area
      \item[{\color{green}Level 4}:] Navigate to and eat food when hungry
    \end{description}

  \newpage
  \section{Layers of Control}
    \label{layers_of_control}
    The layers of control corresponding to each level of competence described in section \ref{levels_of_competence} are shown in this section. 
    The data path diagrams below show how each module connects to every other module. The modules that interface with the hardware have either a one 
    sided arrow (indicating that they only receive information) or a two way arrow (indicating that they may send and receive data from the hardware).
    Note that the data path diagrams below use the same colour coding convention as in section \ref{levels_of_competence}. 
    The inhibition/suppression times specified are in milliseconds. \\
    
    Each module consists of inputs, outputs, instance variables, states, and functions.
    The instance variables are private variables belonging to each module that store some kind of data. Functions may be called with module inputs and instance variables as arguments, and return
    a value that may be outputted, or stored into an instance variable.
    Each state performs one of the following operations:
    \begin{description}
      \item[Conditional dispatch:] Branch to one of two states depending on whether the condition is true or false. \\ e.g.: 
                                   \textit{cond. dispatch: variable ? state = STATE1 : STATE2} \\ If variable is true, the next state will be STATE1, else it will be STATE2.
      \item[Event dispatch:] The next state is selected when a specified condition is met when receiving on an input line. \\ e.g.:
                                   \textit{event dispatch: (on receive (input1) AND condition(input1), state = STATE1) 
                                  OR (on receive (input2) AND condition(input2), state = STATE2)} \\ If an input on input1 is received and a condition involving input1 is true,
                                  the next state will be STATE1. If an input on input2 is received and a condition involving input2 is true, then the next state will be STATE2.
      \item[Side effect:] Sets an instance variable to specified value or a return value of a function. \\ e.g.:
                                   \textit{set variable (function()), state = STATE1} \\ Sets variable to the value returned by the function and set the state to STATE1.
      \item[Output:] Outputs a value on an output line specified by either a function return value, specified value, or instance variable. \\ e.g.:
                                   \textit{output outputLine (variable), state = STATE1} \\ The value of variable is outputted on outputLine and the state is set to STATE1.
      \item[Function call:] A function can be called with no other effects on the system. This is mostly useful for delaying. \\ e.g.:
                                   \textit{sleep(), state = STATE1} \\ the sleep function is called and the state is set to STATE1.
    \end{description}
    
    \noindent Also note that in some modules, some instance variables have the suffix \textit{\textunderscore v}. These are named that way to avoid clashes between inputs, outputs, or functions with the same name.
    
    \newpage
    \subsection{Layer 0}
      This layer corresponds to the first level of competence, where the robot rotates to line itself up approximately parallel to the wall.
      
      \begin{figure}[h]
        \begin{centering}
          \includegraphics[scale=0.35] {layer0.png}
          \caption{Data Path for Layer 0}
          \label{layer0}
        \end {centering}
      \end{figure}  
      
      \subsubsection{WallDetector}
        \textit{WallDetector} reads touch sensor information and determines whether it is facing a wall, parallel to a wall, or not touching a wall.
        \begin{description} 
          \item[Inputs:] none
          \item[Outputs:] wallStatus
          \item[Instance variables:] bumper, left, right
          \item[States:] \hfill
          \begin{description}
            \item[NIL:] set bumper (getFront()), state = GETLEFT
            \item[GETLEFT:] set left (getLeft()), state = GETRIGHT
            \item[GETRIGHT:] set right (getRight()), state = OUT
            \item[OUT:] output wallStatus (getWallStatus(bumper, left, right)), state = NIL
          \end{description}
          \item[Functions:] \hfill
          \begin{description}
            \item[getFront():] returns true if robot's front sensor is activated, else false.
            \item[getLeft():] returns true if robot's left sensor is activated, else false.
            \item[getRight():] returns true if robot's right sensor is activated, else false.
            \item[getWallStatus(bumper, left, right):] returns \textit{no wall} if no sensors are active, 
                  \textit{left wall} if only left sensor, \textit{rotate right} if left and front sensor, 
                  \textit{right wall} if only right sensor, \textit{rotate left} if right and front sensor,
                  else return \textit{rotate random}.
          \end{description}
        \end{description}
      
      \subsubsection{WallHugger}
        \textit{WallHugger} gets the wall status and outputs an angle to rotate by to align the robot with a wall, if rotating is required.
        \begin{description}
          \item[Input:] wallStatus
          \item[Output:] angle
          \item[Instance variables:] angle\textunderscore v
          \item[States:] \hfill
          \begin{description}
            \item[NIL:] event dispatch: on receive(wallStatus), state = GET
            \item[GET:] set angle\textunderscore v (getAngle(wallStatus)), state = CHECK
            \item[CHECK:] cond. dispatch: notEqualZero(angle\textunderscore v) ? state = OUT : NIL
            \item[OUT:] output angle (angle\textunderscore v), state = NIL
          \end{description}
          \item[Functions:] \hfill
          \begin{description}
            \item[getAngle(wallStatus):] Returns a small positive number to rotate by (approx. 5 degrees) if \textit{rotate right}, a small negative number
                                         if \textit{rotate left}, small number with random sign if \textit{rotate random}, or zero if \textit{no wall}, 
                                         \textit{left wall}, or \textit{right wall}.
            \item[notEqualZero(value):] returns true if the value is not equal to zero, else false.
          \end{description}
        \end{description}
      
      \subsubsection{Turn}
        \textit{Turn} interfaces with the robot's motors to turn the wheels when the module receives an angle to rotate by.
        It also keeps track of the current heading relative to the starting direction.
        \begin{description}
          \item[Input:] angle
          \item[Output:] heading
          \item[Instance variables:] heading\textunderscore v
          \item[States:] \hfill
          \begin{description}
            \item[NIL:] (event dispatch: on receive (angle), state = BUSY), output busy (false)\footnote{Note that two state operations are combined into one.
                         This module will constantly output the busy signal as false when in the NIL state.
                         This can be realised in a strict subsumption-compliant machine by having a separate module constantly outputting to
                         this module and event dispatching to a state that outputs the busy signal. 
                         Also, busy is constantly output so that later on when it is inhibited, forward will know immidiately that it can move once Turn is no longer inhibited.}
            \item[BUSY:] output busy (true), state = START
            \item[START:] set heading\textunderscore v (rotate(angle, heading\textunderscore v)), state = OUT
            \item[OUT:] output heading (heading\textunderscore v), state = FIN
            \item[FIN:] output busy (false), state = NIL
          \end{description}
          \item[Functions:] \hfill
          \begin{description}
            \item[rotate(angle, heading):] Rotates robot by \textit{angle}, and returns the addition of the angle and heading.
          \end{description}
        \end{description}
      
      \newpage
      \subsection{Layer 1}
        This layer adds the functionality that allows the robot to move forwards when not rotating.
        
        \begin{figure}[h]
          \begin{centering}
            \includegraphics[scale=0.32] {layer1.png}
            \caption{Data Path for Layer 1}
            \label{layer1}
          \end {centering}
        \end{figure} 
      
        \subsubsection{Forward}
          \textit{Forward} moves the robot forward in small increments when the robot is not turning. It also keeps track of the total number of movement increments
                           since the beginning of execution.
          \begin{description}
            \item[Input:] busy
            \item[Output:] distance
            \item[Instance variables:] distance\textunderscore v
            \item[States:] \hfill
            \begin{description}
              \item[NIL:] cond. dispatch: busy ? state = NIL : DELAY
              \item[DELAY:] sleep()\footnote{Module is put to sleep to avoid moving forward between rotation increments from the Turn module.}, state = CHECK
              \item[CHECK:] cond. dispatch: busy ? state = NIL : START
              \item[START:] set distance\textunderscore v (forwardIncrement(distance\textunderscore v)), state = OUT
              \item[OUT:] output distance (distance\textunderscore v), state = CHECK
            \end{description}
            \item[Functions:] \hfill
            \begin{description}
              \item[sleep()]: Pause execution of this module for a short amount of time (approx. 20 ms).
              \item[forwardIncrement(distance):] Moves both motors forward by a small increment (approx. 1 cm) and returns the current distance value incremented by one.
            \end{description}
          \end{description}
      
      \newpage
      \subsection{Layer 2}
        This layer's functionality causes the robot to randomly depart from walls when following them.
      
        \begin{figure}[h]
          \begin{centering}
            \includegraphics[scale=0.32] {layer2.png}
            \caption{Data Path for Layer 2}
            \label{layer2}
          \end {centering}
        \end{figure} 
          
        \subsubsection{RandomAngleEvent}
          \textit{RandomAngleEvent} outputs a random angle at a random time when lined up with a wall.
          \begin{description}
            \item[Input:] wallStatus
            \item[Output:] angle
            \item[Instance variables:] huggingStatus\textunderscore v
            \item[States:] \hfill
            \begin{description}
              \item[NIL:] set huggingStatus\textunderscore v (getHugging(wallStatus))\footnote{huggingStatus saved separately to wallStatus 
                          so that the value cannot change once checked}, state = CHECK
              \item[CHECK:] cond. dispatch: isHugging(huggingStatus\textunderscore v) ? state = START : NIL
              \item[START:] cond. dispatch: randomBoolean() ? state = OUT : NIL
              \item[OUT:] output angle (randomAngle(huggingStatus\textunderscore v)), state = NIL
            \end{description}
            \item[Functions:] \hfill
            \begin{description}
              \item[getHugging(wallStatus):] returns \textit{left} if wallStatus is left wall, \textit{right} if right wall, else returns \textit{no wall}.
              \item[isHugging(huggingStatus):] returns true if not \textit{no wall}.
              \item[randomBoolean():] returns true with a very small chance (depends on execution speed), else false.
              \item[randomAngle(huggingStatus):] returns a random positive angle if \textit{left} or a random negative angle if \textit{right}. The angles
                                                 are limited to between -180 and 180 degrees, but may be less to avoid the robot turning backwards.
            \end{description}
          \end{description}
        
        \newpage
        \subsection{Layer 3}
          This layer includes frightness and tiredness levels, making the robot go back to a dark area when frightened or to sleep.
          
          \begin{figure}[h]
            \begin{centering}
              \includegraphics[scale=0.29] {layer3.png}
              \caption{Data Path for Layer 3}
              \label{layer3}
            \end {centering}
          \end{figure} 
          
          \subsubsection{Tiredness}
            \textit{Tiredness} keeps track of the tired level and consumes it while awake and restores it when sleeping.
            \begin{description}
              \item[Input:] resting
              \item[Output:] isDark, tiredLevel, reset
              \item[Instance variables:] isDark\textunderscore v, tiredLevel\textunderscore v
              \item[States:] \hfill
              \begin{description}
                \item[NIL:] sleep(), state = DARKCHECK
                \item[CHECKDARK:] set isDark\textunderscore v (isDark()), state = BRANCH
                \item[BRANCH:] cond. dispatch: isDark\textunderscore v ? state = OUTRESET : OUTDARK
                \item[OUTRESET:] output reset (true), state = OUTDARK
                \item[OUTDARK:] output isDark (isDark\textunderscore v), state = CHECK
                \item[CHECK:] cond. dispatch: resting ? state = RESTORE : CONSUME
                \item[RESTORE:] set tiredLevel\textunderscore v (restore(tiredLevel\textunderscore v)), state = OUTLEVEL
                \item[CONSUME:] set tiredLevel\textunderscore v (consume(tiredLevel\textunderscore v)), state = OUTLEVEL
                \item[OUTLEVEL:] output tiredLevel (tiredLevel\textunderscore v), state = NIL
              \end{description}
              \item[Functions:] \hfill
              \begin{description}
                \item[sleep():] Pause module execution (typically one second)\footnote{This value is roughly the rate at which the robot 
                                consumes/restores tiredness and may differ from one second, but restore and consume amounts will need to be compensated.}.
                \item[isDark():] Reads the light sensor and returns true if the value is below a threshold that doesn't let dark light through, else false.
                \item[restore(tiredLevel):] Returns the result of the restore amount (suggested 30 per second) subtracted from the tired level. Note that it cannot return a number below zero.
                \item[consume(tiredLevel):] Returns the result of the consume amount (suggested 10 per second) added to the tired level. This function should have an upper limit (suggested 300).
              \end{description}
            \end{description}
            
          \subsubsection{Frightness}
            \textit{Frightness} keeps track of the fright level of the robot, consuming when bright light and restoring when not.
              \begin{description}
                \item[Input:] none
                \item[Output:] frightLevel
                \item[Instance variables:] frightLevel\textunderscore v
                \item[States:] \hfill
                \begin{description}
                  \item[NIL:] sleep(), state = CHECKLIGHT
                  \item[CHECKLIGHT:] cond. dispatch: isLight() ? state = CONSUME : RESTORE
                  \item[RESTORE:] set frightLevel\textunderscore v (restore(frightLevel\textunderscore v)), state = OUTLEVEL
                  \item[CONSUME:] set frightLevel\textunderscore v (consume(frightLevel\textunderscore v)), state = OUTLEVEL
                  \item[OUTLEVEL:] output frightLevel (frightLevel\textunderscore v), state = NIL
                \end{description}
                \item[Functions:] \hfill
                \begin{description}
                  \item[sleep():] Pause module execution (typically one second).
                  \item[isLight():] Reads the light sensor and returns true if the value is above a threshold that only lets bright light through, else false.
                  \item[restore(frightLevel):] Returns the result of the restore amount (suggested 40 per second) subtracted from the fright level. Note that it cannot return a number below zero.
                  \item[consume(frightLevel):] Returns the result of the consume amount (suggested 80 per second) added to the fright level. This function should have an upper limit (suggested 300).
                \end{description}
              \end{description}
          
          \subsubsection{IntegrateDark}
            \textit{IntegrateDark} uses path integration to keep track of the last dark spot visited by the robot.
            \begin{description}
              \item[Input:] heading, distance, reset(incrDistance, position)\footnote{If a signal on the reset input is received, the variables 
                                                                            within the brackets are reset and the state is set to NIL.}
              \item[Output:] darkPosition
              \item[Instance variables:] prevHeading, prevDistance, incrDistance, position
              \item[States:] \hfill
              \begin{description}
                \item[NIL:] event dispatch: (on receive (heading) AND notEqual(heading, prevHeading), state = SETINCR) 
                            OR (on receive (distance) AND notEqual(distance, prevDistance), state = SETINCR)
                \item[SETINCR:] set incrDistance (subtract(distance, prevDistance)), state = SETPOS
                \item[SETPOS:] set position(pathIntegrate(heading, incrDistance, position)), state = SETPREVHEAD
                \item[SETPREVHEAD:] set prevHeading (heading), state = SETPREVDIST
                \item[SETPREVDIST:] set prevDistance (distance), state = OUT
                \item[OUT:] output darkPosition (position), state = NIL
              \end{description}
              \item[Functions:] \hfill
              \begin{description}
                \item[notEqual(value1, value2):] returns true if the two values are not equal, else false.
                \item[subtract(value1, value2):] returns the result when subtracting value2 from value1.
                \item[pathIntegrate(heading, distance, position):] returns the vector sum of the position vector and a vector with the heading and distance specified.
              \end{description}
            \end{description}
          
          \subsubsection{PathPlanDark}
            \textit{PathPlanDark} takes control of the robot to navigate to a dark spot when scared/tired. 
                    The stored dark point can time-out if a specified amount of time has elapsed without finding a new dark point.
            \begin{description}
              \item[Input:] darkPosition, heading, frightLevel, tiredLevel, isDark
              \item[Output:] angle, resting
              \item[Instance variables:] hasFoundDark, ttlDark, resting\textunderscore v
              \item[States:] \hfill
              \begin{description}
                \item[NIL:] sleep(), state = CHECKFOUND
                \item[CHECKFOUND:] cond. dispatch: hasFoundDark ? state = TTLDARK : CHECKDARK
                \item[CHECKDARK:] cond. dispatch: isDark ? state = SETDARK : NIL
                \item[SETDARK:] set hasFoundDark (true), state = TTLSET
                \item[TTLDARK:] cond. dispatch: isDark ? state = TTLSET : TTLCHECK
                \item[TTLSET:] set ttlDark (currentTime()), state = TTLCHECK
                \item[TTLCHECK:] set hasFoundDark (subtract(currentTime(), ttlDark), state = CHECK1
                \item[CHECK1:] cond. dispatch: requiresSleep(tiredLevel, resting) ? state = SLEEP1 : CHECK2
                \item[CHECK2:] cond. dispatch: resting ? state = SLEEPEND : FRIGHT1
                \item[SLEEP1:] cond. dispatch: resting ? state = SLEEP3 : SLEEP2
                \item[SLEEP2:] output angle (returnToDark(darkPosition, heading), state = SLEEP3
                \item[SLEEP3:] cond. dispatch: isDark ? state = SLEEP4 : SLEEPEND
                \item[SLEEP4:] set resting\textunderscore v (true), state = SLEEP5
                \item[SLEEP5:] output resting (true), state = NIL
                \item[SLEEPEND:] set resting\textunderscore v (false), state = SLEEPEND2
                \item[SLEEPEND2:] output resting (false), state = SLEEPEND3
                \item[SLEEPEND3:] cond. dispatch: requiresSleep(tiredLevel, resting) ? state = NIL : FRIGHT1
                \item[FRIGHT1:] cond. dispatch: requiresHide(frightLevel) ? state = FRIGHT2 : NIL
                \item[FRIGHT2:] output angle (returnToDark(darkPosition, heading), state = NIL
              \end{description}
              \item[Functions:] \hfill
              \begin{description}
                \item[sleep():] Pause module execution for time specified (suggested 300 ms)\footnote{this value determines how often the robot will change direction to seek a dark area.
                                A higher value will also make it likely that the robot will skip small amounts of darkness (and will need to inhibit/suppress Turn and Forward a longer time).}.
                \item[currentTime():] Returns the current system time in milliseconds.
                \item[subtract(value1, value2):] Returns the result of subtracting value2 from value1.
                \item[requiresSleep(tiredLevel, resting):] Returns true if resting and tiredLevel is greater than zero, or if tiredLevel is greater than a threshold (suggested 150), else returns false.
                \item[returnToDark(darkPosition, heading):] Adds 180 degrees to the darkPosition's angle and returns the result of that angle added to the heading.
                \item[requiresHide(frightLevel):] Returns true if frightLevel is greater than a threshold (suggested 150), else returns false.
              \end{description}
            \end{description}
          
        \newpage
        \subsection{Layer 4}
          This layer is responsible for providing the robot with a hunger level and the ability to seek and eat food previously found when hungry.
          
          \begin{figure}[h]
            \begin{centering}
              \includegraphics[scale=0.29] {layer4.png}
              \caption{Data Path for Layer 4}
              \label{layer4}
            \end {centering}
          \end{figure} 
        
          \subsubsection{Hunger}
            \textit{Hunger} keeps track of the hunger level of the robot and consumes it when not eating and restores it when eating.
            \begin{description}
            \item[Input:] none
            \item[Output:] reset, hungerLevel, isFood
            \item[Instance variables:] hungerLevel\textunderscore v
            \item[States:] \hfill
            \begin{description}
              \item[NIL:] delay(), state = CHECKFOOD
              \item[CHECKFOOD:] cond. dispatch: isFood() ? state = RESET : CONSUME
              \item[RESET:] output reset (true), state = OUT1
              \item[OUT1:] output isFood (true), state = RESTORE
              \item[RESTORE:] set hungerLevel\textunderscore v (restore(hungerLevel\textunderscore v)), state = OUTLEVEL
              \item[CONSUME:] set hungerLevel\textunderscore v (consume(hungerLevel\textunderscore v)), state = OUT2
              \item[OUT2:] output isFood (false), state = OUTLEVEL
              \item[OUTLEVEL:] output hungerLevel (hungerLevel\textunderscore v), state = NIL
            \end{description}
            \item[Functions:] \hfill
            \begin{description}
              \item[sleep():] Pause module execution (typically one second).
              \item[isFood():] Reads the IR sensor and returns true if the value is above a threshold that is reached when close to food (represented by IR LEDs).
              \item[restore(hungerLevel):] Returns the result of the restore amount (suggested 40 per second) subtracted from the hunger level. Note that it cannot return a number below zero.
              \item[consume(hungerLevel):] Returns the result of the consume amount (suggested 15 per second) added to the hunger level. This function should have an upper limit (suggested 300).
            \end{description}
          \end{description}
        
          \subsubsection{IntegrateFood}
            \textit{IntegrateFood} uses path integration to keep track of the last food spot visited by the robot. This module is identical to the layer 3's IntegrateDark module except that
            the output is named \textit{foodPosition}, so it will not be described any further here.
            
        
          \subsubsection{PathPlanFood}
          \textit{PathPlanFood} takes control of the robot to navigate to a food spot when hungry. 
                    The stored food point can time-out if a specified amount of time has elapsed without finding a new food point.
            \begin{description}
              \item[Input:] foodPosition, heading, hungerLevel, isFood
              \item[Output:] angle, eating
              \item[Instance variables:] hasFoundFood, ttlFoodd, eating\textunderscore v
              \item[States:] \hfill
              \begin{description}
                \item[NIL:] sleep(), state = CHECKFOUND
                \item[CHECKFOUND:] cond. dispatch: hasFoundFood ? state = TTLFOOD : CHECKFOOD
                \item[CHECKFOOD:] cond. dispatch: isFood ? state = SETFOOD : NIL
                \item[SETFOOD:] set hasFoundFood (true), state = TTLSET
                \item[TTLFOOD:] cond. dispatch: isFood ? state = TTLSET : TTLCHECK
                \item[TTLSET:] set ttlFood (currentTime()), state = TTLCHECK
                \item[TTLCHECK:] set hasFoundFood (subtract(currentTime(), ttlFood), state = CHECK1
                \item[CHECK1:] cond. dispatch: requiresFood(hungerLevel, eating) ? state = EAT1 : CHECK2
                \item[CHECK2:] cond. dispatch: eating ? state = EATEND : NIL
                \item[EAT1:] cond. dispatch: eating ? state = EAT3 : EAT2
                \item[EAT2:] output angle (returnToFood(foodPosition, heading), state = EAT3
                \item[EAT3:] cond. dispatch: isFood ? state = EAT4 : EATEND
                \item[EAT4:] set eating\textunderscore v (true), state = EAT5
                \item[EAT5:] output eating (true), state = NIL
                \item[EATEND:] set eating\textunderscore v (false), state = EATEND2
                \item[SLEEPEND2:] output eating (false), state = NIL
              \end{description}
              \item[Functions:] \hfill
              \begin{description}
                \item[sleep():] Pause module execution for time specified (suggested 300 ms).
                \item[currentTime():] Returns the current system time in milliseconds.
                \item[subtract(value1, value2):] Returns the result of subtracting value2 from value1.
                \item[requiresFood(hungerLevel, eating):] Returns true if eating and hungerLevel is greater than zero, or if hungerLevel is greater than a threshold (suggested 150), else returns false.
                \item[returnToFood(foodPosition, heading):] Adds 180 degrees to the foodPosition's angle and returns the result of that angle added to the heading.
              \end{description}
            \end{description}
        
\end{document}

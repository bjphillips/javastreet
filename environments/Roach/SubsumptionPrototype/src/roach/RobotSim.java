package roach;

import java.awt.Point;
import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.Semaphore;

public class RobotSim implements Robot {
  public static final int FRONT_RANGE = 10;
  public static final int FEELER_ANGLE = 50;
  public static final int ROTATE_DELAY = 100;
  public static final int FORWARD_DELAY = 100;
  public static final double FEELER_DISTANCE = 1;
  
  private final boolean showTrails;
  private final Map map;
  private final int rotateDelay;
  private final int forwardDelay;
  private double x;
  private double y;
  private final AtomicInteger heading;
  private final List<Point> trail;
  private final Semaphore trailMutex;
  private final Semaphore motorMutex;
  
  private final AtomicBoolean front;
  private final AtomicBoolean left;
  private final AtomicBoolean right;
  private final AtomicInteger distance;
  
  public RobotSim(Map map, boolean showTrails, int rotateDelay, int forwardDelay) {
    this.showTrails = showTrails;
    this.map = map;
    this.rotateDelay = rotateDelay;
    this.forwardDelay = forwardDelay;
    this.front = new AtomicBoolean();
    this.left = new AtomicBoolean();
    this.right = new AtomicBoolean();  
    this.motorMutex = new Semaphore(1, true);
    this.heading = new AtomicInteger(0);
    this.distance = new AtomicInteger();
    this.x = 15.0;
    this.y = 15.0;
    this.trail = new ArrayList<>();
    this.trailMutex = new Semaphore(1, true);
  }
  
  @Override
  public boolean getFront() {
    // Check three points to determine whether bumper is touched
    Coordinate tempPoint = getCoordinate();
    
    double[] tempX = new double[3];
    double[] tempY = new double[3];
    
    tempX[0] = Math.round(tempPoint.getX()) + Math.sin((double)(2 * Math.PI * heading.get() / 360));
    tempY[0] = Math.round(tempPoint.getY()) + Math.cos((double)(2 * Math.PI * heading.get() / 360));
    tempX[1] = Math.round(tempPoint.getX()) + Math.sin((double)(2 * Math.PI * addAngles(heading.get(),FRONT_RANGE) / 360));
    tempY[1] = Math.round(tempPoint.getY()) + Math.cos((double)(2 * Math.PI * addAngles(heading.get(),FRONT_RANGE) / 360));
    tempX[2] = Math.round(tempPoint.getX()) + Math.sin((double)(2 * Math.PI * addAngles(heading.get(),-FRONT_RANGE) / 360));
    tempY[2] = Math.round(tempPoint.getY()) + Math.cos((double)(2 * Math.PI * addAngles(heading.get(),-FRONT_RANGE) / 360));    

    int i;
    front.set(false);
    for (i = 0; i < 3; i++) {
      if (map.getWall(tempX[i], tempY[i]) == 1) {
        front.set(true);
        break;
      } 
    }
    return front.get();
  }
  
  @Override
  public boolean getLeft() {
    double tempX;
    double tempY;
    
    Coordinate tempPoint = getCoordinate();
    
    tempX = Math.round(tempPoint.getX()) + FEELER_DISTANCE * Math.sin((double)(2 * Math.PI * addAngles(heading.get(),FEELER_ANGLE) / 360));
    tempY = Math.round(tempPoint.getY()) + FEELER_DISTANCE * Math.cos((double)(2 * Math.PI * addAngles(heading.get(),FEELER_ANGLE) / 360)); 

    left.set(map.getWall(tempX, tempY) == 1);
    return left.get();
  }
  
  @Override
  public boolean getRight() {
    double tempX;
    double tempY;
    
    Coordinate tempPoint = getCoordinate();
        
    tempX = Math.round(tempPoint.getX()) + FEELER_DISTANCE * Math.sin((double)(2 * Math.PI * addAngles(heading.get(),-FEELER_ANGLE) / 360));
    tempY = Math.round(tempPoint.getY()) + FEELER_DISTANCE * Math.cos((double)(2 * Math.PI * addAngles(heading.get(),-FEELER_ANGLE) / 360)); 

    right.set(map.getWall(tempX, tempY) == 1);
    return right.get();
  }
  
  @Override
  public int rotate(int angle) {
    try {
      motorMutex.acquire();
      // Note: -angle as angles are reversed in simulation
      heading.set(addAngles(heading.get(), -angle));
      motorMutex.release();
      Thread.sleep(rotateDelay);
    }
    catch (Exception e) {}
    return heading.get();
  }
  
  @Override
  public int forwardIncrement() {
    Coordinate tempPoint = getCoordinate();
    double tempX, tempY;
    try {
      tempX = tempPoint.getX() + Math.sin((double)(2 * Math.PI * heading.get() / 360));
      tempY = tempPoint.getY() + Math.cos((double)(2 * Math.PI * heading.get() / 360));

      // Update values that cause the robot to not go into a wall (collision detection)
      motorMutex.acquire();
      if (map.getWall(tempX, tempY) == 0) {
        x = tempX;
        y = tempY;
        distance.addAndGet(1);
      }
      else if (map.getWall(tempX, y) == 0) {
        x = tempX;
        distance.addAndGet(1);
      }
      else if (map.getWall(x, tempY) == 0) {
        y = tempY;
        distance.addAndGet(1);
      }
      if (showTrails) {
        addTrailPoint((int)Math.round(x), (int)Math.round(y));
      }
      motorMutex.release();
      Thread.sleep(forwardDelay);
    }
    catch (Exception e) {}
    return distance.get();
  }
  
  // Method returns coordinate (is threadsafe)
  public Coordinate getCoordinate() {
    double tempX;
    double tempY;
    try {motorMutex.acquire();}
    catch (Exception e) {}
    tempX = x;
    tempY = y;
    motorMutex.release();
    return new Coordinate(tempX, tempY);
  }
  
  @Override
  public boolean isDark() {
    return map.isDark(getCoordinate());
  }
  
  @Override
  public boolean isLight() {
    return map.isLight(getCoordinate());
  }
  
  @Override
  public boolean isFood() {
    return map.isFood(getCoordinate());
  }
  
  @Override
  public int getHeading() {
    return heading.get();
  }
  
  @Override
  public int getDistance() {
    return distance.get();
  }
  
  private int addAngles(int angle1, int angle2) {
    return(((angle1 + angle2) % 360) + 360) % 360;
  }
  
  public boolean trailPointExists(int x, int y) {
    try {trailMutex.acquire();}
    catch (Exception e) {}
    for (Point p : trail) {
      if (p.getX() == x && p.getY() == y) {
        trailMutex.release();
        return true;
      }
    }
    trailMutex.release();
    return false;
  }
  
  private void addTrailPoint(int x, int y) {
    try {trailMutex.acquire();}
    catch (Exception e) {}
    boolean exists = false;
    for (Point p : trail) {
      if (p.getX() == x && p.getY() == y) {
        exists = true;
        break;
      }
    }
    if (!exists) {
      trail.add(new Point(x,y));
    }
    trailMutex.release();
  }
}
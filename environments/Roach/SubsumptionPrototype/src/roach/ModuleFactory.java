package roach;

import roach.modules.*;
import java.util.ArrayList;
import java.util.List;

public class ModuleFactory {
  IOController controller;
  Robot robot;
  private final List <roach.Module> modules;
 
  public ModuleFactory(IOController controller, Robot robot) {
    this.controller = controller;
    this.robot = robot;
    this.modules = new ArrayList<>();
  }

  public void makeModules(int delay, boolean debug, int tiredThreshold, int frightThreshold, int darkDelay, int darkTTL, int randAngleDist) {
	modules.add(new WallDetector(delay, controller, robot));
    modules.add(new WallHugger(delay, controller));
    modules.add(new Turn(delay, controller, robot));
    modules.add(new Forward(delay, controller, robot));
    modules.add(new RandomAngleEvent(delay, controller));
    modules.add(new IntegrateDark(delay, debug, controller));
    modules.add(new PathPlanDark(delay, debug, controller, tiredThreshold, frightThreshold, darkDelay, darkTTL, randAngleDist));
    modules.add(new Tiredness(delay, debug, controller, robot));
    modules.add(new Frightness(delay, debug, controller, robot));
    modules.add(new Hunger(delay, debug, controller, robot));
    modules.add(new IntegrateFood(delay, debug, controller));
    modules.add(new PathPlanFood(delay, debug, controller));
  }
  
  // Returns first module with matching name
  public Module getModule(String name) {
    for (Module m : modules) {
      if (m.getName().equals(name)) {
        return m;
      }
    }
    new Exception().printStackTrace();
    return null;
  }
  
  public void startModules() {
    for (Module m : modules) {
      m.begin();
    }
  }
}


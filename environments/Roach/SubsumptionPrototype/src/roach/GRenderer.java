package roach;

import java.util.List;
import java.util.ArrayList;
import java.util.concurrent.Semaphore;
import java.awt.*;
import javax.swing.*;

public class GRenderer implements Renderer, Runnable {
  public static final int DRAW_DELAY = 10;
  public static final int UNIT = 15;
  public static final int WIDTH = 800;
  public static final int HEIGHT = 600;
  
  private final JPanel panel;
  private final Font debugFont;
  private final boolean showTrails;
  private final boolean debug;
  private final Map map;
  private final List<Robot> robots;
  private final Semaphore robotsMutex;
  
  public GRenderer(Map map, boolean showTrails, boolean debug) {
    JFrame frame = new JFrame("Cockroach Simulator");
    
    this.showTrails = showTrails;
    this.debug = debug;
    this.map = map;
    this.robots = new ArrayList<>();
    this.robotsMutex = new Semaphore(1, true);
    this.debugFont = new Font ("Courier", Font.BOLD, 15); 
    
    frame.setSize(WIDTH,HEIGHT);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.panel = new JPanel()
    {
      @Override
      public void paintComponent(Graphics g) {
        super.paintComponent(g);
        render(g);
      }
    };
    frame.setContentPane(panel);
    frame.setVisible(true);
  }
  
  public void render (Graphics g) {
    // draw map
    int i, j;
    for (i = 0; i < map.getHorizontalLength(); i++) {
      for (j = 0; j < map.getVerticalLength(); j++) {
        if (map.getWall(j, i) == 1) {
          g.setColor(Color.black);
          g.fillRect(j * UNIT , i * UNIT , UNIT, UNIT);
        }
        else if (map.isFood(j, i)) {
          g.setColor(Color.green);
          g.fillRect(j * UNIT , i * UNIT , UNIT, UNIT);
        }
        else if (map.isDark(j, i)) {
          g.setColor(Color.darkGray);
          g.fillRect(j * UNIT , i * UNIT , UNIT, UNIT);
        }
        else if (!map.isLight(j, i)) {
          g.setColor(Color.gray);
          g.fillRect(j * UNIT , i * UNIT , UNIT, UNIT);
        }
      }
    }
    
    // draw robots
    try {robotsMutex.acquire();}
    catch (Exception e) {}
    for (Robot r : robots) {
      Coordinate coord = ((RobotSim)r).getCoordinate();
      g.setColor(Color.red);
      g.fillRect((int)(coord.getX() * UNIT) , (int)(coord.getY() * UNIT) , UNIT, UNIT);
    }
    robotsMutex.release();
    
    // print debug values of first robot
    if (debug) {
      g.setFont(debugFont);
      g.setColor(Color.green);
      Coordinate coord = ((RobotSim)robots.get(0)).getCoordinate();
      g.drawString(String.format("(%.1f, %.1f)", coord.getX(), coord.getY()) , 50, 20 );
      g.drawString("Heading: " + robots.get(0).getHeading() , 50, 40 );
      g.drawString("Distance: " + robots.get(0).getDistance() , 50, 60 );
      if (robots.get(0).getLeft())
        g.drawString("left" , 50, 80 );
      if (robots.get(0).getFront())
        g.drawString("front" , 90, 80 );
      if (robots.get(0).getRight())
        g.drawString("right" , 150, 80 );
      
    }
  }

  @Override
  public void run() {
    while (true) {
      panel.repaint();
      try {Thread.sleep(DRAW_DELAY);}
      catch (Exception e) {}
    }
  }
  
  @Override
  public void begin() {
    new Thread(this).start();
  }

  @Override
  public void addRobot(Robot robot) {
    try {robotsMutex.acquire();}
    catch (Exception e) {}
    robots.add(robot);
    robotsMutex.release();
  }
  
  private boolean trailPointExists(int x, int y) {
    boolean exists = false;
    for (Robot r : robots) {
      if (((RobotSim)r).trailPointExists(x, y)) {
        exists = true;
        break;
      }
    }
    return exists;
  }
}
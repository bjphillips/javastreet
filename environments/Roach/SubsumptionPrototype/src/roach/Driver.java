package roach;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.ArrayList;
import java.util.StringTokenizer;

public class Driver {

  public static void main(String[] args) throws Exception {
    
    // TODO: add the rest of the intended input/config arguments
    boolean ignoreConfig = false;
    boolean exit = false;
    boolean showTrails = false;
    boolean debug = false;
    boolean noFood = false;
    boolean noDark = false;
    boolean noLight = false;
    boolean ascii = false;
    boolean graphics = true;
    boolean allRenderers = false;
    boolean simulator = true;
    int numBots = 1;
    int delay = 0;
    int forwardDelay = 100;
    int rotateDelay = 100;
    
    int tiredThreshold = 100;
    int frightThreshold = 100;
    int darkDelay = 500;
    int darkTTL = 30000;
    int randAngleDist = 30;
    
    // check for ignoreconfig first
    try {
      for (String s : args) {
        if (s.equals("ignoreconfig")) {
          ignoreConfig = true;
        }
      }
    }
    catch (Exception e) {}
    
    // process config file
    if (!ignoreConfig) {
      try {
        FileInputStream fstream = new FileInputStream("config");
        BufferedReader reader = new BufferedReader(new InputStreamReader(fstream));
        String line;
        StringTokenizer st;
        while (true) {
          line = reader.readLine();
          if (line == null) {
            break;
          }
          else if (line.charAt(0) != '/') {
            st = new StringTokenizer(line);
            String first = st.nextToken();
            if (first.equals("num_bots"))
              numBots = Integer.parseInt(st.nextToken());
            else if (first.equals("state_delay"))
              delay = Integer.parseInt(st.nextToken());
            else if (first.equals("rotate_delay"))
              rotateDelay = Integer.parseInt(st.nextToken());
            else if (first.equals("forward_delay"))
              forwardDelay = Integer.parseInt(st.nextToken());
            
            
            else if (first.equals("tired_threshold"))
              tiredThreshold = Integer.parseInt(st.nextToken());
            else if (first.equals("fright_threshold"))
              frightThreshold = Integer.parseInt(st.nextToken());
            else if (first.equals("dark_ttl"))
              darkTTL = Integer.parseInt(st.nextToken());
            else if (first.equals("path_dark_delay"))
              darkDelay = Integer.parseInt(st.nextToken());
            
            
            else if (first.equals("rand_angle_dist"))
              randAngleDist = Integer.parseInt(st.nextToken());
            
            
            else if (first.equals("graphics"))
              graphics = (Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("ascii"))
              ascii = (Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("food"))
              noFood = !(Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("dark"))
              noDark = !(Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("light"))
              noLight = !(Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("trails"))
              showTrails = (Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("debug"))
              debug = (Integer.parseInt(st.nextToken()) == 1);
            else if (first.equals("simulator"))
              simulator = (Integer.parseInt(st.nextToken()) == 1);
            else 
              throw new Exception();
          } 
        }
      }
      catch (Exception e) {
        System.out.println("Failed to read config file!");
      }
    }
    
    // process input arguments
    int i = 0;
    try {
      for (String s : args) {
        if (s.equals("help")) {
          System.out.printf("<first number>: number of robots (def: 1)\n<second number>: delay between each state in state machine (in ms) (def: 0)\n<third number>: time taken when rotating (in ms) (def: 100)\n<fourth number>: time taken when moving forwards (in ms) (def: 100)\ntrails: enables trails\ndebug: enables debug print statements\nnorenderer: disables rendering of simulation\nnofood: no food spawned\nnodark: no dark areas formed\nnolight: no light areas formed\nascii: use ascii renderer instead of graphical renderer\nallrenderers: Use all renderers at the same time\nignoreconfig: disables reading from config file\nnosimulator: uses real robot interface\n");
          exit = true;
        }
        else if (s.equals("ignoreconfig")) {
          // do nothing - already handled
        }
        else if (s.equals("trails")) {
          System.out.println("Trails are active!");
          showTrails = true;
        }
        else if (s.equals("debug")) {
          System.out.println("Debugging is active!");
          debug = true;
        }
        else if (s.equals("norenderer")) {
          System.out.println("Render is inactive!");
          graphics = false;
          ascii = false;
        }
        else if (s.equals("nofood")) {
          System.out.println("Food not created on map!");
          noFood = true;
        }
        else if (s.equals("nodark")) {
          System.out.println("Dark area not created on map!");
          noDark = true;
        }
        else if (s.equals("nolight")) {
          System.out.println("Light area not created on map!");
          noLight = true;
        }
        else if (s.equals("ascii")) {
          System.out.println("Using ascii renderer");
          ascii = true;
          graphics = false;
        }
        else if (s.equals("allrenderers")) {
          System.out.println("Using all renderers!");
          ascii = true;
          graphics = true;
        }
        else if (s.equals("nosimulator")) {
          System.out.println("using real robot!");
          simulator = false;
        }
        else if (i == 0) {
          numBots = Integer.parseInt(s);
          System.out.println("Creating " + numBots + " robots!");
          i++;
        }
        else if (i == 1) {
          delay = Integer.parseInt(s);
          System.out.println("State machine delay is " + delay + " ms!");
          i++;
        }
        else if (i == 2) {
          rotateDelay = Integer.parseInt(s);
          System.out.println("Rotate speed set to " + rotateDelay + " ms!");
          i++;
        }
        else if (i == 3) {
          forwardDelay = Integer.parseInt(s);
          System.out.println("Forward speed set to " + forwardDelay + " ms!");
          i++;
        }
      }
    }
    catch (Exception e) {
      System.out.println("Unknown input! Type help for list of arguments");
      exit = true;
    }
    
    // begin execution of robot and simulator
    if (!exit) {
      Map map = new Map(noFood, noDark, noLight);
      List<Renderer> renderers = new ArrayList<Renderer>();
      if (ascii)
        renderers.add(new ASCIIRenderer(map, showTrails, debug));        
      if (graphics)
        renderers.add(new GRenderer(map, showTrails, debug));
      
      for (i = 0; i < numBots; i++) {
        IOController controller = new IOController();
        Robot robot;
        if (simulator)
          robot = new RobotSim(map, showTrails, rotateDelay, forwardDelay);
        else 
          //Robot robot = new RobotReal(); TODO: uncomment when implemented, remove line below
          robot = new RobotSim(map, showTrails, rotateDelay, forwardDelay);
          
        for (Renderer r : renderers)
          r.addRobot(robot);
        ModuleFactory factory = new ModuleFactory(controller, robot);
        
        factory.makeModules(delay, debug, tiredThreshold, frightThreshold, darkDelay, darkTTL, randAngleDist);
        controller.addModuleFactory(factory);
        controller.addControlWires();
        factory.startModules();
      }
      for (Renderer r : renderers)
        r.begin();
    }
  }
}
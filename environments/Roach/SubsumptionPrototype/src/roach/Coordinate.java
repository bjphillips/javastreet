package roach;

public class Coordinate {
  private double x;
  private double y;

  public Coordinate() {
    this.x = 0;
    this.y = 0;
  }
  
  public Coordinate(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  public void translate(double x, double y) {
    this.x += x;
    this.y += y;
  }
  
  public void set(double x, double y) {
    this.x = x;
    this.y = y;
  }
  
  public void set(int x, int y) {
    this.x = (double)x;
    this.y = (double)y;
  }
  
  public void setX(double x) {
    this.x = x;
  }
  
  public void setX(int x) {
    this.x = (double)x;
  }
  
  public void setY(double y) {
    this.y = y;
  }
  
  public void setY(int y) {
    this.y = (double)y;
  }
  
  
  public void set(Coordinate c) {
    this.x = c.getX();
    this.y = c.getY();
  }
  
  public double getX() {
    return x;
  }
  
  public double getY() {
    return y;
  }
  
  public Coordinate getCoordinate() {
    return new Coordinate(this.x, this.y);
  }
  
  public int getDistance() {
    return (int)Math.sqrt((x * x) + (y * y));
  }
  
  public int getAngle() {
    int angle = (int)Math.toDegrees(Math.atan2(x, y));
    if (angle < 0)
      angle += 360;
    return angle;
  }
  
  @Override
  public String toString() {
    return String.format("(%.1f, %.1f)", this.x, this.y);
  }
  
}
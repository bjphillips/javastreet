package roach;

import java.util.ArrayList;
import java.util.List;

public class IOController {
  ModuleFactory factory;
  List <Wire> wires;

  public IOController() {
    wires = new ArrayList<>();
    System.out.println("IOController initialised");
  }
  
  // Send outputs along appropriate wires
  public void onOutput(String name, String variable, int value) {
    boolean didOutput = false;
    for (Wire w : wires) {
      if (w.getOutName().equals(name) && w.getOutput().equals(variable)) {
        // if inhibitor, inhibit all wires with the same output as the input of the inhibiting wire
        if (w.getInhibitorStatus() == 1) {
          for (Wire wi : wires) {
            if (wi.getOutName().equals(w.getInName()) && wi.getOutput().equals(w.getInput())) {
              wi.inhibit(w.getInhibitorDuration());
            }
          }
        }
        // if suppressor, inhibit all non-suppressing wires with same input (don't suppress other suppressing wires)
        else if (w.getInhibitorStatus() == 2) {
          for (Wire wi : wires) {
            if (wi.getInName().equals(w.getInName()) && wi.getInput().equals(w.getInput())
                                                && wi.getInhibitorStatus() != 2) {
              wi.inhibit(w.getInhibitorDuration());
            }
          }          
        }
        // Don't output if wire inhibits as no data is sent
        if (w.getInhibitorStatus() != 1) {
          w.output(value);
        }
        didOutput = true;
      }
    }
    if (!didOutput) {
      new Exception().printStackTrace();
    }
  }
  
  public void addModuleFactory(ModuleFactory factory) {
    this.factory = factory;
  }
  
  public void addControlWires() {
    // Output module, output name, input module, input name, 
    //    inhibitor status(0:normal, 1:inhibitor, 2:suppressor), inhibition duration
    // If inhibitor, input module and name are actually output module and name to be inhibited
    // Level 0
    wires.add(new Wire(factory.getModule("WallDetector"), "wallStatus",
              factory.getModule("WallHugger"), "wallStatus", 0, 0));
    wires.add(new Wire(factory.getModule("WallHugger"), "angle", 
              factory.getModule("Turn"), "angle", 0, 0));
    // Level 1
    wires.add(new Wire(factory.getModule("Turn"), "busy", 
              factory.getModule("Forward"), "busy", 0, 0));
    // Level 2
    wires.add(new Wire(factory.getModule("WallDetector"), "wallStatus",
              factory.getModule("RandomAngleEvent"), "wallStatus", 0, 0));
    wires.add(new Wire(factory.getModule("RandomAngleEvent"), "angle", 
              factory.getModule("Turn"), "angle", 2, 700));
              
    // Level 3
    wires.add(new Wire(factory.getModule("Turn"), "heading", 
              factory.getModule("IntegrateDark"), "heading", 0, 0));
    wires.add(new Wire(factory.getModule("Forward"), "distance", 
              factory.getModule("IntegrateDark"), "distance", 0, 0));
    wires.add(new Wire(factory.getModule("IntegrateDark"), "positionX", 
              factory.getModule("PathPlanDark"), "darkPositionX", 0, 0));
    wires.add(new Wire(factory.getModule("IntegrateDark"), "positionY", 
              factory.getModule("PathPlanDark"), "darkPositionY", 0, 0));
    wires.add(new Wire(factory.getModule("PathPlanDark"), "angle", 
              factory.getModule("Turn"), "angle", 2, 700));
    wires.add(new Wire(factory.getModule("PathPlanDark"), "resting", 
              factory.getModule("Forward"), "busy", 2, 1500));
    wires.add(new Wire(factory.getModule("PathPlanDark"), "resting", 
              factory.getModule("RandomAngleEvent"), "angle", 1, 700));
    wires.add(new Wire(factory.getModule("PathPlanDark"), "resting", 
              factory.getModule("WallHugger"), "angle", 1, 700));
    wires.add(new Wire(factory.getModule("Turn"), "heading", 
              factory.getModule("PathPlanDark"), "heading", 0, 0));
    wires.add(new Wire(factory.getModule("PathPlanDark"), "resting2", 
              factory.getModule("Tiredness"), "isResting", 0, 0));
    wires.add(new Wire(factory.getModule("Tiredness"), "tiredLevel", 
              factory.getModule("PathPlanDark"), "tiredLevel", 0, 0));
    wires.add(new Wire(factory.getModule("Tiredness"), "isDark", 
              factory.getModule("PathPlanDark"), "isDark", 0, 0));
    wires.add(new Wire(factory.getModule("Tiredness"), "reset", 
              factory.getModule("IntegrateDark"), "reset", 0, 0));
    wires.add(new Wire(factory.getModule("Frightness"), "frightLevel", 
              factory.getModule("PathPlanDark"), "frightLevel", 0, 0));
              
    // Level 4
    wires.add(new Wire(factory.getModule("Turn"), "heading", 
              factory.getModule("IntegrateFood"), "heading", 0, 0));
    wires.add(new Wire(factory.getModule("Forward"), "distance", 
              factory.getModule("IntegrateFood"), "distance", 0, 0));
    wires.add(new Wire(factory.getModule("IntegrateFood"), "positionX", 
              factory.getModule("PathPlanFood"), "foodPositionX", 0, 0));
    wires.add(new Wire(factory.getModule("IntegrateFood"), "positionY", 
              factory.getModule("PathPlanFood"), "foodPositionY", 0, 0));
    wires.add(new Wire(factory.getModule("Hunger"), "reset", 
              factory.getModule("IntegrateFood"), "reset", 0, 0));
    wires.add(new Wire(factory.getModule("Hunger"), "hungerLevel", 
              factory.getModule("PathPlanFood"), "hungerLevel", 0, 0));
    wires.add(new Wire(factory.getModule("Hunger"), "isFood", 
              factory.getModule("PathPlanFood"), "isFood", 0, 0));
    wires.add(new Wire(factory.getModule("Turn"), "heading", 
              factory.getModule("PathPlanFood"), "heading", 0, 0));
    
    wires.add(new Wire(factory.getModule("PathPlanFood"), "angle", 
              factory.getModule("Turn"), "angle", 2, 700));
    wires.add(new Wire(factory.getModule("PathPlanFood"), "eating", 
              factory.getModule("Forward"), "busy", 2, 1500));
    wires.add(new Wire(factory.getModule("PathPlanFood"), "eating", 
              factory.getModule("RandomAngleEvent"), "angle", 1, 700));
    wires.add(new Wire(factory.getModule("PathPlanFood"), "eating", 
              factory.getModule("WallHugger"), "angle", 1, 700));
    
    wires.add(new Wire(factory.getModule("PathPlanFood"), "angle", 
              factory.getModule("PathPlanDark"), "angle", 1, 700));
    wires.add(new Wire(factory.getModule("PathPlanFood"), "eating", 
              factory.getModule("PathPlanDark"), "resting", 1, 1500));
    wires.add(new Wire(factory.getModule("PathPlanFood"), "eating", 
              factory.getModule("PathPlanDark"), "angle", 1, 1500));
  }
}


package roach;

import java.util.concurrent.atomic.AtomicBoolean;

public class Wire {
  
  // For being inhibited
  AtomicBoolean isInhibited;
  long inhibitStartTime;
  int inhibitDuration;
  
  // For inhibiting
  int inhibitorDuration;
  int inhibitorStatus;
  
  Module outModule;
  String output;
  Module inModule;
  String input;
  
  public Wire(Module outModule, String output, Module inModule, String input, 
                                  int inhibitorStatus, int inhibitorDuration) {
    this.outModule = outModule;
    this.output = output;
    this.inModule = inModule;
    this.input = input;
    this.isInhibited = new AtomicBoolean();
    this.inhibitorStatus = inhibitorStatus;
    this.inhibitorDuration = inhibitorDuration;
  }
  
  public void output(int value) {
    if (!isInhibited()) {
      inModule.input(input, value);
    }
  }
  
  // 0 = normal, 1 = inhibitor, 2 = suppressor
  public int getInhibitorStatus() {
    return inhibitorStatus;
  }
  
  // begin inhibiting this wire
  public void inhibit(int inhibitDuration) {
    this.inhibitDuration = inhibitDuration;
    inhibitStartTime = System.currentTimeMillis();
    isInhibited.set(true);
  }
  
  public String getOutName() {
    return outModule.getName();
  }
  
  public String getInName() {
    return inModule.getName();
  }
  
  public String getOutput() {
    return output;
  }
  
  public String getInput() {
    return input;
  }
  
  // returns time in milliseconds that this wire inhibits others for
  public int getInhibitorDuration() {
    return inhibitorDuration;
  }
  
  // Check to see whether this wire should no longer be inhibited
  private boolean isInhibited() {
    if (isInhibited.get()) {
      if ((System.currentTimeMillis() - inhibitStartTime) > (long)inhibitDuration) {
        isInhibited.set(false);
        return false;
      }
      return true;
    }
    return false;
  }
}

package roach.modules;

import roach.IOController;
import roach.Robot;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import roach.Module;

public class Tiredness implements Runnable, Module {
  private static final int TIRED_MAX = 255;
  private static final int DELAY = 200;
  private static final int RESTORE_RATE = (int)(20.0 * ((double)DELAY / 1000.0));
  private static final int CONSUME_RATE = (int)(5.0 * ((double)DELAY / 1000.0));
  
  private final int delay;
  private final boolean debug;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;
  
  private final AtomicBoolean isResting;
  private final AtomicBoolean isDark_v;
  private final AtomicInteger tiredLevel_v;
  private State state;

  public Tiredness(int delay, boolean debug, IOController controller, Robot robot) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.robot = robot;
    this.name = "Tiredness";
    this.isResting = new AtomicBoolean();
    this.isDark_v = new AtomicBoolean(); 
    this.tiredLevel_v = new AtomicInteger(); 
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() { 
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:     Thread.sleep(DELAY);
                        state = State.DARK;
                        break;
          case DARK:    isDark_v.set(isDark());
                        state = State.BRANCH;
                        break;
          case BRANCH:  state = isDark_v.get() ? State.OUT1 : State.OUT2;
                        break;
          case OUT1:    output("reset", 1);
                        state = State.OUT2;
                        break;
          case OUT2:    output("isDark", isDark_v.get() ? 1 : 0);
                        state = State.CHECK;
                        break;
          case CHECK:   state = isResting.get() ? State.RESTORE : State.CONSUME;
                        break;
          case RESTORE: tiredLevel_v.set(restore(tiredLevel_v.get()));
                        state = State.OUT3;
                        break;
          case CONSUME: tiredLevel_v.set(consume(tiredLevel_v.get()));
                        state = State.OUT3;
                        break;
          case OUT3:    output("tiredLevel", tiredLevel_v.get());
                        if (debug)
                          System.out.println("TiredLevel: " + tiredLevel_v.get());
                        state = State.NIL;
                        break;
          default:      new Exception().printStackTrace();
                        break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    DARK,
    BRANCH,
    OUT1,
    OUT2,
    CHECK,
    RESTORE,
    CONSUME,
    OUT3
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "isResting":   isResting.set((value == 1) ? true : false);
                          break;
      default:            new Exception().printStackTrace();
                          System.out.println("Input was: " + variable);
                          break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private boolean isDark() {
    return robot.isDark();
  }
  
  private int restore(int value) {
    value -= RESTORE_RATE;
    return (value > 0) ? value : 0;
  }
  
  private int consume(int value) {
    value += CONSUME_RATE;
    return (value < TIRED_MAX) ? value : 255;
  }
}
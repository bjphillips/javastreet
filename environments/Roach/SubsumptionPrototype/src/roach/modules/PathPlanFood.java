package roach.modules;

import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Random;
import roach.Coordinate;
import roach.Module;

public class PathPlanFood implements Runnable, Module {
  private static final int HUNGER_THRESHOLD = 100;
  private static final int DELAY = 300;
  private static final int TTL = 30000;
  private static final int RAND_ANGLE_DIST = 30;
  
  private final int delay;
  private final boolean debug;
  private final IOController controller;
  private final String name;
  private final Thread FSMThread;

  private final AtomicBoolean isFood;
  private final AtomicBoolean isEating;
  private final AtomicBoolean hasFoundFood;
  private final AtomicInteger hungerLevel;
  private AtomicInteger prevHeading;
  private final AtomicInteger foodPositionX;
  private final AtomicInteger foodPositionY;
  private final AtomicInteger heading;
  private final AtomicInteger ttlFood;
  
  private Random random;
  private Coordinate foodPosition;
  private State state;

  public PathPlanFood(int delay, boolean debug, IOController controller) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.name = "PathPlanFood";
    this.random = new Random();
    this.isFood = new AtomicBoolean();
    this.isEating = new AtomicBoolean();
    this.hasFoundFood = new AtomicBoolean();
    this.hungerLevel = new AtomicInteger();
    this.foodPositionX = new AtomicInteger();
    this.foodPositionY = new AtomicInteger();
    this.foodPosition = new Coordinate();
    this.heading = new AtomicInteger();
    this.ttlFood = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() { 
    while (true) {
      try {
        Thread.sleep(delay);
        // TODO: Handle incorrect food location better
        switch (state) {
          case NIL:     Thread.sleep(DELAY);
                        state = State.CHECKFOUND;
                        break;
          case CHECKFOUND: state = hasFoundFood.get() ? State.TTLFOOD : State.CHECKFOOD;
                        break;
          case CHECKFOOD:state = isFood.get() ? State.SETFOOD : State.NIL;
                        break;
          case SETFOOD: hasFoundFood.set(true);
                        state = State.SET_TTL;
                        break;
          case TTLFOOD: state = isFood.get() ? State.SET_TTL : State.CHECKTTL;
                        break;
          case SET_TTL: ttlFood.set((int)System.currentTimeMillis());
                        state = State.CHECKTTL;
                        break;
          case CHECKTTL:hasFoundFood.set(((int)System.currentTimeMillis() - ttlFood.get()) < TTL);
                        if (debug && !hasFoundFood.get())
                          System.out.println("Food point timed out!");
                        state = State.CHECK;
                        break;
          case CHECK:   state = requiresFood(hungerLevel.get(), isEating.get()) ? State.EAT1 : State.CHECK2;
                        break;
          case CHECK2:  state = isEating.get() ? State.EATEND : State.NIL;
                        break;
          case EAT1:    state = isEating.get() ? State.EAT3 : State.EAT2;
                        break;
          case EAT2:    output("angle", returnToFood(foodPosition.getAngle(), foodPosition.getDistance(), heading.get()));
                        state = State.EAT3;
                        break;
          case EAT3:    state = isFood.get() ? State.EAT4 : State.EATEND;
                        break;
          case EAT4:    isEating.set(true);
                        state = State.EAT5;
                        break;
          case EAT5:    output("eating", 1);
                        if (debug)
                          System.out.println("Robot is eating!");
                        state = State.NIL;
                        break;
          case EATEND:  isEating.set(false);
                        state = State.EATEND2;
                        break;
          case EATEND2: output("eating", 0);
                        state = State.NIL;
                        break;
          default:      new Exception().printStackTrace();
                        break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    CHECKFOUND,
    CHECK,
    CHECKFOOD,
    SETFOOD,
    TTLFOOD,
    SET_TTL,
    CHECKTTL,
    CHECK2,
    EAT1,
    EAT2,
    EAT3,
    EAT4,
    EAT5,
    EATEND,
    EATEND2
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "isFood":      isFood.set((value == 1) ? true : false);
                          break;
      case "hungerLevel": hungerLevel.set(value);
                          break;
      case "heading":     heading.set(value);
                          break;
      case "foodPositionX":   foodPositionX.set(value);
                              break;
      case "foodPositionY":   foodPositionY.set(value);
                              foodPosition.set(foodPositionX.get(), foodPositionY.get());
                              break;
      default:            new Exception().printStackTrace();
                          System.out.println("Input was: " + variable);
                          break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private boolean requiresFood(int hungerLevel, boolean isEating) {
    if (isEating && hungerLevel > 0)
      return true; 
    else     
      return hungerLevel > HUNGER_THRESHOLD;
  }
  
  private int returnToFood(int angle, int distance, int heading) {
    if (debug)
      System.out.printf("Return to food: Heading is %d, need to face %d, rotating by %d!\n", heading, addAngles(angle, 180), - addAngles(addAngles(angle, 180), -heading));
    //if (distance != 0)
    //  return addAngles(-(addAngles(addAngles(angle, 180), -heading)), (random.nextInt((int)(RAND_ANGLE_DIST / (double)distance) + 1) - (int)((RAND_ANGLE_DIST / (double)distance) / 2) )); 
    return - (addAngles(addAngles(angle, 180), -heading));
  }
  
  private int addAngles(int angle1, int angle2) {
    return(((angle1 + angle2) % 360) + 360) % 360;
  }
  
}
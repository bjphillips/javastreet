package roach.modules;
import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import roach.Module;
import roach.Robot;

public class Forward implements Runnable, Module {
  private static final int DELAY_LENGTH = 20;
  
  private final int delay;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;

  private final AtomicBoolean busy;
  private final AtomicInteger distance_v;
  private State state;

  public Forward(int delay, IOController controller, Robot robot) {
    this.delay = delay;
    this.controller = controller;
    this.robot = robot;
    this.name = "Forward";
    this.busy = new AtomicBoolean();
    this.distance_v = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   state = busy.get() ? State.NIL : State.DELAY;
                      break;
          // Requires delay so that it does not move forward between incremental rotations
          case DELAY: Thread.sleep(DELAY_LENGTH);
                      state = State.CHECK;
                      break;
          case CHECK: state = busy.get() ? State.NIL : State.START;
                      break;
          case START: distance_v.set(forwardIncrement());
                      state = State.OUT;
                      break;
          case OUT:   output("distance", distance_v.get());
                      state = State.CHECK;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    DELAY,
    CHECK,
    START,
    REC,
    OUT
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "busy":   busy.set(value == 1 ? true : false);
                     break;
      default:       new Exception().printStackTrace();
                     break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private int forwardIncrement() {
    return robot.forwardIncrement();
  }
}
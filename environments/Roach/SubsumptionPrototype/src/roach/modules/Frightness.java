package roach.modules;

import roach.IOController;
import roach.Robot;
import java.util.concurrent.atomic.AtomicInteger;
import roach.Module;

public class Frightness implements Runnable, Module {
  private static final int FRIGHT_MAX = 255;
  private static final int DELAY = 200;
  private static final int RESTORE_RATE = (int)(30.0 * ((double)DELAY / 1000.0));
  private static final int CONSUME_RATE = (int)(50.0 * ((double)DELAY / 1000.0));
  
  private final int delay;
  private final boolean debug;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;
  
  private final AtomicInteger frightLevel_v;
  private State state;

  public Frightness(int delay, boolean debug, IOController controller, Robot robot) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.robot = robot;
    this.name = "Frightness"; 
    this.frightLevel_v = new AtomicInteger(); 
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() { 
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:     Thread.sleep(DELAY);
                        state = State.CHKLIGHT;
                        break;
          case CHKLIGHT:state = isLight() ? State.CONSUME : State.RESTORE;
                        // dispatch to chkdark instead of restore if you only want to restore if in dark area
                        break;
          case CHKDARK: state = isDark() ? State.RESTORE : State.OUT;
                        break;
          case RESTORE: frightLevel_v.set(restore(frightLevel_v.get()));
                        state = State.OUT;
                        break;
          case CONSUME: frightLevel_v.set(consume(frightLevel_v.get()));
                        state = State.OUT;
                        break;
          case OUT:     output("frightLevel", frightLevel_v.get());
                        if (debug)
                          System.out.println("FrightLevel: " + frightLevel_v.get());
                        state = State.NIL;
                        break;
          default:      new Exception().printStackTrace();
                        break;
        }
      }
      catch (InterruptedException e) {
          e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    CHKDARK,
    CHKLIGHT,
    RESTORE,
    CONSUME,
    OUT
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      default:            new Exception().printStackTrace();
                          System.out.println("Input was: " + variable);
                          break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private boolean isDark() {
    return robot.isDark();
  }
  
  private boolean isLight() {
    return robot.isLight(); 
  }
  
  private int restore(int value) {
    value -= RESTORE_RATE;
    return (value > 0) ? value : 0;
  }
  
  private int consume(int value) {
    value += CONSUME_RATE;
    return (value < FRIGHT_MAX) ? value : 255;
  }
}
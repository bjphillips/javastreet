package roach.modules;

import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Random;
import roach.Module;

public class WallHugger implements Runnable, Module {
  private static final int ROTATE_DEG = 5;
  
  private final int delay;
  private final IOController controller;
  private final String name;
  private final Thread FSMThread;
  private final Random random;

  private final AtomicInteger wallStatus;
  private final AtomicInteger angle_v;
  private State state;
  
  private boolean isRandom = false;
  private boolean randomBoolean = false;

  public WallHugger(int delay, IOController controller) {
    this.delay = delay;
    this.controller = controller;
    this.name = "WallHugger";
    this.random = new Random();
    this.wallStatus = new AtomicInteger();
    this.angle_v = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   // event dispatch: wallStatus
                      break;
          case GET:   angle_v.set(getAngle());
                      state = State.CHECK;
                      break;
          case CHECK: state = (angle_v.get() != 0) ? (State.OUT) : (State.NIL);
                      break;
          case OUT:   output("angle", angle_v.get());
                      state = State.NIL;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    GET,
    CHECK,
    OUT
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "wallStatus": wallStatus.set(value);
                     if (state == State.NIL)
                       state = State.GET;
                     break;
      default:       new Exception().printStackTrace();
                     break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  // TODO: Make random rotation direction remembrance subsumption compliant
  private int getAngle() {
    int status = wallStatus.get();
    if (status == 0 || status == 1 || status == 5) {
      isRandom = false;
      return 0;
    }
    else if (status == 4) {
      isRandom = false;      
      return ROTATE_DEG;
    }
    else if (status == 2) {
      isRandom = false;
      return -ROTATE_DEG;
    }
    else if (status == 3) {
      if (!isRandom) {
        isRandom = true;
        randomBoolean = random.nextBoolean();
      }
      return randomBoolean ? ROTATE_DEG : -ROTATE_DEG;
    }
    new Exception().printStackTrace();
    System.out.println("Value: " + status);
    return 0;
  }
}
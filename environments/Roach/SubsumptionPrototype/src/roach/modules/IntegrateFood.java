package roach.modules;

import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import roach.Coordinate;
import roach.Module;

public class IntegrateFood implements Runnable, Module {
  private static final int DELAY = 0;
  
  private final int delay;
  private final boolean debug;
  private final IOController controller;
  private final String name;
  private final Thread FSMThread;

  private final AtomicInteger heading;
  private final AtomicInteger distance;
  private final AtomicInteger prevHeading;
  private final AtomicInteger prevDistance;
  private final AtomicInteger incrDistance;
  private final Coordinate position;
  private State state;

  // TODO: Test for any errors since robots sometimes return to spot that is way off
  public IntegrateFood(int delay, boolean debug, IOController controller) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.name = "IntegrateFood";
    this.heading = new AtomicInteger();
    this.distance = new AtomicInteger();
    this.prevHeading = new AtomicInteger();
    this.prevDistance = new AtomicInteger();
    this.incrDistance = new AtomicInteger();
    this.position = new Coordinate();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   try {Thread.sleep(DELAY);}
                      catch (Exception e) {}
                      state = State.EVENT;
                      break;
          case EVENT: // event dispatch: heading or distance != previous
                      break;
          case SET1:  incrDistance.set(distance.get() - prevDistance.get());
                      state = State.SET2;
                      break;
          case SET2:  position.set(pathIntegrate(heading.get(), incrDistance.get(), position.getCoordinate()));
                      state = State.SET3;
                      break;
          case SET3:  prevHeading.set(heading.get());
                      state = State.SET4;
                      break;
          case SET4:  prevDistance.set(distance.get());
                      state = State.OUT;
                      break;
                      // TODO: Determine whether outputting two values at the same time subsumption compliant
          case OUT:   output("positionX", (int)position.getX());
                      output("positionY", (int)position.getY());
                      state = State.NIL;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
          e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    EVENT,
    SET1,
    SET2,
    SET3,
    SET4,
    OUT
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "heading":  heading.set(value);
                       if (state == State.EVENT && heading.get() != prevHeading.get())
                         state = State.SET1;
                       break;
      case "distance": distance.set(value);
                       if (state == State.EVENT && distance.get() != prevDistance.get())
                         state = State.SET1;
                       break;
      case "reset":    reset();
                       if (debug)
                         System.out.println("Reset food point!");
                       break;
      default:         new Exception().printStackTrace();
                       break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void reset() {
    state = State.NIL;
    // TODO: Make subsumption compliant (does it require instance vars to be reset?)
    //prevHeading.set(0);
    //prevDistance.set(0);
    incrDistance.set(0);
    position.set(0, 0);
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private Coordinate pathIntegrate(int heading, int incrDistance, Coordinate pos) {
    pos.translate((double)incrDistance * Math.sin((double)(2 * Math.PI * heading / 360)),
                  (double)incrDistance * Math.cos((double)(2 * Math.PI * heading / 360)));
    return pos;
  }
}
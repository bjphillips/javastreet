package roach.modules;

import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.Random;
import roach.Coordinate;
import roach.Module;

public class PathPlanDark implements Runnable, Module {
  private final int delay;
  private final boolean debug;  
  private final int tiredThreshold;
  private final int frightThreshold;
  private final int darkDelay;
  private final int darkTTL;
  private final int randAngleDist;
  private final IOController controller;
  private final String name;
  private final Thread FSMThread;

  private final AtomicBoolean isDark;
  private final AtomicBoolean isResting;
  private final AtomicBoolean hasFoundDark;
  private final AtomicInteger tiredLevel;
  private final AtomicInteger frightLevel;
  private AtomicInteger prevHeading;
  private final AtomicInteger darkPositionX;
  private final AtomicInteger darkPositionY;
  private final AtomicInteger heading;
  private final AtomicInteger ttlDark;
  
  private final Random random;
  private final Coordinate darkPosition;
  private State state;

  public PathPlanDark(int delay, boolean debug, IOController controller, int tiredThreshold, int frightThreshold, int darkDelay, int darkTTL, int randAngleDist) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.tiredThreshold = tiredThreshold;
    this.frightThreshold = frightThreshold;
    this.darkDelay = darkDelay;
    this.darkTTL = darkTTL;
    this.randAngleDist = randAngleDist;
    this.name = "PathPlanDark";
    this.random = new Random();
    this.isDark = new AtomicBoolean();
    this.isResting = new AtomicBoolean();
    this.hasFoundDark = new AtomicBoolean();
    this.tiredLevel = new AtomicInteger();
    this.frightLevel = new AtomicInteger();
    this.darkPositionX = new AtomicInteger();
    this.darkPositionY = new AtomicInteger();
    this.darkPosition = new Coordinate();
    this.heading = new AtomicInteger();
    this.ttlDark = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() { 
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
                        // TODO: Include handling for when light shines in dark area (robot will go back to dark point but will be light)
                        //  (Forget when distance is < 1)?
                        // or the older the point is, the longer it takes to try and find it again (or vary angle greater as gets older?)
          case NIL:     Thread.sleep(darkDelay);
                        state = State.CHECKFOUND;
                        break;
          case CHECKFOUND:state = hasFoundDark.get() ? State.TTLDARK : State.CHECKDRK;
                        break;
          case CHECKDRK:state = isDark.get() ? State.SETDARK : State.NIL;
                        break;
          case SETDARK: hasFoundDark.set(true);
                        state = State.SET_TTL;
                        break;
          case TTLDARK: state = isDark.get() ? State.SET_TTL : State.CHECKTTL;
                        break;
          case SET_TTL: ttlDark.set((int)System.currentTimeMillis());
                        state = State.CHECKTTL;
                        break;
          case CHECKTTL:hasFoundDark.set(((int)System.currentTimeMillis() - ttlDark.get()) < darkTTL);
                        if (debug && !hasFoundDark.get())
                          System.out.println("Dark point timed out!");
                        state = State.CHECK;
                        break;
          case CHECK:   state = requiresSleep(tiredLevel.get(), isResting.get()) ? State.SLEEP1 : State.CHECK2;
                        break;
          case CHECK2:  state = isResting.get() ? State.SLEEPEND : State.FRIGHT1;
                        break;
          case SLEEP1:  state = isResting.get() ? State.SLEEP3 : State.SLEEP2;
                        break;
          case SLEEP2:  output("angle", returnToDark(darkPosition.getAngle(), darkPosition.getDistance(), heading.get()));
                        state = State.SLEEP3;
                        break;
          case SLEEP3:  state = isDark.get() ? State.SLEEP4 : State.SLEEPEND;
                        break;
          case SLEEP4:  isResting.set(true);
                        state = State.SLEEP5;
                        break;
                        // outputting separate resting to be sent to tiredness so that it isn't inhibited
          case SLEEP5:  output("resting", 1);
                        output("resting2", 1);
                        if (debug)
                          System.out.println("Robot is sleeping!");
                        state = State.NIL;
                        break;
          case SLEEPEND:isResting.set(false);
                        output("resting", 0);
                        output("resting2", 0);
                        state = State.SLEEPEND2;
                        break;
          case SLEEPEND2: state = requiresSleep(tiredLevel.get(), isResting.get()) ? State.NIL : State.FRIGHT1;
                        break;
          case FRIGHT1: state = requiresHide(frightLevel.get()) ? State.FRIGHT2 : State.NIL;
                        break;
          case FRIGHT2: output("angle", returnToDark(darkPosition.getAngle(), darkPosition.getDistance(), heading.get()));
                        if (debug)
                          System.out.println("Robot is hiding!");
                        state = State.NIL;
                        break;
          default:      new Exception().printStackTrace();
                        break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    CHECKFOUND,
    CHECK,
    CHECKDRK,
    SETDARK,
    TTLDARK,
    SET_TTL,
    CHECKTTL,
    CHECK2,
    SLEEP1,
    SLEEP2,
    SLEEP3,
    SLEEP4,
    SLEEP5,
    SLEEPEND,
    SLEEPEND2,
    FRIGHT1,
    FRIGHT2
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "isDark":      isDark.set((value == 1) ? true : false);
                          break;
      case "tiredLevel":  tiredLevel.set(value);
                          break;
      case "frightLevel": frightLevel.set(value);
                          break;
      case "heading":     heading.set(value);
                          break;
      case "darkPositionX":   darkPositionX.set(value);
                              break;
      case "darkPositionY":   darkPositionY.set(value);
                              darkPosition.set(darkPositionX.get(), darkPositionY.get());
                              break;
      default:            new Exception().printStackTrace();
                          System.out.println("Input was: " + variable);
                          break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private boolean requiresSleep(int tiredLevel, boolean resting) {
    if (resting && tiredLevel > 0)
      return true; 
    else 
      return tiredLevel > tiredThreshold;
  }
  
  private boolean requiresHide(int frightLevel) {
    return frightLevel > frightThreshold;
  }
  
  private int returnToDark(int angle, int distance, int heading) {
    // go in opposite direction to direction vector is currently pointing in
    if (debug)
      System.out.printf("Return to dark: Heading is %d, need to face %d, rotating by %d! Distance is %d!\n", heading, 
                        addAngles(angle, 180), - addAngles(addAngles(angle, 180), -heading), distance);
    // avoid dividing by zero
    //if (distance != 0)
    //  return addAngles(-(addAngles(addAngles(angle, 180), -heading)), (random.nextInt((int)(randAngleDist / (double)distance) + 1) - 
    //                   (int)((randAngleDist / (double)distance) / 2) ));
    return - (addAngles(addAngles(angle, 180), -heading));
      
  }
  
  private int addAngles(int angle1, int angle2) {
    return(((angle1 + angle2) % 360) + 360) % 360;
  }
  
}
package roach.modules;

import roach.IOController;
import roach.Robot;
import java.util.concurrent.atomic.AtomicInteger;
import roach.Module;

public class Turn implements Runnable, Module {
  private final int delay;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;

  private final AtomicInteger angle;
  private final AtomicInteger heading_v;
  private State state;

  public Turn(int delay, IOController controller, Robot robot) {
    this.delay = delay;
    this.controller = controller;
    this.robot = robot;
    this.name = "Turn";
    this.angle = new AtomicInteger();
    this.heading_v = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   // event dispatch: angle
          
                      // ensures that forward knows that it can move after turn is no longer inhibited
                      // TODO: make subsumptiom compliant (not sure what to do about this)
                      output("busy", 0);
                      break;
          case BUSY:  output("busy", 1);
                      state = State.START;
                      break;
          case START: heading_v.set(rotate(angle.get()));
                      state = State.OUT;
                      break;
          case OUT:   output("heading", heading_v.get());
                      state = State.FIN;
                      break;
          case FIN:   output("busy", 0);
                      state = State.NIL;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    BUSY,
    START,
    OUT,
    FIN
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "angle":  angle.set(value);
                     if (state == State.NIL)
                       state = State.BUSY;
                     break;
      default:       new Exception().printStackTrace();
                     break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private int rotate(int angle) {
    return robot.rotate(angle);
  }
}
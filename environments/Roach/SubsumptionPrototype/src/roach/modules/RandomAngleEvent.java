package roach.modules;

import roach.IOController;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.Random;
import roach.Module;

public class RandomAngleEvent implements Runnable, Module {
  private static final int MIN_ANGLE = 10;
  private static final int MAX_ANGLE = 50;
  private static final int SLEEP_TIME = 500;
  private static final float RAND_CHANCE = 0.2f;
  
  private final int delay;
  private final IOController controller;
  private final String name;
  private final Thread FSMThread;
  private State state;
  
  private final AtomicInteger wallStatus;
  private final AtomicInteger huggingStatus;
  private final Random random;

  public RandomAngleEvent(int delay, IOController controller) {
    this.delay = delay;
    this.controller = controller;
    this.name = "RandomAngleEvent";
    this.random = new Random();
    this.wallStatus = new AtomicInteger();
    this.huggingStatus = new AtomicInteger();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   Thread.sleep(SLEEP_TIME);
                      state = State.GET;
                      break;
					  // saved separately so that it cannot change after being checked
          case GET:   huggingStatus.set(isHugging(wallStatus.get()));
                      state = State.CHECK;
                      break;
          case CHECK: state = (huggingStatus.get() != 0) ? State.START : State.NIL;
                      break;
          case START: state = randomBoolean() ? State.OUT : State.NIL;
                      break;          
          case OUT:   output("angle", randomAngle(huggingStatus.get()));
                      state = State.NIL;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    GET,
    CHECK,
    START,
    OUT,
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      case "wallStatus":  wallStatus.set(value);
                     break;
      default:       new Exception().printStackTrace();
                     break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  // returns 0 if not hugging, 1 if hugging left wall, 2 if hugging right wall
  private int isHugging(int wallStatus) {
    if (wallStatus == 5)
      return 1;
    else if (wallStatus == 1)
      return 2;
    else
      return 0;
  }
  
  private boolean randomBoolean() {
    return (random.nextInt((int)(1 / RAND_CHANCE)) == 0);
  }
  
  // Returns random angle based on which side of the wall the robot is on
  private int randomAngle(int wallStatus) {
    int rand = random.nextInt((MAX_ANGLE - MIN_ANGLE) + 1) + MIN_ANGLE;
    int angle = ((huggingStatus.get() == 1) ? 1 : -1) * rand;
    return angle;
  }
}
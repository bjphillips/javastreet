package roach.modules;

import roach.IOController;
import roach.Robot;
import java.util.concurrent.atomic.AtomicInteger;
import roach.Module;

public class Hunger implements Runnable, Module {
  private static final int HUNGER_MAX = 255;
  private static final int DELAY = 200;
  private static final int RESTORE_RATE = (int)(20.0 * ((double)DELAY / 1000.0));
  private static final int CONSUME_RATE = (int)(10.0 * ((double)DELAY / 1000.0));
  
  private final int delay;
  private final boolean debug;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;
  
  private final AtomicInteger hungerLevel_v;
  private State state;

  public Hunger(int delay, boolean debug, IOController controller, Robot robot) {
    this.delay = delay;
    this.debug = debug;
    this.controller = controller;
    this.robot = robot;
    this.name = "Hunger";
    this.hungerLevel_v = new AtomicInteger(); 
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:     Thread.sleep(DELAY);
                        state = State.CHECK;
                        break;
          case CHECK:   state = isFood() ? State.RESET : State.CONSUME;
                        break;
          case RESET:   output("reset", 1);
                        state = State.OUT1;
                        break;
          case OUT1:    output("isFood", 1);
                        state = State.RESTORE;
                        break;
          case RESTORE: hungerLevel_v.set(restore(hungerLevel_v.get()));
                        state = State.OUT3;
                        break;
          case CONSUME: hungerLevel_v.set(consume(hungerLevel_v.get()));
                        state = State.OUT2;
                        break;
          case OUT2:    output("isFood", 0);
                        state = State.OUT3;
                        break;
          case OUT3:    output("hungerLevel", hungerLevel_v.get());
                        if (debug)
                          System.out.println("hungerLevel: " + hungerLevel_v.get());
                        state = State.NIL;
                        break;    
          default:      new Exception().printStackTrace();
                        break;
        } 
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    CHECK,
    RESET,
    OUT1,
    RESTORE,
    CONSUME,
    OUT2,
    OUT3
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    switch (variable) {
      default:            new Exception().printStackTrace();
                          System.out.println("Input was: " + variable);
                          break;
    }
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private boolean isFood() {
    return robot.isFood();
  }
  
  private int restore(int value) {
    value -= RESTORE_RATE;
    return (value > 0) ? value : 0;
  }
  
  private int consume(int value) {
    value += CONSUME_RATE;
    return (value < HUNGER_MAX) ? value : 255;
  }
}
package roach.modules;

import roach.IOController;
import roach.Robot;
import java.util.concurrent.atomic.AtomicBoolean;

public class WallDetector implements Runnable, roach.Module {
  
  private final int delay;
  private final IOController controller;
  private final Robot robot;
  private final String name;
  private final Thread FSMThread;

  private final AtomicBoolean bumper;
  private final AtomicBoolean left;
  private final AtomicBoolean right;
  private State state;

  public WallDetector(int delay, IOController controller, Robot robot) {
    this.delay = delay;
    this.controller = controller;
    this.robot = robot;
    this.name = "WallDetector";
    this.bumper = new AtomicBoolean();
    this.left = new AtomicBoolean();
    this.right = new AtomicBoolean();
    this.state = State.NIL;
    this.FSMThread = new Thread(this);
    System.out.println(this.name + " module initialised");
  }
  
  @Override
  // FSM
  public void run() {
    while (true) {
      try {
        Thread.sleep(delay);
        switch (state) {
          case NIL:   bumper.set(robot.getFront());
                      state = State.GETL;
                      break;
          case GETL:  left.set(robot.getLeft());
                      state = State.GETR;
                      break;
          case GETR:  right.set(robot.getRight());
                      state = State.OUT;
                      break;
          case OUT:   output("wallStatus", getWallStatus());
                      state = State.NIL;
                      break;
          default:    new Exception().printStackTrace();
                      break;
        }
      }
      catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
  
  enum State {
    NIL,
    GETL,
    GETR,
    OUT
  }
  
  // Includes logic for event dispatch
  @Override
  public void input(String variable, int value) {
    new Exception().printStackTrace();
  }
 
  @Override
  public String getName() {
    return name;
  }
  
  @Override
  public void begin() {
    FSMThread.start();
  }
  
  private void output(String variable, int value) {
    controller.onOutput(this.name, variable, value);
  }
  
  private int getWallStatus() {
    if (!left.get() && !bumper.get() && !right.get())
      return 0;
    else if (!left.get() && !bumper.get() && right.get())
      return 1;
    else if (!left.get() && bumper.get() && right.get())
      return 2;
    else if (left.get() && bumper.get() && !right.get())
      return 4;
    else if (left.get() && !bumper.get() && !right.get())
      return 5;
    else
      return 3;
  }
}
package roach;

public interface Module {
  public void run();
  public void input(String variable, int value);
  public String getName();
  public void begin();
}

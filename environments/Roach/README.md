This environment simulates a simple "cockroach" robot. The bulk of the work was
completed by James Donovan over the summer of 2015/16.

The cockroach agent was developed using a subsumption architecture. James first
made a Java prototype which includes Java classes for the subsumption modules.
This is in the SUbsumptionPrototype folder. It would be a useful framework for
the development of other subsumption agents.

The subsumption modules were then realised as Street rules. The version in this
folder runs the Street agent under the JavaStreet simulator.

The environment provides a simple graphical depiction of the cockroach (or
cockroaches) in a walled enclosure with light and dark areas and food sources.

# Cockroach Behaviour

Broadly, the cockroach agents:
* get hungry and look for food
* get sleepy and go to dark places to rest
* are afraid of bright light and retreat to the last dark place they discovered
* prefer to follow walls than explore open spaces

Simulation parameters can be passed at the command line or in the `config`
file. An example `config` file:

    num_bots 1
    state_delay 0
    rotate_delay 100
    forward_delay 130
    graphics 1
    ascii 0
    food 1
    dark 1
    light 1
    trails 0
    debug 0
    simulator 1
    tired_threshold 100
    fright_threshold 100
    dark_ttl 30000
    path_dark_delay 300
    rand_angle_dist 30000

# Building And Running

This is a Netbeans project. Sorry. Maybe it is not too bad because it does mean
you can build, run and clean the project from this folder with commands like:

    ant compile
    ant run
    ant clean

Simulation options can be set in the `config` file.

# Documentation

`doc/SubsumptionArchitecturePrimer.pdf` contains a primer to subsumption
architectures in general.

`doc/RoachModules.pdf` contains detailed documentation of the subsumption
modules used in the cockroach agent.

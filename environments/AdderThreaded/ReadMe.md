Threaded Adder Example Environment
==================================

This example show how to create a simple environment that instantiates an agent
and runs the agent in its own thread. Inputs and outputs are passed between the
agent and the environment.

The environment prompts the user for two numbers. These are passed as inputs to
the agent, which returns their sum as an output.

Building and Running
--------------------

The project can be built using GNU make e.g.

    cd JavaStreet/environments/AdderThreaded
    make

This will create `AdderThreaded.class`. It assumes there is a `JavaStreet.jar`
file in `../build/libs`. If this is not present, it will need to be built first.

To run the project e.g.

    cd JavaStreet/environments/AdderThreaded
    java -classpath "../../build/libs/*":. AdderThreaded

The `AdderThreaded.bat` file may work on your OS.

I/O Protocol
------------

The environment passes the adder operands as `(_INPUT, op1, <op1>)` and
`(_INPUT, op2, <op2>)` WMEs. It reads a `(_OUTPUT, sum, <sum>)`.
Once the environment finds an output it deletes the WMEs `(_INPUT, op1, <op1>)`
and `(_INPUT, op2, <op2>)` before sending new ones.



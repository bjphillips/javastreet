/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
import java.util.*;
import javastreet.*;

/**
 * This simple example peripheral just receives the output
 * (_OUTPUT, sum, <sum>, ADD)
 * from the agent and makes it available to the environment.
 */
public class OutputHandler extends Peripheral {

	private int sum;
	private boolean sumReady;

	public OutputHandler(ThreadedAgent a) {
	 	super(a);
	 	sum = 0;
	 	sumReady = false;
	}

	public synchronized void handleOutput(TokenPacket tokenPacket) {
		for (Token token : tokenPacket) {
			// if this is (_OUTPUT, sum, <sum>, ADD)...
			if (token.getFlag().equals(TokenFlag.ADD)
				&& token.getAttribute(1).equals("sum")) {
				try {
					sum = Integer.parseInt(token.getAttribute(2));
				}
				catch (NumberFormatException ex) {
				}
				sumReady = true;
			}
		}
	}

	/* BEWARE - in you need to be careful with these access functions.
	 * The handleOutput method is called from the ThreadedAgent's thread. The
	 * Access functions are called from the environment's thread.
	 * In this case synchronised methods have been used to make everything
	 * thread safe.
	 */

	public synchronized boolean ready() {
		return sumReady;
	}

	public synchronized void reset() {
		sumReady = false;
	}

	public synchronized int sum() {
		return sum;
	}
}
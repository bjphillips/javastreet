/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
import javastreet.*;
import java.io.Console;
import java.util.*;

/**
 * An example user defined environment for a Street agent that is run,
 * asynchronously, in its own thread.
 * 1) the rules for the agent are loaded from AdderThreaded.street.
 * 2) two user input integers are passed as inputs to the agent.
 * 3) the agent generates the sum as an output
 * 4) the input WMEs are deleted
 * 5) goto step 1
 */
public class AdderThreaded {

	static ThreadedAgent agent;

	public static void main(String[] args) throws Exception {

		// get the console for user input
        Console console = System.console();
        if (console == null) {
        	System.out.println("No console. Giving up.");
        	return;
        }

        // create an agent and plug in the peripheral to handle the outputs
        agent = new ThreadedAgent();
        OutputHandler outputHandler = new OutputHandler(agent);
        
        // load the production rules
        if (!agent.loadFile("AdderThreaded.street")) {
            System.out.println("Can't load `AdderThreaded.street`. Giving up.");
            return;
        }

        // start the agent running
        agent.start();

		while (true) {

			// get the first integer to add
			String inputLine = console.readLine("Enter the first integer to add or just press enter to end > ");
			int op1;
			try {
				op1 = Integer.parseInt(inputLine);
			}
			catch (NumberFormatException ex) {
				System.out.println("Bye.");
				agent.terminate();
				return;
			}

			// pass the input to the agent
			String[] a1 = {"_INPUT", "op1", Integer.toString(op1)};
			agent.pushInput(new TokenPacket(a1, true));

			// get the second integer to add
			inputLine = console.readLine("Enter the second integer to add or just press enter to end > ");
			int op2;
			try {
				op2 = Integer.parseInt(inputLine);
			}
			catch (NumberFormatException ex) {
				System.out.println("Bye.");
				agent.terminate();
				return;
			}

			// pass the second input to the agent
			String[] a2 = {"_INPUT", "op2", Integer.toString(op2)};
			agent.pushInput(new TokenPacket(a2, true));

			// wait for the sum from the agent
			while (!outputHandler.ready());

			// print the result
			System.out.println("The agent says the sum is: "+outputHandler.sum());
			outputHandler.reset();

			// clear the old inputs
			agent.pushInput(new TokenPacket(a1, false));
			agent.pushInput(new TokenPacket(a2, false));
		}
	}
}


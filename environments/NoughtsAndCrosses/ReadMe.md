This project started life as a JavaFX FXML project in Netbeans. Once it was
working it was migrated to a gradle build system so that it is IDE independent.

*Note* that the gradle javafx-plugin it is using requires a recent version of
the Java development kit. It is working with build 1.8.0_31-b13.

Migration to Gradle
-------------------

1) downloaded javafx.plugin [from](https://bitbucket.org/shemnon/javafx-gradle/src/4695a6d1ed9e/samples/Ensemble8/?at=master)
2) moved source files to gradle's conventional folders
    - java files to src/main/java/<package>/
    - fxml files to src/main/resources/<package>/
3) ran `gradle gradlew` and checked in to revision control:
    - gradlew
    - gradlew.bat
    - gradle/wrapper/
    - gradle-wrapper.jar
    - gradle-wrapper.properties
4) deleted Netbeans artefacts:
    - build.xml
    - nbproject
    - manifest.mf
5) moved `NoughtsAndCrosses.street` into the `resources/street` folder and used
   `getClass().getClassLoader().getResourceAsStream("street/NoughtsAndCrosses.street")` 
   to load it. This is required so that`NoughtsAndCrosses.street` is distributed
   as part of the final distribution.


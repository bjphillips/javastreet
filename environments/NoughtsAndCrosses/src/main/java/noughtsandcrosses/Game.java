/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package noughtsandcrosses;

import java.util.HashMap;
import javafx.application.Platform;
import javastreet.*;
import java.io.InputStream;


/**
 * A game occurs between a 'player' and an 'agent'. The player is NOUGHTS
 * and always goes first; however the player can choose to skip their turn.
 * 
 * When the environment updates the agent on the state of the game:
 * 1) for every square that has changed state (or not been initialised) it sends 
 *    (_INPUT, OandX, <row>, <column>, <state>, ADD)
 * 2) for each the agent should respond with (_OUTPUT, OandX, <row>, <column>, <state>, ADD)
 * 3) the environment will then send (_INPUT, OandX, <row>, <column>, <state>, DELETE)
 * 4) the environment won't move to the next state of the game until the agent 
 *    has acknowledged all of the updates
 *
 * When the game starts:
 * 1) the environment updates the agent on the state of the game
 *
 * When the player makes a move (or skips their turn):
 * 1) the environment updates the agent on the new state of the game
 * 2) once the agent has acknowledged the changes, the environment will send
 *    (_INPUT, OandX, yourMove, ADD) will be sent
 * 
 * When the agent makes its move:
 * 1) it sends (_OUTPUT, OandX, <row>, <column>, ADD)
 * 2) the environment acknowledges with (_INPUT, OandX, yourMove, DELETE)
 * 3) the environment updates the agent on the new state of the game
 */
public class Game {
    
    private enum SquareState {
        BLANK, NOUGHT, CROSS 
    }  
    private final SquareState[][] initialGameState = {{SquareState.BLANK, SquareState.BLANK, SquareState.BLANK}, 
        {SquareState.BLANK, SquareState.BLANK, SquareState.BLANK}, 
        {SquareState.BLANK, SquareState.BLANK, SquareState.BLANK}};
    private final SquareState[][] nullGameState = {{null, null, null}, 
        {null, null, null}, {null, null, null}};
    private MainSceneController myMainSceneController;
    private SquareState[][] gameState;
    private SquareState[][] oldGameState;
    private HashMap<String,String> updatesSent;
    private ThreadedAgent agent;
    private GUIDebugger debugger;
    private enum TurnState {
        WAITING_PLAYER,     // waiting for the player to choose a square
        WAITING_ACK_PLAYER, // waiting for the agent to ack the board changes after player's move
        WAITING_AGENT,      // waiting for the agent to choose a square
        WAITING_ACK_AGENT   // waiting for the agent to ack the board changes after its own move
    }
    private TurnState turnState;
	private final String[] yourMoveAttributes = {"_INPUT", "OandX", "yourMove"};
    
    Game() {
        gameState = new SquareState[3][3];
        oldGameState = new SquareState[3][3];
        copyStateArray(initialGameState, gameState);
        copyStateArray(nullGameState, oldGameState);
        updatesSent = new HashMap<>();
        turnState = TurnState.WAITING_ACK_AGENT;
        
        // create the agent and load the rules
        agent = new ThreadedAgent();
        InputStream inputStream = getClass().getClassLoader().getResourceAsStream("street/NoughtsAndCrosses.street");
        if (inputStream==null) {
            System.err.println("Can't load `NoughtsAndCrosses.street`. Giving up.");
            System.exit(1);
        }
        if (!agent.loadInputStream(inputStream)) {
            System.err.println("Errors loading `NoughtsAndCrosses.street`. Giving up.");
            System.exit(1);
        }

        // attach our noughts and crosses peripheral
        new OutputListener(agent); 
        
        // connect a windowed debugger to the agent
        debugger = new GUIDebugger(agent);
		
		// Create an exitHandler to tidy up when the debugger exits
		Runnable exitHandler = new Runnable() {
			@Override
			public void run() {
				gameOver();
			}
		};
		debugger.setExitHandler(exitHandler);

        String[] args = {};
        try {
            debugger.runController(args);
        }
        catch (Exception ex) {
            System.err.println("Error launching windowed debuger.");
            System.exit(1);
        }

		String[] attributes = {"_INPUT", "system", "reset"};
        agent.pushInput(new TokenPacket(attributes, true));
        updateAgentGameState(); // send the initial game state to the agent
		agent.resume(); // start the agent running continuously
    }
    
    void setMainSceneController(MainSceneController mainSceneController) {
        myMainSceneController = mainSceneController;
    }
    
	void gameOver() {
		System.out.println("Game over.");
        agent.terminate();
		System.exit(0);
    }

    void playerMove(int row, int column) {
        if (!turnState.equals(TurnState.WAITING_PLAYER)) {
            return;
        }
        if (gameState[row][column]==SquareState.BLANK) {
            myMainSceneController.showNought(row, column);
            gameState[row][column]=SquareState.NOUGHT;
            updateAgentGameState();
            turnState = TurnState.WAITING_ACK_PLAYER;
            myMainSceneController.setSkipEnable(false);
        }
    }
    
    void restart() {
        myMainSceneController.clearAll();
        agent.pushInput(new TokenPacket(yourMoveAttributes, false));
        copyStateArray(initialGameState, gameState);
        updateAgentGameState();
        turnState = TurnState.WAITING_ACK_AGENT;
        myMainSceneController.setSkipEnable(true);
    }
    
    void playerSkip() {
        if (!turnState.equals(TurnState.WAITING_PLAYER)) {
            return;
        }
        agent.pushInput(new TokenPacket(yourMoveAttributes, true));
        turnState = TurnState.WAITING_AGENT;
        myMainSceneController.setSkipEnable(false);
    }
    
    private void updateAgentGameState() {
        for (int row=0; row<gameState[0].length; row++) {
            for (int column=0; column<gameState[1].length; column++) {
                if (!gameState[row][column].equals(oldGameState[row][column])) {
					String[] attributes = {"_INPUT", "OandX", Integer.toString(row), Integer.toString(column), gameState[row][column].name()};
                    agent.pushInput(new TokenPacket(attributes, true));
                    oldGameState[row][column] = gameState[row][column];
                    updatesSent.put("OandX"+row+column, gameState[row][column].name());
                }
            }
        }
    }
    
    // This is ugly and annoying but clone() does not work as expected for arrays
    private void copyStateArray(SquareState[][] source, SquareState[][] destination) {
        for (int i=0; i<3; i++) {
            System.arraycopy(source[i], 0, destination[i], 0, 3);
        }
    }
    
    private class OutputListener extends Peripheral {
        
        public OutputListener(ThreadedAgent agent) {
            super(agent);
        }
        
        @Override
        public void handleOutput(TokenPacket tokenPacket) {
			
			for (Token token: tokenPacket) {
				int row;
				int column;
				
				// is this (_OUTPUT, OandX, <row>, <column>...)?
				if (token.numberOfAttributes() < 4
						|| !token.getAttribute(1).equals("OandX")) {
					continue;
				}
					
				try {
					row = Integer.parseInt(token.getAttribute(2));
					column = Integer.parseInt(token.getAttribute(3));
				}
				catch (NumberFormatException ex) {
					System.err.println("Invalid output from the agent: "+token.toString());
					continue;
				}
				if ((row < 0) || (row > 2) || (column < 0) || (column > 2)) {
					System.err.println("Invalid row or column from the agent: "+token.toString());
					continue;
				}
				
				// is this -(_OUTPUT, OandX, <row>, <column>, <state>)
				if (token.numberOfAttributes() == 5 
						&& token.getFlag().equals(TokenFlag.DELETE)
						&& updatesSent.containsKey("OandX"+row+column)) {
					String[] attributes = {"_INPUT", "OandX", Integer.toString(row), Integer.toString(column), updatesSent.get("OandX"+row+column)};
					agent.pushInput(new TokenPacket(attributes, false));
					updatesSent.remove("OandX"+row+column);
					if (updatesSent.isEmpty()) {
						if (turnState.equals(TurnState.WAITING_ACK_PLAYER)) {
							agent.pushInput(new TokenPacket(yourMoveAttributes, true));
							turnState = TurnState.WAITING_AGENT;
						}
						else if (turnState.equals(TurnState.WAITING_ACK_AGENT)) {
							turnState = TurnState.WAITING_PLAYER;
						}
					}
					continue;
				}

				// is this (_OUTPUT, OandX, <row>, <column>)?
				if (token.numberOfAttributes() != 4 ||
						!token.getFlag().equals(TokenFlag.ADD)) {
					System.err.println("The agent has sent an unexpected output.");
					continue;
				}					
				if (!turnState.equals(TurnState.WAITING_AGENT)) {
					System.err.println("The agent has made a move out of turn.");
					continue;
				}
				if (gameState[row][column]!=SquareState.BLANK) {
					System.err.println("The agent has made an invalid move: "+row+" "+column);
					continue;
				}
				agent.pushInput(new TokenPacket(yourMoveAttributes, false));
				gameState[row][column]=SquareState.CROSS;
				updateAgentGameState();
				turnState = TurnState.WAITING_ACK_AGENT;

				// showCross needs to be run on the FX application thread
				Platform.runLater(new Runnable() {
					@Override public void run() {
						myMainSceneController.showCross(row, column);
						myMainSceneController.setSkipEnable(true);
					}
				});
			}
        }
    }

}

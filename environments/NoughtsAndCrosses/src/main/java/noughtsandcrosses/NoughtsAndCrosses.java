/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package noughtsandcrosses;

import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

/**
 *
 * @author phillips
 */
public class NoughtsAndCrosses extends Application {
    
    private MainSceneController mainSceneController;
    private Game game;
    
    @Override
    public void start(Stage stage) throws Exception {
        game = new Game();
        
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("MainScene.fxml"));
        Parent root = fxmlLoader.load();
        mainSceneController = fxmlLoader.getController();        
        
        game.setMainSceneController(mainSceneController);
        mainSceneController.setGame(game);
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setMinWidth(295);
        stage.setMinHeight(335);
        // To Do: remove these max values to allow resizing
        // Note that the circles and crosses will need to be resized
        stage.setMaxWidth(295);
        stage.setMaxHeight(335);        
        stage.setTitle("Noughts and Crosses");
		stage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			  public void handle(WindowEvent we) {
				  game.gameOver();
			}
		});        
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }    
 
}

/* 
 * Copyright (c) 2015, Jesse Frost, Braden Phillips, The University of Adelaide.
 */
package noughtsandcrosses;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Line;
import javafx.scene.shape.StrokeType;

/**
 * FXML Controller class
 *
 * @author phillips
 */
public class MainSceneController implements Initializable {

    private Game myGame;
    @FXML
    private Pane pane00;
    @FXML
    private Pane pane01;
    @FXML
    private Pane pane02;
    @FXML
    private Pane pane10;
    @FXML
    private Pane pane11;
    @FXML
    private Pane pane12;
    @FXML
    private Pane pane20;
    @FXML
    private Pane pane21;
    @FXML
    private Pane pane22;
    private Pane[][] paneArray;
    @FXML
    private Button skipButton;
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        Pane[][] temp = {{pane00,pane01,pane02},{pane10,pane11,pane12},{pane20,pane21,pane22}};
        paneArray = temp;
    }    

    @FXML
    private void quit(ActionEvent event) {
        myGame.gameOver();
    }

    @FXML
    private void restart(ActionEvent event) {
        myGame.restart();
    }

    @FXML
    private void mouseClick(MouseEvent event) {
        int row;
        int column=0;
        boolean found = false;
        if (!(event.getTarget() instanceof Pane)) {
            return;
        }
        paneSearch:
            for (row=0; row<3; row++) {
                for (column=0; column<3; column++)  {
                    if (((Pane)event.getTarget()).equals(paneArray[row][column])) {
                        found = true;
                        break paneSearch;
                    }
                }
            }
        if (!found) {
            return;
        }
        myGame.playerMove(row,column);
    }
    
    void setGame(Game game) {
        myGame = game;
    }
    
    void showNought(int row, int column) {
        Circle nought = new Circle(25);
        nought.setLayoutX(38.0f);
        nought.setLayoutY(38.0f);
        nought.setStrokeWidth(2);
        nought.setFill(Color.web("#ffffff00"));
        nought.setStrokeType(StrokeType.OUTSIDE);
        nought.setStroke(Color.web("black"));
        paneArray[row][column].getChildren().add(nought);
    }

    void showCross(int row, int column) {
        Line forwardslash = new Line(-25.0f,0,25.0f,0);
        forwardslash.setLayoutX(38.0f);
        forwardslash.setLayoutY(38.0f);
        forwardslash.setStrokeWidth(2);
        forwardslash.setStrokeType(StrokeType.OUTSIDE);
        forwardslash.setStroke(Color.web("black"));
        forwardslash.setRotate(45);
        paneArray[row][column].getChildren().add(forwardslash);
        Line backslash = new Line(-25.0f,0,25.0f,0);
        backslash.setLayoutX(38.0f);
        backslash.setLayoutY(38.0f);
        backslash.setStrokeWidth(2);
        backslash.setStrokeType(StrokeType.OUTSIDE);
        backslash.setStroke(Color.web("black"));
        backslash.setRotate(315);
        paneArray[row][column].getChildren().add(backslash);
    }
    
    void clearAll() {
        for (int row=0; row<3; row++) {
            for (int column=0; column<3; column++) {
                paneArray[row][column].getChildren().clear();                
            }
        }
    }

    void setSkipEnable(boolean enable) {
        skipButton.setDisable(!enable);
    }
    
    @FXML
    private void skip(ActionEvent event) {
        myGame.playerSkip();
    }
 
}

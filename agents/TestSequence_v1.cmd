echo ** Testing Sequence_v1.street
load Sequence_v1.street
trace new
step 1
assert counter value 0
step 1
assert - counter value 0
assert counter value 1
assert statesVisited Apple true
step 1
assert - counter value 1
assert counter value 2
assert statesVisited Banana true
step 1
assert - counter value 2
assert counter value 3
assert statesVisited Cherry true
step 1
assert - counter value 3
assert counter value 4
assert statesVisited Durian true
step 1
assert - counter value 4
assert counter value 0
assert statesVisited Elderberry true
echo ** Tests Pass
exit

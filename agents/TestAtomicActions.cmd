echo ** Testing AtomicActions.street
load AtomicActions.street
trace none
step 1
assert C D E
assert C D F
step
assert E F G
assert - C D E
assert - Test0 Fail True
assert Test1 Pass True
step
assert Test2 Pass E
assert Test2 Pass F
assert Test3 Pass True
assert - Test4 Fail True
echo ** Tests Pass
exit

This folder contains simple agents intended as examples and case studies and
also to test the simulator.

Agents that test corner cases but that are not useful or interesting in
themselves are called `Test<Something>.street`.

Command files that run a set of tests and apply assertions are called
`Test<Something>.cmd` and either load `<Something>.street` or
`Test<Something>.street`.


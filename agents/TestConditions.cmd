echo ** Testing TestConditions.street
load TestConditions.street
trace none
step 2
assert testHasPassed pass1 true
assert testHasPassed pass2 true
assert testHasPassed pass3 true
assert testHasPassed pass4 true
assert testHasPassed pass5 true
assert testHasPassed pass6 5
assert testHasPassed pass7 true
assert testHasPassed pass8 5
assert testHasPassed pass9 5
assert testHasPassed pass10 true
assert testHasPassed pass11a 5
assert testHasPassed pass11b 10
assert testHasPassed pass12a 5
assert testHasPassed pass12b 10
assert testHasPassed pass13 Bob
assert testHasPassed pass14 5
assert testHasPassed pass15 A
assert testHasPassed pass16z 5
assert testHasPassed pass16y 10
assert testHasPassed pass16x 5
assert testHasPassed pass17z 5
assert testHasPassed pass17y 10
assert testHasPassed pass18 true
assert testHasPassed pass19 true
assert testHasPassed pass20 true
assert testHasPassed pass21 true
// assert testHasPassed pass22 3
echo !! SKIPPING REPEATED VARIABLE ASSERTION
// assert testHasPassed pass23 3
echo !! SKIPPING REPEATED VARIABLE ASSERTION
assert - testHasFailed fail1 true
assert - testHasFailed fail2 true
assert - testHasFailed fail3 true
assert - testHasFailed fail4 true
assert - testHasFailed fail5 true
assert - testHasFailed fail6 true
assert - testHasFailed fail7 true
assert - testHasFailed fail8 true
assert - testHasFailed fail9 true
assert - testHasFailed fail10 true
assert - testHasFailed fail10a true
assert - testHasFailed fail11 true
assert - testHasFailed fail12 true
assert - testHasFailed fail13 true
assert - testHasFailed fail14 true
assert - testHasFailed fail15 true
assert - testHasFailed fail16 true
assert - testHasFailed fail17 true
assert - testHasFailed fail18 true
assert - testHasFailed fail19 true
assert - testHasFailed fail20 true
// assert - testHasFailed fail21 true
echo !! SKIPPING REPEATED VARIABLE ASSERTION
echo ** Tests Pass
exit

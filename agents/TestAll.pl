#!/usr/bin/perl -w

# For each file Test<x>.cmd this perl script runs
# java -jar ../build/libs/JavaStreet.jar < Test<x>.cmd.
# If any of the simulations terminate with an error due to 
# a failed assertion, this script will terminate with an
# error.

@files = <Test*.cmd>;
foreach $file (@files) {
	print $file . "\n";
	system("java -jar ../build/libs/JavaStreet.jar < " .$file);
	if ($? == -1) {
  		print "command failed: $!\n";
	}
	else { 
		if ($? != 0) {
	  		printf "TEST FAILED AN ASSERTION\n";
	  		exit 1;
		}
	}
}

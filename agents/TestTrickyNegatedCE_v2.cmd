echo ** Testing TrickyNegatedCE_v2.street
trace none
trace debug
load TrickyNegatedCE_v2.street
add problem object fridge
add fridge hasA beer
add beer isA thing
add fridge hasA apple
add apple isA thing
step
assert - ruleHasFired fridge apple
assert - ruleHasFired fridge beer
del fridge hasA beer
step
assert - ruleHasFired fridge apple
assert ruleHasFired fridge beer
del fridge hasA apple
step
assert ruleHasFired fridge apple
assert ruleHasFired fridge beer
echo ** Tests Pass
exit


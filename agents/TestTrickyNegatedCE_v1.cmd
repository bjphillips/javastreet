echo ** Testing TrickyNegatedCE_v1.street
trace none
trace debug
load TrickyNegatedCE_v1.street
add problem object fridge
step
assert fridge hasNothingDetected true
add fridge hasA beer
add fridge hasA apple
del fridge hasNothingDetected true
step
del fridge hasA beer
step
assert - fridge hasNothingDetected true
del fridge hasA apple
step
assert fridge hasNothingDetected true
echo ** Tests Pass
exit


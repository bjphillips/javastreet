echo ** Testing TestNegatedConditions.street
load TestNegatedConditions.street
add S1 A 1
add S1 A 2
add S2 C 1
add S2 C 3
step
assert - hasFired rule1 1
assert hasFired rule1 2
assert - hasFired rule2 1
assert - hasFired rule2 2
assert hasFired rule4 1
assert hasFired rule4 2
del S2 C 1
step
assert hasFired rule1 1
assert hasFired rule1 2
assert - hasFired rule2 1
assert - hasFired rule2 2
assert hasFired rule4 1
assert hasFired rule4 2
del S2 C 3
step
assert hasFired rule1 1
assert hasFired rule1 2
assert hasFired rule2 1
assert hasFired rule2 2
assert hasFired rule4 1
assert hasFired rule4 2
add S2 B 1
add S2 B 2
del hasFired rule4 1
del hasFired rule4 2
step
del S2 B 1
assert - hasFired rule4 1
assert - hasFired rule4 2
step
del S2 B 2
step
assert hasFired rule4 1
assert hasFired rule4 2
echo ** Tests Pass
exit

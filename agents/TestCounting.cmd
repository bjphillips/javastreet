echo ** Testing TestCounting.street
load TestCounting.street
add id1 subagent countItems
add id1 hasA id2
add id2 isA table
add id2 alreadyCounted false
step
add id1 hasA id3
add id3 isA chair
add id3 alreadyCounted false
step
add id1 numberItems 0
step 3
assert id2 myIndex 0
assert id3 myIndex 1
assert id1 numberItems 2
add id4 subagent countItemsBadly
add id4 hasA id5
add id5 isA apple
add id5 alreadyCounted false
step
add id4 hasA id6
add id6 isA banana 
add id6 alreadyCounted false
step
add id4 numberItems 0
step 3
add id1 hasA id7
add id7 isA bowl
add id7 alreadyCounted false
add id1 hasA id8
add id8 isA spoon
add id8 alreadyCounted false
step 3
assert id7 myIndex 2
assert id8 myIndex 3
assert id1 numberItems 4
echo ** Tests Pass
exit

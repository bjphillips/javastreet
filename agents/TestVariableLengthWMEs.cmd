echo ** Testing TestVariableLengthWMEs.street
load TestVariableLengthWMEs.street

// WM initially {}
// add A B C
// add 1 2
// WM finally {(A B C), (1 2)}
add A B C
add 1 2
step
assert - ruleFired AB
assert ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert ruleFired notAB
assert - ruleFired notABC
assert ruleFired notABD
assert ruleFired notABCD
del ruleFired ABC
del ruleFired notAB
del ruleFired notABD
del ruleFired notABCD
step

// WM initially {(A B C), (1 2)}
// add A B C
// WM finally {(A B C), (1 2)}
add A B C
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
step

// WM initially {(A B C), (1 2)}
// del A B C
// WM finally {(1 2)}
del A B C
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
del ruleFired notABC
step

// WM initially {(1 2)}
// add (A B)
// WM finally {(1 2), (A B)}
add A B
step
assert ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
del ruleFired AB
step

// WM initially {(1 2), (A B)}
// del A B C
// WM finally {(1 2), (A B)}
del A B C
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
step

// WM initially {(1 2), (A B)}
// add A B C
// WM finally {(1 2), (A B), (A B C)}
add A B C
step
assert - ruleFired AB
assert ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
del ruleFired ABC
step

// WM initially {(1 2), (A B), (A B C)}
// add A B C D
// WM finally {(1 2), (A B), (A B C), (A B C D)}
add A B C D
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
del ruleFired ABCD
step

// WM initially {(1 2), (A B), (A B C), (A B C D)}
// del A B
// WM finally {(1 2), (A B C), (A B C D)}
del A B
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
del ruleFired notAB
step

// WM initially {(1 2), (A B C), (A B C D)}
// del A B
// WM finally {(1 2), (A B C), (A B C D)}
del A B
step
assert - ruleFired AB
assert - ruleFired ABC
assert - ruleFired ABD
assert - ruleFired ABCD
assert - ruleFired notAB
assert - ruleFired notABC
assert - ruleFired notABD
assert - ruleFired notABCD
step

echo ** Tests Pass
exit

echo ** Testing Sequence_v2.street
load Sequence_v2.street
trace new
step 1
assert _ID1 counter 0
step 1
assert - _ID1 counter 0
assert _ID1 counter 1
assert hasOutput Apple true
assert - hasOutput Banana true
assert - hasOutput Cherry true
assert - hasOutput Durian true
assert - hasOutput Elderberry true
step 1
assert - _ID1 counter 1
assert _ID1 counter 2
assert hasOutput Banana true
step 1
assert - _ID1 counter 2
assert _ID1 counter 3
assert hasOutput Cherry true
step 1
assert - _ID1 counter 3
assert _ID1 counter 4
assert hasOutput Durian true
step 1
assert - _ID1 counter 4
assert _ID1 counter 0
assert hasOutput Elderberry true
echo ** Tests Pass
exit

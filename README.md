# README #

## JavaStreet Simulator ##

Street Language is a parallel production rule language. It is executed directly
by a Street Engine, a new kind of processor designed specifically for
artificial intelligence systems.

JavaStreet is a simple simulator for Street Language. It can be used to develop
and debug Street Language agents. It can also be connected to physical or
virtual peripherals allowing an agent to interact with simulated or real
environments.

More information on the Street project can be found via the 
[project's homepage](http://www.adelaide.edu.au/chiptec/research/Street/).

For more information on Street Language refer to this 
[brief introduction](./doc/StreetLanguage.pdf) (in `doc/StreetLanguage.pdf`).

### Getting Started ###

* Download and install [JDK8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Clone this repository, and check out the `master` branch.
    - This branch contains all the code and assets required to run the
      simulator and is the latest working version of the code.
* Execute the build task of the included Gradle wrapper, `gradlew build` on
  Windows, or `./gradle build` on *nix systems.
    - This will download Gradle as well as any project dependencies, assemble
      the project, and put everything you need in `build/libs/JavaStreet.jar`.
    - Note that if you are running on Windows, you will may need to allow the
      application through your firewall.
* Copy JavaStreet.jar to some sensible location on your CLASSPATH or just put 
  in a working folder with your Street Language source files.

### Running the Simulator ###

You can run the simulator as a console debugger, as a windowed debugger or in
command line mode.

#### Command Line Mode ####

Launch the simulator with, for example:

    java -jar JavaStreet.jar Sequence_v1.street 10

This example will load the production rules from the first sequence example
into a Street agent and then step the agent through 10 cycles.

(Instead of building the JavaStreet.jar file you can use Gradle to run the
simulator. For example, within the project's root folder, 
`./gradlew run -PappArgs="['agents/Sequence_v1.street', 10]"`

#### Console Debugger Mode ####

To run in console debugger mode, launch the simulator with no command line
parameters, for example:

    java -jar JavaStreet.jar

You will be presented with a command prompt:

    Street>    

Enter `help` for a list of commands. You can, for example, load source files,
step the agent and print the contents of working memory.

The debugger can also be run in _batch mode_ by redirecting standard input, for
example:

    java -jar JavaStreet.jar < Test.cmd

or

    java -jar JavaStreet.jar -b Test.cmd

where `Test.cmd` is a text file containing interactive debugger commands.

#### Windowed Debugger Mode ####

To run in windowed debugger mode, launch the simulator with `-w` as the first
command line parameter, for example:

    java -jar JavaStreet.jar -w

You will be presented with a debugger window. There is a text field in which you
can enter debugger commands. The same commands as used for the console debugger
are available. Enter `help` for a list.

### Code Development Tools ###

The code should be built and run using Gradle, which will use the
`build.gradle` file to set the appropriate class paths. This means you do not
need to manually configure your environment.

Of course you can, if you wish, use an IDE to develop. Just please
*do not check IDE specific project files into the git repository!*

The windowed debugger uses [JavaFx in a Swing Application](http://docs.oracle.com/javase/8/javafx/interoperability-tutorial/swing-fx-interoperability.htm#CHDIEEJE).
The GUI components were specified in FXML using JavaFX Scene Builder 2.0.

#### Netbeans ####

To use the Netbeans IDE, add the [Gradle Support](http://plugins.netbeans.org/plugin/44510/gradle-support) 
plugin to Netbeans.
Then you can open the root project folder as a Netbeans project. The `Build`,
`Debug` and `Run` commands in the IDE work (so you can debug the debugger if
you like).

From within Netbeans you will need to run the simulator in _command line mode_ ,
_batch mode_ or as a _windowed debugger_. To do this you need to set the command
line arguments with the project context menu `Properties | Gradle project |
Manage Built-In Tasks`. To run in _command line mode_, change the arguments of
the `Run` task to, for example:

    -PappArgs=['agents/Sequence_v1.street','10']

or, for example: 

    -PappArgs=['-b','agents/Test.cmd']

#### IntelliJ IDEA ####

*Note this is untested.*

Using the [IDEA plugin](http://www.gradle.org/docs/current/userguide/idea_plugin.html)
for Gradle you can create the configuration files for an IDEA project and
workspace. From the command line run `gradle idea` to create the files. Just
don't check them in to source control.

#### Eclipse ####

*Note this is untested.*

Using the [Eclipse plugin](http://gradle.org/docs/current/userguide/eclipse_plugin.html)
for Gradle you can create the configuration files for an Eclipse project. 
From the command line run `gradle eclipse` to create the files. Just
don't check them in to source control.

### Example Environments ###

The `environments` folder contains some examples of how the JavaSteet simulator
can be embedded within a virtual environment. The environment is created as a
separate Java application that includes the JavaStreet.jar archive.

#### Adder Threaded ####

This is a very simple example in which an agent is created, loaded with rules
from a file and then run in its own thread. A trivial peripheral is attached to
the agent to demonstrate how to handle outputs from the agent.

Before building this example, ensure that `JavaStreet.jar` has been built 
and is in the `build/libs` folder.

Although a `Makefile` is included, the environment can be built simply with

    cd environemnts/AdderThreaded
    javac -classpath "../../build/libs/*" *.java


#### Noughts and Crosses ####

This example uses a graphical environment and was developed as a JavaFX
application. It also demonstrates how the windowed debugger can be attached to
an environment.

There is a gradle build system. First ensure that `JavaStreet.jar`
has been built and is in the `build/libs` folder.

To run the example:

    cd environments/NoughtsAndCrosses
    gradlew run

Alternatively, you can build a distribution of the example with:

    gradlew build

This will create application installers for you O/S in
`NoughtsAndCrosses\build\distributions`.

### Project Status ###

The latest major release is v2.

The v2 release adds support for variable length working memory elements. It is
backwards compatible with v1 for the most part. Salient exceptions are:

* In condition elements, binary relations with a variable on the left hand side
must be enclosed in parenthesis e.g. `(myAgent version (<v> > 1))`.
* The Token constructor has changed to support tokens with a variable number of
attributes.
* The I/O interface to the built-in peripherals (timer, random number generator
  and print peripheral) has changed to take advantage of variable length
  tokens.

There is plenty of work to do as demonstrated by the [open issues](https://bitbucket.org/bjphillips/javastreet/issues?status=new&status=open).

### Source Control ###

Version numbers are assigned according to [Semantic Versioning](http://semver.org). 
This means that version numbers are of the form `MAJOR.MINOR.PATCH` where:

* `MAJOR` versions are released when there are changes to the Street Language,
  the debugger interface, or the simulator API that are not backwards
  compatible.
* `MINOR` versions are released when there is new backwards compatible
  functionality.
* `PATCH` versions are released with backwards compatible bug fixes.

The project uses a branching model based on [Gitflow](http://nvie.com/posts/a-successful-git-branching-model/).
Although we are not being strict about all aspects of the model, please develop
non-trivial features in a feature branch, merge changes into the develop branch
and release tagged versions through the master branch.

### License ###

JavaStreet is distributed under the [BSD License](./LICENSE.md).

### Contacts ###

* [Braden Phillips](http://www.adelaide.edu.au/directory/braden.phillips) is
  the primary project supervisor.